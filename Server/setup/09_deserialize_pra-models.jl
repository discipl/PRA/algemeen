using DataFrames
using Arrow
using JSONTables
using YAML
using JSON3
using OrderedCollections
using Glob

const PROCESSED_DATA_PATH = joinpath("data", "processed")
const PRA_MODELS_PATH = joinpath("data","pra-models")

yaml_files = glob("*/*", PRA_MODELS_PATH)

model_dicts = YAML.load_file.(yaml_files, dicttype=OrderedDict{String,Any})

content = DataFrame([NamedTuple{Tuple(Symbol.(keys(x)))}(values(x)) for x in  model_dicts])

