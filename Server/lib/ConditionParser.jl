module ConditionParser

struct NotSetYet end

abstract type BooleanExpression end
abstract type StringExpression end
abstract type StringCollectionExpression end

struct LookupOperationTrait end
struct UnaryOperationTrait end
struct BinaryOperationTrait end
struct VarArgOperationTrait end
struct LiteralOperationTrait end

struct ComputedBooleanLookup <: BooleanExpression
    variableName::String
end
operationtype(::ComputedBooleanLookup) = LookupOperationTrait()

struct KnownBooleanLookup <: BooleanExpression
    variableName::String
end
operationtype(::KnownBooleanLookup) = LookupOperationTrait()

struct AnyOf <: BooleanExpression
    arguments::Vector{BooleanExpression}
end
operationtype(::AnyOf) = VarArgOperationTrait()

struct AllOf <: BooleanExpression
    arguments::Vector{BooleanExpression}
end
operationtype(::AllOf) = VarArgOperationTrait()

struct ContainsAnyStringOf <: BooleanExpression
    firstArgument::StringCollectionExpression
    secondArgument::StringCollectionExpression
end
operationtype(::ContainsAnyStringOf) = BinaryOperationTrait()

struct StringLiteral <: StringExpression
    value::String
end
operationtype(::StringLiteral) = LiteralOperationTrait()

struct KnownStringLookup <: StringCollectionExpression
    variableName::String
end
operationtype(::KnownStringLookup) = LookupOperationTrait()

struct StringCollectionLiteral <: StringCollectionExpression
    arguments::Vector{StringExpression}
end
operationtype(::StringCollectionLiteral) = VarArgOperationTrait()

struct KnownStringCollectionLookup <: StringCollectionExpression
    variableName::String
end
operationtype(::KnownStringCollectionLookup) = LookupOperationTrait()

function parse_expression(expression_dict)
    operator_str = expression_dict["operator"]
    if operator_str == "ComputedBooleanLookup"
        ComputedBooleanLookup(expression_dict["variableName"])
    elseif operator_str == "KnownBooleanLookup"
        KnownBooleanLookup(expression_dict["variableName"])
    elseif operator_str == "AnyOf"
        AnyOf(parse_expression.(expression_dict["arguments"]))
    elseif operator_str == "AllOf"
        AllOf(parse_expression.(expression_dict["arguments"]))
    elseif operator_str == "ContainsAnyStringOf"
        ContainsAnyStringOf(parse_expression(expression_dict["arguments"]["firstArgument"]),
                            parse_expression(expression_dict["arguments"]["secondArgument"]))
    elseif operator_str == "StringLiteral"
        StringLiteral(expression_dict["value"])
    elseif operator_str == "KnownStringLookup"
        KnownStringLookup(expression_dict["variableName"])
    elseif operator_str == "StringCollectionLiteral"
        StringCollectionLiteral(parse_expression.(expression_dict["arguments"]))
    elseif operator_str == "KnownStringCollectionLookup"
        KnownStringCollectionLookup(expression_dict["variableName"])
    elseif operator_str == "NotSetYet"
        NotSetYet()
    end
end

function evaluate(expression::ComputedBooleanLookup, context_variables::AbstractDict, computed_variables::AbstractDict)
    if !(:Boolean in keys(computed_variables))
        return missing
    end
    get(computed_variables[:Boolean], Symbol(expression.variableName), missing)
end
function evaluate(expression::KnownBooleanLookup, context_variables::AbstractDict, computed_variables::AbstractDict)
    if !(:Boolean in keys(context_variables))
        return missing
    end
    get(context_variables[:Boolean], Symbol(expression.variableName), missing)
end
function evaluate(expression::AnyOf, context_variables::AbstractDict, computed_variables::AbstractDict)
    any(evaluate.(expression.arguments, Ref(context_variables), Ref(computed_variables)))
end
function evaluate(expression::AllOf, context_variables::AbstractDict, computed_variables::AbstractDict)
    all(evaluate.(expression.arguments, Ref(context_variables), Ref(computed_variables)))
end
function evaluate(expression::ContainsAnyStringOf, context_variables::AbstractDict, computed_variables::AbstractDict)
    !isdisjoint(evaluate(expression.firstArgument, context_variables, computed_variables),
                evaluate(expression.secondArgument, context_variables, computed_variables))
end
function evaluate(expression::StringLiteral, context_variables::AbstractDict, computed_variables::AbstractDict)
    expression.value
end
function evaluate(expression::KnownStringLookup, context_variables::AbstractDict, computed_variables::AbstractDict)
    if !(:String in keys(context_variables))
        return missing
    end
    get(context_variables[:String], Symbol(expression.variableName), missing)
end
function evaluate(expression::StringCollectionLiteral, context_variables::AbstractDict, computed_variables::AbstractDict)
    evaluate.(expression.arguments, Ref(context_variables), Ref(computed_variables))
end
function evaluate(expression::KnownStringCollectionLookup, context_variables::AbstractDict, computed_variables::AbstractDict)
    if !(:StringCollection in keys(context_variables))
        return String[]
    end
    get(context_variables[:StringCollection], Symbol(expression.variableName), String[])
end
function evaluate(expression::NotSetYet, context_variables::AbstractDict, computed_variables::AbstractDict)
    error("incomplete expression")
end

#function evaluate(expression, context_variables::AbstractDict, computed_variables::AbstractDict)
#    evaluate(expression, context_variables, computed_variables, operationtype(expression))
#end
#
#function evaluate(expression, context_variables::AbstractDict, computed_variables::AbstractDict, ::LookupOperationTrait)
#    expression.variableName
#end

struct Notification
    item_id::Int
    notification_id::Int
    language::String
    text::String
    criterion::BooleanExpression
end

function evaluate(notification::Notification, context_variables::AbstractDict, computed_variables::AbstractDict)
    evaluate(notification.criterion, context_variables, computed_variables)
end

#function find_condition(conditions::AbstractVector{<:AbstractDict}, name::AbstractString)
function find_condition(conditions::AbstractVector, name::AbstractString)
    for condition in conditions
        if condition["variableName"] == name
            return condition["assignment"]
        end
    end
end

function regeling2notifications(regeling::AbstractDict)
    if !("Regeling.Notificaties" in keys(regeling)) || isnothing(regeling["Regeling.Notificaties"])
        return Notification[]
    end
    return [Notification(regeling["Id"],
                  notification["NotificationId"],
                  regeling["Zoekdescriptor.Taal"],
                  notification["TextTemplate"],
                  parse_expression(find_condition(regeling["Regeling.Condities"],
                                   notification["Criterion"]["variableName"])))
            for notification in regeling["Regeling.Notificaties"]
            if !isnothing(find_condition(regeling["Regeling.Condities"], notification["Criterion"]["variableName"]))]
end

end # module
