module SchemeModel

using YAML
using OrderedCollections

import ..Main.GlobalConstants.SCHEME_PATH

const TERM_COL_NAME = "Zoekdescriptor.Zoektermen"
const FILTER_COL_NAME = "Zoekdescriptor.Filters"
const UNIQUE_RESULTS_COL_NAME = "Zoekdescriptor.Model"

abstract type Format end
struct LanguageFormat <: Format end
struct FixedFormat <: Format end
struct VariantFormat <: Format end
struct ModelFormat <: Format end
struct DateTimeFormat <: Format end
struct ContentTypeFormat <: Format end
struct RelativeIdFormat <: Format end
struct RelativeContentIdFormat <: Format end
struct ContentBlockTypeFormat <: Format end
struct RelativeContentBlockIdFormat <: Format end
struct HtmlTextFormat <: Format end
# TODO rename format slug with handle
struct SlugFormat <: Format end
struct HandleFormat <: Format end
struct DateTimeFormat <: Format end
struct ContentBlocksFormat <: Format end
struct SingleLineTextFormat <: Format end
struct MultiLineTextFormat <: Format end
struct ExpansionGroupFormat <: Format end
struct ContentFormat <: Format end
struct ComputedFormat <: Format end
struct ItemTypeFormat <: Format end
struct ChipsFormat <: Format end
struct FilterFormat <: Format end
struct TermFormat <: Format end
struct TypeLinkFormat <: Format end
struct GrondslagFormat <: Format end
struct LevensgebeurtenisLinkFormat <: Format end
struct CategoryFormat <: Format end
struct FilterTypeFormat <: Format end
struct IconFormat <: Format end
struct FiltersFormat <: Format end
struct MaintainersFormat <: Format end
struct NoFormat <: Format end
struct UnknownFormat <: Format 
    name::String
end

struct NotificationListFormat <: Format end
struct ConditionListFormat <: Format end
struct AssignmentFormat <: Format end
struct ContextVariableDefinitionFormat <: Format end
struct ContextVariableDefinitionListFormat <: Format end

struct ComputedBooleanLookupFormat <: Format end
struct KnownBooleanLookupFormat <: Format end
struct AnyOfFormat <: Format end
struct AllOfFormat <: Format end
struct ContainsAnyStringOfFormat <: Format end
struct KnownStringCollectionLookupFormat <: Format end
struct StringCollectionLiteralFormat <: Format end
struct StringLiteralFormat <: Format end
struct KnownStringLookupFormat <: Format end
struct UrlFormat <: Format end
struct TableFormat <: Format end
struct SpeachBulbFormat <: Format end
struct ImageFormat <: Format end
struct FloatFormat <: Format end

function str2format(str)
    mapping = Dict(
        "relative-content-block-id" => RelativeContentBlockIdFormat(),
        "slug" => SlugFormat(),
        "html-text" => HtmlTextFormat(),
        "content-type" => ContentTypeFormat(),
        "content-block-type" => ContentBlockTypeFormat(),
        "relative-content-id" => RelativeContentIdFormat(),
        "content-blocks" => ContentBlocksFormat(),
        "single-line-text" => SingleLineTextFormat(),
        "multi-line-text" => MultiLineTextFormat(),
        "expansion-group" => ExpansionGroupFormat(),
        "content" => ContentFormat(),
        "computed" => ComputedFormat(),
        "fixed" => FixedFormat(),
        "item-type" => ItemTypeFormat(),
        "date-time" => DateTimeFormat(),
        "language" => LanguageFormat(),
        "variant" => VariantFormat(),
        "model" => ModelFormat(),
        "chips" => ChipsFormat(),
        "filter" => FilterFormat(),
        "term" => TermFormat(),
        "type-link" => TypeLinkFormat(),
        "grondslag" => GrondslagFormat(),
        "levensgebeurtenis-link" => LevensgebeurtenisLinkFormat(),
        "category" => CategoryFormat(),
        "filter-type" => FilterTypeFormat(),
        "icon" => IconFormat(),
        "filters" => FiltersFormat(),
        "maintainers" => MaintainersFormat(),
        "none" => NoFormat(),
        "notification-list" => NotificationListFormat(),
        "condition-list" => ConditionListFormat(),
        "assignment" => AssignmentFormat(),
        "context-variable-definition" => ContextVariableDefinitionFormat(),
        "ContextVariableDefinitionList" => ContextVariableDefinitionListFormat(),
        "ComputedBooleanLookup" => ComputedBooleanLookupFormat(),
        "KnownBooleanLookup" => KnownBooleanLookupFormat(),
        "AnyOf" => AnyOfFormat(),
        "AllOf" => AllOfFormat(),
        "ContainsAnyStringOf" => ContainsAnyStringOfFormat(),
        "KnownStringCollectionLookup" => KnownStringCollectionLookupFormat(),
        "StringCollectionLiteral" => StringCollectionLiteralFormat(),
        "KnownStringLookup" => KnownStringLookupFormat(),
        "StringLiteral" => StringLiteralFormat(),
        "relative-id" => RelativeIdFormat(),
        "Url" => UrlFormat(),
        "Table" => TableFormat(),
        "SpeachBulb" => SpeachBulbFormat(),
        "Image" => ImageFormat(),
        "float" => FloatFormat(),
    )
    return get(mapping, str, UnknownFormat(str))
end
Format(scheme::AbstractDict) = str2format(get(scheme, "format", "none"))

abstract type AbstractDescription end
struct NoDescription <: AbstractDescription end
struct Description <: AbstractDescription
    text::String
end

function description(scheme::AbstractDict)
    if "description" in keys(scheme) && !isnothing(scheme["description"])
        return Description(scheme["description"])
    else
        return NoDescription()
    end
end

abstract type ValueGeneration end
struct NoGeneration <: ValueGeneration end
struct StaticValue{T} <: ValueGeneration
    value::T
end
abstract type ValueFunction <: ValueGeneration end
struct GloballyUniqueForFieldOverAllObjectInstances <: ValueFunction end
struct GloballyUniqueForField <: ValueFunction end
struct CurrentDatetime <: ValueFunction end
struct CurrentFilePath <: ValueFunction end
struct UnimplementedValueGeneration <: ValueGeneration
    name::String
end
#struct  <: ValueInitializer end
#struct  <: ValueInitializer end

ValueGeneration(generation_name::AbstractString, scheme::AbstractDict) = get(
    Dict("unique_for_field_over_all_object_instances" => ()->GloballyUniqueForFieldOverAllObjectInstances(),
         "unique_for_field" => ()->GloballyUniqueForField(),
         "current_datetime" => ()->CurrentDatetime(),
         "current_path" => ()->CurrentFilePath(),
    ),
    generation_name, ()->UnimplementedValueGeneration(generation_name)
)()

function initializer(scheme::AbstractDict)
    if "default" in keys(scheme)
        return StaticValue(scheme["default"])
    elseif "initializer" in keys(scheme)
        return ValueGeneration(scheme["initializer"]["function"], scheme)
    else
        return NoGeneration()
    end
end

function autoupdater(scheme::AbstractDict)
    if "autoupdater" in keys(scheme)
        return ValueGeneration(scheme["autoupdater"]["function"], scheme)
    else
        return NoGeneration()
    end
end

abstract type SchemeContainer end
abstract type Scheme end

struct ValueScheme{S<:Scheme, F<:Format, D<:AbstractDescription, I<:ValueGeneration, A<:ValueGeneration} <: SchemeContainer
    scheme::S
    format::F
    name::String
    description::D
    initializer::I
    autoupdater::A
end

ValueScheme(s::Scheme, scheme::AbstractDict, key::AbstractString) = ValueScheme(
    s,
    Format(scheme),
    convert(String, if "editor" in keys(scheme) && "sectionheading" in keys(scheme["editor"])
        scheme["editor"]["sectionheading"]
    else
        key
    end),
    description(scheme),
    initializer(scheme),
    autoupdater(scheme),
)

# TODO make this type stable with a NamedTuple
struct RecursiveRefScheme <: SchemeContainer
    defs::Dict{String, Union{ValueScheme, Nothing}}
    ref::String
end

#_show(x::Format) = println(x)
#_show(x::AbstractDescription) = println(x)
#_show(x::ValueGeneration) = println(x)

#Base.show(io::IO, v::Scheme) = _show(v)

# struct Field{V<:ValueScheme}
#     key::String
#     value::V
# end
#struct ObjectScheme <: Scheme
#    properties::Vector{Field}
#end

# NT is objectkey => <:SchemeContainer
struct ObjectScheme{NT<:NamedTuple} <: Scheme
    properties::NT
    required::Vector{Symbol}
end
struct ArrayScheme{V<:SchemeContainer} <: Scheme
    items::V
end
#struct OneOfScheme <: Scheme
#    list::Vector{ObjectScheme}
#end
#struct AllOfScheme <: Scheme
#    list::Vector{ObjectScheme}
#end
# NT is discriminator_value => ObjectScheme
struct OneOfScheme{NT<:NamedTuple} <: Scheme
    possible_objects::NT
    discriminator::Symbol
end
# T is list of ObjectSchemes
struct AllOfScheme{T<:Tuple} <: Scheme
    merge_objects::T
end
struct StringScheme <: Scheme end
struct StringEnumScheme <: Scheme
    values::Vector{String}
end
struct IntegerScheme <: Scheme end
struct FloatScheme <: Scheme end
struct BooleanScheme <: Scheme end

const INDENTATION = 2
function _show(x::Scheme, indentation=INDENTATION, max_indentation=10000)
    println(x)
end
# TODO also print the actual defs instead of only the names
function _show(v::RecursiveRefScheme, indentation=INDENTATION, max_indentation=10000)
    println("RecursiveRefScheme(")
    println(repeat(" ", indentation)*"defs = $(collect(keys(v.defs)))")
    println(repeat(" ", indentation)*"ref = $(v.ref)")
    println(repeat(" ", indentation-INDENTATION)*")")
end
function _show(v::ValueScheme, indentation=INDENTATION, max_indentation=10000)
    println("ValueScheme(")
    println(repeat(" ", indentation)*"format = $(v.format)")
    println(repeat(" ", indentation)*"name = $(v.name)")
    println(repeat(" ", indentation)*"description = $(v.description)")
    println(repeat(" ", indentation)*"initializer = $(v.initializer)")
    println(repeat(" ", indentation)*"autoupdater = $(v.autoupdater)")
    print(repeat(" ", indentation)*"scheme = ")
    _show(v.scheme, indentation+INDENTATION, max_indentation)
    println(repeat(" ", indentation-INDENTATION)*")")
end
function _show(x::ObjectScheme, indentation=0, max_indentation=10000)
    println("ObjectScheme(")
    if indentation < max_indentation
        for (k,v) in pairs(x.properties)
            print(repeat(" ", indentation)*"$k$(k in x.required ? "*" : "") = ")
            _show(v, indentation+INDENTATION, max_indentation)
        end
    else
        println(repeat(" ", indentation)*"...")
    end
    println(repeat(" ", indentation-INDENTATION)*")")
end
function _show(x::ArrayScheme, indentation=0, max_indentation=10000)
    println("ArrayScheme(")
    if indentation < max_indentation
        print(repeat(" ", indentation)*"items = ")
        _show(x.items, indentation+INDENTATION, max_indentation)
    else
        println(repeat(" ", indentation)*"...")
    end
    println(repeat(" ", indentation-INDENTATION)*")")
end
function _show(x::OneOfScheme, indentation=0, max_indentation=10000)
    println("OneOfScheme(")
    if indentation < max_indentation
        println(repeat(" ", indentation)*"discriminator = $(x.discriminator)")
        for (k,v) in pairs(x.possible_objects)
            print(repeat(" ", indentation)*"$k = ")
            _show(v, indentation+INDENTATION, max_indentation)
        end
    else
        println(repeat(" ", indentation)*"...")
    end
    println(repeat(" ", indentation-INDENTATION)*")")
end
function _show(x::AllOfScheme, indentation=0, max_indentation=10000)
    println("AllOfScheme(")
    if indentation < max_indentation
        for (i,v) in pairs(x.merge_objects)
            print(repeat(" ", indentation)*"$i. ")
            _show(v, indentation+INDENTATION, max_indentation)
        end
    else
        println(repeat(" ", indentation)*"...")
    end
    println(repeat(" ", indentation-INDENTATION)*")")
end

find_ref(scheme_root::AbstractDict, scheme::AbstractDict) = find_ref(scheme_root, scheme["\$ref"])
function find_ref(scheme_root::AbstractDict, ref_name::String)
    # for now only refs in the same file (#/) are supported
    @assert startswith(ref_name, "#/")
    # skip the #
    ref_path = split(ref_name, "/")[2:end]
    result = scheme_root
    for path_part in ref_path
        result = result[path_part]
    end
    result
end

#parse_ref(scheme_root, scheme, key) = parse_scheme(scheme_root, find_ref(scheme_root, scheme), split(scheme["\$ref"], "/")[end])
function parse_ref(scheme_root, scheme, key, recursive)
    ref_scheme = find_ref(scheme_root, scheme)
    if isnothing(recursive)
        return parse_scheme(scheme_root, ref_scheme, key, nothing)
    else
        ref = refpath(scheme)
        if !(ref in keys(recursive))
            recursive[ref] = nothing
            recursive[ref] = parse_scheme(scheme_root, ref_scheme, key, recursive)
        end
        return RecursiveRefScheme(recursive, ref)
    end
end

refpath(refscheme::AbstractDict) = refscheme["\$ref"]
refname(refscheme::AbstractDict) = refname(refscheme["\$ref"])
refname(refvalue::AbstractString) = last(split(refvalue, "/"))

#function parse_oneOf(scheme_root, scheme)
#    Ts = parse_scheme.(Ref(scheme_root), scheme["oneOf"])
#    T = Tuple{Ts...}
#    r = ValueScheme{OneOfScheme{T}, typeof(str2format(get(scheme, "format", "none")))}
#    return r
#end
#parse_discriminator(scheme::AbstractDict) = parse_discriminator(scheme["discriminator"], refnames)
parse_discriminator(discriminator::AbstractString, refnames::AbstractVector{<:AbstractString}) = (;
    propertyName=Symbol(discriminator),
    mapping_rev=Dict(refnames .=> Symbol.(refnames))
)
function parse_discriminator(discriminator::AbstractDict, refnames::AbstractVector{<:AbstractString})
    optional_mapping = get(discriminator, "mapping", Dict())
    optional_refname2mapname = Dict(refname(v) => k for (k,v) in optional_mapping)
    return (;
        propertyName=Symbol(discriminator["propertyName"]),
        mapping_rev=Dict(refnames .=> Symbol.(get.(Ref(optional_refname2mapname), refnames, refnames)))
    )
end
function parse_oneOf(scheme_root, scheme, key, recursive)
    refnames = refname.(scheme["oneOf"])
    discriminator = parse_discriminator(scheme["discriminator"], refnames)
    # the oneOf list must consist of only refs, so we call the the parse_ref function directly instead of the parse_scheme function
    oneOf_objects = parse_ref.(Ref(scheme_root), scheme["oneOf"], refnames, Ref(recursive))
    discriminator_value2one_of = NamedTuple(zip(getindex.(Ref(discriminator.mapping_rev), refnames), oneOf_objects))
    r = ValueScheme(OneOfScheme(discriminator_value2one_of, discriminator.propertyName), scheme, key)
    return r
end
function parse_allOf(scheme_root, scheme, key, recursive)
    # allOf items should always resolve to a list of objects so we will not use
    # the parse_scheme function and therefore also possibly indirectly the
    # parse_ref function which could recursivly parse the object (which should
    # not happend for references occuring in allOf lists)
    allOf_objects = []
    for allOf_scheme_i in scheme["allOf"]
        if "\$ref" in keys(allOf_scheme_i)
            key_i = refname(allOf_scheme_i)
            allOf_scheme_i = find_ref(scheme_root, allOf_scheme_i)
        else
            key_i = key
        end
        push!(allOf_objects, parse_object(scheme_root, allOf_scheme_i, key_i, recursive))
    end
    return ValueScheme(AllOfScheme(Tuple(allOf_objects)), scheme, key)
end
parse_string(scheme_root, scheme, key, recursive) = ValueScheme("enum" in keys(scheme) ? StringEnumScheme(scheme["enum"]) : StringScheme(), scheme, key)
parse_integer(scheme_root, scheme, key, recursive) = ValueScheme(IntegerScheme(), scheme, key)
parse_float(scheme_root, scheme, key, recursive) = ValueScheme(FloatScheme(), scheme, key)
parse_boolean(scheme_root, scheme, key, recursive) = ValueScheme(BooleanScheme(), scheme, key)
function parse_array(scheme_root, scheme, key, recursive)
    items = parse_scheme(scheme_root, scheme["items"], "item of $key", recursive)
    return ValueScheme(ArrayScheme(items), scheme, key)
end
function parse_object(scheme_root, scheme, key, recursive)
    properties = NamedTuple(zip(
        Symbol.(keys(scheme["properties"])),
        parse_scheme.(Ref(scheme_root), values(scheme["properties"]), keys(scheme["properties"]), Ref(recursive))
    ))
    required = Symbol.(get(scheme, "required", Symbol[]))
    return ValueScheme(ObjectScheme(properties, required), scheme, key)
end
function parse_scheme(scheme_root::AbstractDict, scheme::AbstractDict, key="root", recursive=nothing)
    #println()
    #println(scheme)
    #println()
    if isnothing(recursive) && (
            "editor" in keys(scheme) &&
            "recursiveRef" in keys(scheme["editor"]) &&
            scheme["editor"]["recursiveRef"] )
        recursive = Dict{String, Union{ValueScheme, Nothing}}()
    end
    if "\$ref" in keys(scheme)
        return parse_ref(scheme_root, scheme, key, recursive)
    elseif "oneOf" in keys(scheme)
        return parse_oneOf(scheme_root, scheme, key, recursive)
    elseif "allOf" in keys(scheme)
        return parse_allOf(scheme_root, scheme, key, recursive)
    elseif scheme["type"] == "string"
        return parse_string(scheme_root, scheme, key, recursive)
    elseif scheme["type"] == "integer"
        return parse_integer(scheme_root, scheme, key, recursive)
    elseif scheme["type"] == "number"
        return parse_float(scheme_root, scheme, key, recursive)
    elseif scheme["type"] == "boolean"
        return parse_boolean(scheme_root, scheme, key, recursive)
    elseif scheme["type"] == "array"
        return parse_array(scheme_root, scheme, key, recursive)
    elseif scheme["type"] == "object"
        return parse_object(scheme_root, scheme, key, recursive)
    end
    error("not implemented: $scheme")
end

const (
    item_scheme,
    basis_scheme,
    item_types,
    content_types,
    contentblock_types,
) = let
    root = YAML.load_file(SCHEME_PATH, dicttype=OrderedDict{String, Any})
    item_scheme = parse_scheme(root, root["Item"])
    (
        item_scheme,
        parse_scheme(root, root["Basis"]),
        collect(string.(keys(item_scheme.scheme.possible_objects))),
        collect(string.(keys(parse_scheme(root, root["Content"]).scheme.items.scheme.possible_objects))),
        collect(string.(keys(parse_scheme(root, root["ContentBase"]).scheme.properties.ContentBlocks.scheme.items.scheme.possible_objects))),
    )
end

#_show(item_scheme, 2, 10)

const languages = [
  "nl-NL",
  "en-GB",
  "fr-FR",
  "es-ES",
]

const type2fields = OrderedDict(
    k=>vcat(collect((x->collect(string.(keys(x.scheme.properties)))).(v.scheme.merge_objects))...)
    for (k,v) in pairs(item_scheme.scheme.possible_objects)
)


new_contentcontainer(content_id) = OrderedDict("ContentId"=>content_id,
                                               "ContentType"=>first(content_types),
                                               "ContentBlocks" => OrderedDict[])

new_contentblock() = OrderedDict("ContentBlockId"=>abs(rand(Int)),
                                 "ContentBlockType"=>first(contentblock_types),
                                 "Body"=>"")

end # module

