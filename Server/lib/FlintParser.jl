module FlintParser

using HTTP
using JSON3
using DataFrames
using OrderedCollections

import HierarchicalConfigs: dataframe2dictvec, dictvec2dataframe

function load_json_url(url)
    resp = HTTP.get(url)
    JSON3.read(resp.body)
end

function load_json_file(filepath)
    open(filepath, "r") do f
        JSON3.read(f)
    end
end

function flintdict2framestable(flintdict)
    df = DataFrame(flintdict[:frames])
    df.subdivision_frame = get.(df.subdivision, :frame, missing)
    df.subdivision_isNegated = get.(df.subdivision, :isNegated, missing)
    df.subdivision_children = get.(df.subdivision, :children, missing)
    df.subdivision_children_frames = Broadcast.BroadcastFunction(get).(get.(df.subdivision, :children, missing), :frame, missing)
    df.subdivision_operatorToJoinChildren = get.(df.subdivision, :operatorToJoinChildren, missing)
    select!(df,
        "id",
        "label",
        "fact",
        "typeId",
        "subTypeId",
        "annotations",
        "comments",
        "isComplex",
        #"subdivision",
        "subdivision_frame",
        "subdivision_isNegated",
        #"subdivision_children",
        "subdivision_children_frames",
        "subdivision_operatorToJoinChildren",
    )
    single_frame_subdivisions = .!ismissing.(df.subdivision_frame) .&
                                isempty.(df.subdivision_children_frames) .&
                                isnothing.(df.subdivision_operatorToJoinChildren)
    df.subdivision_children_frames = ifelse.(
        single_frame_subdivisions,
        (x->[x]).(df.subdivision_frame),
        df.subdivision_children_frames)
    df.subdivision_operatorToJoinChildren = ifelse.(
        single_frame_subdivisions,
        "single frame",
        df.subdivision_operatorToJoinChildren)
    select!(df, Not(:subdivision_frame))
    df.annotations = copy.(df.annotations)
    df.mark = "mark-" .* string.(0:nrow(df)-1, base=36)
    return dataframe2dictvec(df)
end

end # module
