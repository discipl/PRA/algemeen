using Arrow
using DataFrames

const PROCESSED_DATA_PATH = joinpath("data", "processed")

const levensgebeurtenissen = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "levensgebeurtenissen.arrow")))
const lv_questions = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "levensgebeurtenissen_questions.arrow")))
const lv_content = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "levensgebeurtenissen_content.arrow")))

const sc = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "sc.arrow")))
const upl = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "upl.arrow")))
const upl2sc = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "upl2sc.arrow")))
const sdg = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "sdg.arrow")))
const grondslagen = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "grondslagen.arrow")))

sc.Source .= "SamenwerkendeCatalogi"
sc.Levensgebeurtenissen .= Ref(Int[])
sc.levensgebeurtenis_id .= missing
sc.subTitle .= nothing

bool_cols = eltype.(eachcol(upl)) .== Bool

upl.Categories = getindex.(Ref(names(upl)[bool_cols]), eachrow(hcat(eachcol(upl[:,bool_cols])...)))

upl2SdgInfoGebList = combine(groupby(innerjoin(flatten(upl[:, [:UPL_Alpha, :SDG_Code]], :SDG_Code), sdg, on=:SDG_Code), :UPL_Alpha), :SDG_Informatiegebied => Ref ∘ unique)

leftjoin!(upl, upl2SdgInfoGebList, on=:UPL_Alpha)
replace!(upl.SDG_Informatiegebied_Ref_unique, missing => String[])
disallowmissing!(upl, :SDG_Informatiegebied_Ref_unique)

leftjoin!(sc, upl[:, [:UPL_Alpha, :Categories, :SDG_Informatiegebied_Ref_unique]], on=:Onderwerp_UPL_Alpha=>:UPL_Alpha, matchmissing=:equal)
replace!(sc.Categories, missing => String[])
replace!(sc.SDG_Informatiegebied_Ref_unique, missing => String[])
disallowmissing!(sc, [:SDG_Informatiegebied_Ref_unique, :Categories])

sc.Subjects = vcat.(sc.Subject, sc.Categories, sc.SDG_Informatiegebied_Ref_unique)

lv_content.title_Alpha = replace.(lowercase.(lv_content.title), r"[^a-z0-9]"=>"")
lv_content.recordPosition = rownumber.(eachrow(lv_content))
lv_content.Source .= "ContentToolsRijksoverheid"

tovec(x) = [x]
lv_content.Levensgebeurtenissen = tovec.(lv_content.levensgebeurtenis_id)
lv_content.UPL_Alpha .= Ref(String[])
lv_content.Onderwerp_UPL_Alpha .= missing
lv_content.authorityScheme .= "Ministerie"
lv_content.audience__ .= "particulier"

qa = flatten(lv_questions, [:answerOptions__id, :answerOptions__text, :answerOptions__feedback, :answerOptions__helpText])
qa.QA = qa.text .* " = " .* qa.answerOptions__text

qa[:, [:id, :answerOptions__id, :QA]]

lv_content_qa = flatten(lv_content[:, [:recordPosition, :relevantFor__questionId, :relevantFor__answerOptionIds]], [:relevantFor__questionId, :relevantFor__answerOptionIds])
lv_content_qa = flatten(lv_content_qa, :relevantFor__answerOptionIds)

leftjoin!(lv_content_qa, qa[:, [:id, :answerOptions__id, :QA]], on=[:relevantFor__questionId => :id, :relevantFor__answerOptionIds => :answerOptions__id])
disallowmissing!(lv_content_qa, :QA)

#combine(groupby(lv_content_qa, :recordPosition), :QA => Ref)

leftjoin!(lv_content, combine(groupby(lv_content_qa, :recordPosition), :QA => Ref), on=:recordPosition)

lv_content.Subjects = vcat.(
    ifelse.(isnothing.(lv_content.group1), Ref(String[]), tovec.(lv_content.group1)),
    ifelse.(isnothing.(lv_content.group2), Ref(String[]), tovec.(lv_content.group2)),
    ifelse.(isnothing.(lv_content.group3), Ref(String[]), tovec.(lv_content.group3)),
    lv_content.QA_Ref,
)

leftjoin!(lv_content, levensgebeurtenissen[:, [:levensgebeurtenis_id, :language, :organisation, :publicationDate]], on=:levensgebeurtenis_id)

content = vcat(
    select(sc,         :title, :title_Alpha, :Onderwerp_UPL_Alpha, :UPL_Alpha, :Levensgebeurtenissen, :levensgebeurtenis_id, :Source, :recordPosition, :language,
           :authorityScheme, :authority__  => :Authority, :audience__, :modified        => :Modified, :subTitle, :abstract => :Description, :Subjects, :identifier => :Source_URL),
    select(lv_content, :title, :title_Alpha, :Onderwerp_UPL_Alpha, :UPL_Alpha, :Levensgebeurtenissen, :levensgebeurtenis_id, :Source, :recordPosition, :language,
           :authorityScheme, :organisation => :Authority, :audience__, :publicationDate => :Modified, :subTitle, :bodyText => :Description, :Subjects, :Request_URL => :Source_URL)
)

content.UPL_Alpha = convert.(Vector{String}, content.UPL_Alpha)
disallowmissing!(content, [:language, :Authority, :Modified, :subTitle])

transform!(groupby(content, :title_Alpha),
    nrow => :N_Onderwerp_Contents,
    groupindices => :Onderwerp_Id,
    :Levensgebeurtenissen => Ref ∘ unique ∘ Iterators.flatten => :Levensgebeurtenissen,
)

content.Short_Description .= content.Description
content.Subjects = filter.(!ismissing, content.Subjects)
content.Subjects = convert.(Vector{String}, coalesce.(content.Subjects, Ref(String[])))
content.Content_Id = rownumber.(eachrow(content))

# extract Grondslagen from UPL

Grondslag = NamedTuple{(:label, :grondslag, :link)}

upl2grondslagen = combine(groupby(grondslagen, :UPL_Alpha),
        AsTable([:Grondslaglabel, :Grondslag, :Grondslaglink]) =>
            Ref ∘ ByRow(x->Grondslag(collect(x))) =>
            :Grondslagen
)
#upl2grondslagen = combine(groupby(grondslagen, :UPL_Alpha),
#        AsTable([:Grondslaglabel, :Grondslag, :Grondslaglink]) =>
#            Ref ∘ ByRow(x->Dict(zip([:label, :grondslag, :link], collect(x)))) =>
#            :Grondslagen
#)

#using OrderedCollections
#
#upl2grondslagen = combine(groupby(grondslagen, :UPL_Alpha),
#        AsTable([:Grondslaglabel, :Grondslag, :Grondslaglink]) =>
#            Ref ∘ ByRow(OrderedDict ∘ pairs) =>
#            :Grondslagen
#)

#using Dictionaries
#
#upl2grondslagen = combine(groupby(grondslagen, :UPL_Alpha),
#        AsTable([:Grondslaglabel, :Grondslag, :Grondslaglink]) =>
#            Ref ∘ ByRow(Dictionary) =>
#            :Grondslagen
#)

upl2grondslagen.Grondslagen = convert.(Vector{Grondslag}, upl2grondslagen.Grondslagen)
#
#upl2grondslagen.Grondslagen = convert(Vector{Vector{NamedTuple{(:label, :grondslag, :link)}}}, upl2grondslagen.Grondslagen)
#
#upl2grondslagen.Grondslagen = convert(Vector{Vector{NamedTuple{(:label, :grondslag, :link), Tuple{String, String, Union{String, Missing}}}}}, upl2grondslagen.Grondslagen)

a = leftjoin(flatten(content[:, [:UPL_Alpha, :Content_Id]], :UPL_Alpha), upl2grondslagen, on=:UPL_Alpha)
filter!(:Grondslagen => !ismissing, a)
a = combine(groupby(a, :Content_Id), :Grondslagen => Ref ∘ collect ∘ Iterators.flatten => :Grondslagen)

#a.Grondslagen = convert.(Vector{NamedTuple}, a.Grondslagen)

leftjoin!(content, a, on=:Content_Id)
content.Grondslagen = coalesce.(content.Grondslagen, Ref(NamedTuple[]))
#content.Grondslagen = coalesce.(content.Grondslagen, Ref(OrderedDict[]))
#content.Grondslagen = coalesce.(content.Grondslagen, Ref(Dictionary[]))
#content.Grondslagen = convert(Vector{Vector{OrderedDict}}, content.Grondslagen)
#content.Grondslagen = convert(Vector{Vector{Dictionary{Symbol, Union{Missing, String}}}}, content.Grondslagen)

# extract search Terms from text

using PythonCall
const html = pyimport("html")

tfidf = content[:, [:Content_Id]]
tfidf.Word = split.(replace.(pyconvert.(String, html.unescape.(content.Description)), r"\.|:|\?|!|,|/|;|\(|\)|'|\"|<.*?>"=>" "))
tfidf = flatten(tfidf, :Word)
tfidf.Word = lowercase.(tfidf.Word)
transform!(groupby(tfidf, [:Content_Id, :Word]), nrow => :Word_In_Doc_Frequency)
transform!(groupby(tfidf, :Content_Id), nrow => :N_Content_Words)

tfidf.TF = log1p.(tfidf.Word_In_Doc_Frequency ./ tfidf.N_Content_Words)

transform!(groupby(tfidf, :Word), :Content_Id => length ∘ unique => :N_Docs_Which_Contain_Word, nrow => :N_Words)

tfidf.IDF = length(unique(tfidf.Content_Id)) ./ tfidf.N_Docs_Which_Contain_Word
#tfidf.IDF = log1p.(length(unique(tfidf.Content_Id)) ./ tfidf.N_Words)

tfidf.TFIDF = tfidf.TF .* tfidf.IDF

tfidf = sort(unique(tfidf), [:Content_Id, :TFIDF], rev=true)

tfidf = tfidf[tfidf.TFIDF .> 1.0, :]

searchterms = combine(groupby(tfidf, :Content_Id), :Word => Ref => :Terms)

leftjoin!(content, searchterms, on=:Content_Id)
content.Terms = convert.(Vector{String}, coalesce.(content.Terms, Ref(String[])))
disallowmissing!(content, :Terms)

Arrow.write(joinpath(PROCESSED_DATA_PATH, "content.arrow"), content)
