using JSON3, DataFrames, PythonCall

ministeries_en = DataFrame(vcat(JSON3.read.(readdir("data/raw/minaz/ministeries/en", join=true))...))
ministeries_nl = DataFrame(vcat(JSON3.read.(readdir("data/raw/minaz/ministeries/nl", join=true))...))

onderwerpen_en = begin
    rows = JSON3.read.(readdir("data/raw/minaz/docs/onderwerpen/en/", join=true))
    all_keys = unique(vcat(collect.(keys.(rows))...))
    default_row = Dict(x=>missing for x in all_keys)
    rows = mergewith.(coalesce, Ref(default_row), rows) 
    df = DataFrame(rows)
    # Bij sommige rijen staat "nl-NL" terwijl alle tekst in het engels is.
    df.language .= "en-GB"
    unique!(df)
end

onderwerpen_nl = begin
    rows = JSON3.read.(readdir("data/raw/minaz/docs/onderwerpen/nl/", join=true))
    all_keys = unique(vcat(collect.(keys.(rows))...))
    default_row = Dict(x=>missing for x in all_keys)
    rows = mergewith.(coalesce, Ref(default_row), rows) 
    df = DataFrame(rows)
    #df.language .= "nl-NL"
    unique!(df)
end


df = select(vcat(onderwerpen_nl, onderwerpen_en), :themes, :organisationalunits, :introduction, :title, :name, :authorities, :language, :lastmodified)

df.themes = convert.(Vector{String}, coalesce.(df.themes, Ref([])))
#themes = union(df.themes...)

#df.ministeries = union.(df.organisationalunits, df.authorities)
df.ministeries = df.organisationalunits
df.ministeries = coalesce.(df.ministeries, Ref([]))
select!(df, :lastmodified, :language, :name, :title, :themes, :ministeries)

#ministeries = union(df.ministeries...)

#df = flatten(df, :themes)
#df = flatten(df, :ministeries)

using YAML
using OrderedCollections

const SUBJECTS_PATH = joinpath("..", "data", "onderwerp")
mkpath(SUBJECTS_PATH)

for row in eachrow(df)
    dir = mkpath(joinpath(SUBJECTS_PATH, row.language))
    filename = joinpath(dir, "$(row.name).yaml")
    YAML.write_file(filename, OrderedDict(pairs(row)))
end

subjects_df = df

function filter_subjects(themes::Vector{String}, ministeries::Vector{String}, subjects::Vector{String})
    #filtered = df[(∋).(Ref(themes), df.themes) .& (∋).(Ref(ministeries), df.ministeries), :]
    filtered = subjects_df[
        issubset.(Ref(themes), subjects_df.themes) .& 
        issubset.(Ref(ministeries), subjects_df.ministeries), :]
    if !isempty(subjects) 
        filtered = filtered[in.(filtered.title, Ref(subjects)), :]
    end
    themes = union(filtered.themes...)
    ministeries = union(filtered.ministeries...)
    return Dict(:themes => themes, :ministeries => ministeries, :subjects => filtered.title)
end
