module Content

using Genie, Genie.Requests, Genie.Renderer.Json
using SwaggerMarkdown
using DataFrames
using PythonCall
import Distances: pairwise, CosineDist

import ..Main.DataModel
import ..Main.SchemeModel
import ..Browse
import ..Main.GlobalConstants.SCHEME_LOCATION

content_response = """
    responses:
      '200':
        description: OK
        content:
          application/json:
            schema:
              type: object
              properties:
                filter_ids:
                  type: array
                  description: Ids used to filter.
                relevant_ids:
                  type: array
                  description: Ids used to for relevance.
                searchquery:
                  type: string
                  description: Used searchquery.
                filters:
                  type: array
                  description: The used filters.
                themes:
                  type: array
                  description: Themes used to filter.
                theme_subjects:
                  type: array
                  description: Used subjects to filter, according to the given themes.
                from:
                  type: integer
                  description: Index of the first record of the filterd content included in this response.
                to:
                  type: integer
                  description: Index of the last record of the filterd content included in this response.
                totalResults:
                  type: integer
                  description: The total number of records which resulted from given filter.
                results:
                  description: The resulting table as an array of objects.
                  type: array
                  items:
                    \$ref: '$SCHEME_LOCATION#/Zoekdescriptor'
"""

# Docs in Open API v3.0 format
@swagger """
/content:
  get:
    description: Get PRA content according to the given criteria.
    parameters:
      - in: query
        name: filter_ids
        schema:
          type: array
          items:
            type: integer
        style: pipeDelimited
        required: false
        description: List of ids to filter on. The order will be changed when also a searchquery is given. Otherwise, the returned documents will have the same order as the ids this list.
      - in: query
        name: relevant_ids
        schema:
          type: array
          items:
            type: integer
        style: pipeDelimited
        required: false
        description: List of ids which must be relevant for the search results.
      - in: query
        name: searchquery
        schema:
          type: string
          default: ""
        required: false
        description: The search query to rank the results with. When multiple pieces of contents are available for the same topic, only the highest ranked will be returned.
      - in: query
        name: filters
        schema:
          type: array
          items:
            type: string
        style: pipeDelimited
        required: false
        description: List of filters which must be present in the results.
      - in: query
        name: themes
        schema:
          type: array
          items:
            type: string
            example: "Economie"
        style: pipeDelimited
        required: false
        description: List of themes of which the results must be part of.
      - in: query
        name: type_filter
        schema:
          type: string
          default: ""
          example: "Regeling"
        required: false
        description: The PRA-model type of which all returned items must be part of.
      - in: query
        name: start
        schema:
          type: integer
          minimum: 1
          default: 1
        required: false
        description: The row number of the first result which will be included in the response. (Useful for pagination)
      - in: query
        name: n
        schema:
          type: integer
          minimum: 1
          default: 10
        required: false
        description: The number of results to be included in the response. (Useful for pagination)
$content_response
  post:
    summary: Get content according to some criteria.
    requestBody:
        description: Criteria for the wanted content.
        required: false
        content:
          application/json:
            schema:
              type: object
              properties:
                filter_ids:
                  description: List of ids to filter on. The order will be changed when also a searchquery is given. Otherwise, the returned documents will have the same order as the ids this list.
                  type: array
                  items:
                    type: integer
                  default: []
                relevant_ids:
                  description: List of ids which must be relevant for the search results.
                  type: array
                  items:
                    type: integer
                  default: []
                searchquery:
                  description: The search query to rank the results with. When multiple pieces of contents are available for the same topic, only the highest ranked will be returned.
                  type: string
                  default: ""
                filters:
                  description: List of filters which must be present in the results.
                  type: array
                  items:
                    type: string
                  default: []
                themes:
                  description: List of themes of which the results must be part of.
                  type: array
                  items:
                    type: string
                  default: []
                  example: ["Familie, zorg en gezondheid", "Economie"]
                type_filter:
                  description: The PRA-model type of which all returned items must be part of.
                  type: string
                  default: ""
                  example: "Levensgebeurtenis"
                start:
                  description: The row number of the first result which will be included in the response. (Useful for pagination)
                  type: integer
                  minimum: 1
                  default: 1
                n:
                  description: The number of results to be included in the response. (Useful for pagination)
                  type: integer
                  minimum: 1
                  default: 10
$content_response
"""

route("/content", method = GET) do
    filter_ids = parse.(Int, split(getpayload(:filter_ids, ""), "|", keepempty=false))
    relevant_ids = parse.(Int, split(getpayload(:relevant_ids, ""), "|", keepempty=false))
    searchquery = getpayload(:searchquery, "")
    filters = convert(Vector{String}, split(getpayload(:filters, ""), "|", keepempty=false))
    themes = convert(Vector{String}, split(getpayload(:themes, ""), "|", keepempty=false))
    type_filter = getpayload(:type_filter, "")
    start = parse(Int, getpayload(:start, "1"))
    n = parse(Int, getpayload(:n, "10"))
    return content(filter_ids, relevant_ids, searchquery, filters, themes, type_filter, start, n)
end

route("/content", method = POST) do
    payload_dict = jsonpayload()
    if isnothing(payload_dict)
        payload_dict = Dict{String, Any}()
    end
    filter_ids = convert(Vector{Int}, copy(get(payload_dict, "filter_ids", Int[])))
    relevant_ids = convert(Vector{Int}, copy(get(payload_dict, "relevant_ids", Int[])))
    searchquery = get(payload_dict, "searchquery", "")
    filters = convert(Vector{String}, copy(get(payload_dict, "filters", String[])))
    themes = convert(Vector{String}, copy(get(payload_dict, "themes", String[])))
    type_filter = get(payload_dict, "type_filter", "")
    start = get(payload_dict, "start", 1)
    n = get(payload_dict, "n", 10)
    return content(filter_ids, relevant_ids, searchquery, filters, themes, type_filter, start, n)
end

function content(filter_ids::Vector{Int}, relevant_ids::Vector{Int}, searchquery::String, filters::Vector{String},
                 themes::Vector{String}, type_filter::String, start::Int, n::Int)
    theme_subjects = String[(Browse.browse(String[], [t], String[], String[])[:subjects] for t in themes)...;]
    filtered_content = get_filtered_content_ranking(filter_ids, relevant_ids, searchquery, filters, theme_subjects, type_filter)
    unique!(filtered_content, SchemeModel.UNIQUE_RESULTS_COL_NAME)
    results = filtered_content[start:(min(end, start+n)), :] |> Tables.namedtupleiterator |> collect
    return json((;
        filter_ids = filter_ids,
        relevant_ids = relevant_ids,
        searchquery = searchquery,
        filters = filters,
        themes = themes,
        theme_subjects = theme_subjects,
        type_filter = type_filter,
        from = start,
        to = min(nrow(filtered_content), start+n),
        totalResults = nrow(filtered_content),
        results = results,
    ))
end

function get_filtered_content_ranking(
            filter_ids::Vector{Int}, relevant_ids::Vector{Int}, searchquery::String, all_filters::Vector{String}, any_filters::Vector{String}, type_filter::String)
    filter_mask = issubset.(Ref(all_filters), DataModel.zoekdescriptors[!,SchemeModel.FILTER_COL_NAME]) .& (
        .~isdisjoint.(Ref(any_filters), DataModel.zoekdescriptors[!,SchemeModel.FILTER_COL_NAME]) .|
        isempty(any_filters)
    )
    filter_ids_mask = (.~isdisjoint.(Ref(filter_ids), DataModel.zoekdescriptors[!, :Id])) .| isempty(filter_ids)
    # boolean mask for zoekdescriptors for rows which are relevant for the given relevant_ids
    relevant_ids_mask = begin
        # find all relevant models as strings
        model_starts = DataModel.zoekdescriptors[.~isdisjoint.(Ref(relevant_ids), DataModel.zoekdescriptors[!, :Id]), "Zoekdescriptor.Model"] |> unique
        # make boolean mask for zoekdescriptors for all specialisations of one of the found models
        model_mask = any.(Broadcast.BroadcastFunction(startswith).(DataModel.zoekdescriptors[:, "Zoekdescriptor.Model"],
                                                                   Ref(model_starts)))
        # find all relevant levensgebeurtenissen as levensgebeurtenis-ids
        levensgebeurtenissen_ids = DataModel.levensgebeurtenissen[.~isdisjoint.(Ref(relevant_ids), DataModel.levensgebeurtenissen[!, :Id]),
                                                                  "Levensgebeurtenis.Id"] |> unique
        # find ids of regelingen which are relevant for the found levensgebeurtenissen
        ids = DataModel.regelingen[.~isdisjoint.(Ref(levensgebeurtenissen_ids), DataModel.regelingen[:,"Regeling.Relevante_Levensgebeurtenissen"]), :Id]
        # make boolean mask for zoekdescriptors for regelingen which are relevant for the found levensgebeurtenissen
        levensgebeurtenissen_mask = DataModel.zoekdescriptors[:, :Id] .∈ Ref(ids)
        model_mask .| levensgebeurtenissen_mask .| isempty(relevant_ids)
    end
    # combine all filtering masks
    type_filter_mask = if isempty(type_filter)
        trues(nrow(DataModel.zoekdescriptors))
    else
        DataModel.zoekdescriptors[:, :Type] .== type_filter
    end
    mask = filter_mask .& filter_ids_mask .& relevant_ids_mask .& type_filter_mask
    # construct the subset of zoekdescriptors to search trough
    filtered_content = DataModel.zoekdescriptors[mask, :]
    searchterms = split(searchquery)
    if !isempty(searchterms)
        querymat = [pyconvert.(Vector{Float32}, DataModel.ft.get_word_vector.(lowercase.(searchterms)))...;;]
        termdists = pairwise(CosineDist(), querymat, DataModel.termmat)
        termrank = DataFrame(Term = DataModel.terms[!, SchemeModel.TERM_COL_NAME], Term_Scores = collect.(eachcol(1 .- termdists)))
        filtered_content_terms = flatten(DataFrames.select(filtered_content, :Id, SchemeModel.TERM_COL_NAME=>:Term), :Term)
        filtered_content_terms = innerjoin(filtered_content_terms, termrank, on=:Term)
        filtered_content_terms.Term_Scores = Broadcast.BroadcastFunction(exp).(filtered_content_terms.Term_Scores .* 10)
        filtered_content_terms.Query_Term_Score = sum.(filtered_content_terms.Term_Scores)
        # TODO remove this line also
        sort!(filtered_content_terms, :Query_Term_Score, rev=true)
        queryscores = combine(groupby(filtered_content_terms, :Id),
            :Query_Term_Score => sum => :Query_Content_Score,
            # For inspecing individual term scores TODO remove when deploy
            :Term => Ref,
            :Query_Term_Score => Ref,
            # TODO remove until here
        )
        leftjoin!(filtered_content, queryscores, on=:Id)
        filtered_content[ismissing.(filtered_content.Query_Content_Score), :Query_Content_Score] .= 0.0
        disallowmissing!(filtered_content, :Query_Content_Score)
        sort!(filtered_content, :Query_Content_Score, rev=true)
    end
    filtered_content
end

end # module
