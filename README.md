# PRA Algemeen

## Git Large File Storage

To correctly clone this repository you need to have `git lfs` installed. (cloning can take some time
because a 1.5 GB file will be downloaded automatically)

# Old Rasa backend

The old Rasa backend can be found at [tag:Deprecated-Rasa-backend](../tree/Deprecated-Rasa-backend?ref_type=tags)
