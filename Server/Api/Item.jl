module Item

using Genie, Genie.Requests, Genie.Renderer.Json
using SwaggerMarkdown
using DataFrames
using HierarchicalConfigs: dataframe2dictvec

import ..Main.DataModel
import ..Main.GlobalConstants.SCHEME_LOCATION


item_response = """
    responses:
      '200':
        description: OK
        content:
          application/json:
            schema:
              \$ref: '$SCHEME_LOCATION#/Item'
"""

# Docs in Open API v3.0 format
@swagger """
/item:
  get:
    description: Get single PRA-model.
    parameters:
      - in: query
        name: id
        schema:
          type: integer
          example: $(last(DataModel.zoekdescriptors.Id))
        required: true
        description: The id of the PRA-model.
$item_response
  post:
    summary: Get single PRA-model.
    requestBody:
        description: Criteria for the wanted item.
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                id:
                  description: The id of the PRA-model.
                  type: integer
                  example: $(first(DataModel.zoekdescriptors.Id))
$item_response
"""

route("/item", method = GET) do
    id = parse(Int, getpayload(:id))
    return item(id)
end

route("/item", method = POST) do
    payload_dict = jsonpayload()
    if isnothing(payload_dict) || !("id" in keys(payload_dict))
        return Genie.Renderer.Json.json(Dict("error" => "No id given"))
    end
    id = payload_dict["id"]
    return item(id)
end

function item(id::Int)
    found_rows_with_id = DataModel.zoekdescriptors[DataModel.zoekdescriptors.Id .== id, :]
    if nrow(found_rows_with_id) == 0 
        return Genie.Renderer.Json.json(Dict("error" => "No item with Id found. Maybe Complete is not set to true, or item has no Zoekdescriptor.Model?", "Id" => id))
    end
    z = first(found_rows_with_id)
    if z.Type == "Regeling"
        i = DataModel.regelingen[DataModel.regelingen.Id .== id, :]
    elseif z.Type == "Levensgebeurtenis"
        i = DataModel.levensgebeurtenissen[DataModel.levensgebeurtenissen.Id .== id, :]
    else
        return Genie.Renderer.Json.json(Dict("error" => "No supported Zoekdescriptor.Type, should be one of [\"Regeling\", \"Levensgebeurtenis\"]) Zoekdescriptor.Model", "Zoekdescriptor:" => dataframe2dictvec(z)))
    end
    return Genie.Renderer.Json.json(first(dataframe2dictvec(i)))
end

end # module
