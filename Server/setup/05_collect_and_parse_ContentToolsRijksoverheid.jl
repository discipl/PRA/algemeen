using HTTP
using JSON3
using DataFrames
import Base.Broadcast:BroadcastFunction
using Arrow

const PROCESSED_DATA_PATH = joinpath("data","processed")

# get 'index' page which (probably) contains all levensgebeurtenissen
filtertools_resp = HTTP.get("https://api.contenttoolsrijksoverheid.nl/v1/filtertools?page=0&size=10000")
filtertools_body = JSON3.read(filtertools_resp.body)

levensgebeurtenissen = DataFrame(filtertools_body._embedded.filtertools)
levensgebeurtenissen.levensgebeurtenis_id .= rownumber.(eachrow(levensgebeurtenissen))

function flatten_nested(col::AbstractVector{<:Union{Missing, AbstractDict}}, name)
    new_dfs = []
    for key in unique(Iterators.flatten(keys.(skipmissing(col))))
        push!(new_dfs, flatten_nested(passmissing(get).(col, key, missing), "$(name)__$(key)"))
    end
    return hcat(new_dfs...)
end
function flatten_nested(col::AbstractVector{<:AbstractDict}, name)
    new_dfs = []
    for key in unique(Iterators.flatten(keys.(col)))
        push!(new_dfs, flatten_nested(get.(col, key, missing), "$(name)__$(key)"))
    end
    return hcat(new_dfs...)
end
flatten_nested(col::Any, name) = DataFrame(name=>col)

function arrays_of_dicts2columns_of_arrays(col::AbstractVector{<:AbstractVector}, name)
    new_dfs = []
    for key in unique(Iterators.flatten(Iterators.flatten(BroadcastFunction(keys).(col))))
        push!(new_dfs, DataFrame("$(name)__$(key)" => BroadcastFunction(get).(col, key, missing)))
    end
    return hcat(new_dfs...)
end

for (name, col) in zip(names(levensgebeurtenissen), eachcol(levensgebeurtenissen))
    println(name," ", typeof(col))
    if eltype(col) <: AbstractDict
        DataFrames.hcat!(levensgebeurtenissen, flatten_nested(col, name) )
        select!(levensgebeurtenissen, Not(name))
    end
end

Arrow.write(joinpath(PROCESSED_DATA_PATH, "levensgebeurtenissen.arrow"), levensgebeurtenissen)

# Get questions of each levensgebeurtenis
questions_dfs = []
for levgeb_url in levensgebeurtenissen._links__self__href
    lg_resp = HTTP.get(levgeb_url)
    lg_body = JSON3.read(lg_resp.body)
    questions = DataFrame(lg_body.questions)
    questions.Request_URL .= levgeb_url
    cols_to_delete = []
    new_cols = []
    for (name, col) in zip(names(questions), eachcol(questions))
        println(name," ", typeof(col))
        if eltype(col) <: AbstractVector
            push!(cols_to_delete, name)
            try
                new_df = arrays_of_dicts2columns_of_arrays(col, name)
                push!(new_cols, new_df)
            catch e
            end
        end
    end
    DataFrames.hcat!(questions,  new_cols...)
    select!(questions, Not(cols_to_delete))
    push!(questions_dfs, questions)
    sleep(1)
end
questions_df = vcat(questions_dfs..., cols = :union, source = :levensgebeurtenis_id)

Arrow.write(joinpath(PROCESSED_DATA_PATH, "levensgebeurtenissen_questions.arrow"), questions_df)
questions_df2 = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "levensgebeurtenissen_questions.arrow")))

# Get content of each levensgebeurtenis
cb_dfs = []
for levgeb_url in levensgebeurtenissen._links__self__href
    println(levgeb_url)
    cb_resp = HTTP.get(levgeb_url * "/contentblocks")
    cb_body =  JSON3.read(cb_resp.body)
    df = DataFrame(cb_body._embedded.contentBlocks) 
    df.Request_URL .= levgeb_url
    push!(cb_dfs, df)
    sleep(1)
end
cb_df = vcat(cb_dfs..., cols = :union, source = :levensgebeurtenis_id)

DataFrames.hcat!(cb_df, arrays_of_dicts2columns_of_arrays(cb_df.relevantFor, "relevantFor"))
select!(cb_df, Not("relevantFor"))

cb_df.uniformProductContents = convert.(Vector{Any}, cb_df.uniformProductContents)
Arrow.write(joinpath(PROCESSED_DATA_PATH, "levensgebeurtenissen_content.arrow"), cb_df)
