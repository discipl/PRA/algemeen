using Arrow
using DataFrames

const PROCESSED_DATA_PATH = joinpath("data", "processed")

const sc = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "sc.arrow")))
const upl = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "upl.arrow")))
const content = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "content.arrow")))

h1_upls = ["digid", "heffingskortinguitbetaling", "loonbelasting", "onderwijsbijdrageenschoolkostenvotegemoetkoming", "orgaandonorregistratie", "studiefinanciering", "zorgtoeslag", "zorgverzekeringsplicht"]
h1_titles = ["digidaanvragen", "minderloonheffingvragenaanwerkgever", "aangifteinkomstenbelastingdoen", "tegemoetkomingscholierenaanvragen", "keuzemakenoverorgaandonatie", "studiefinancieringaanvragen", "zorgtoeslagaanvragen", "zorgverzekeringafsluiten"]

upl2onderwerpen = content[:, [:Onderwerp_UPL_Alpha, :title_Alpha]]
dropmissing!(upl2onderwerpen, :Onderwerp_UPL_Alpha)
upl2onderwerpen = vcat(upl2onderwerpen,
    DataFrame(
        Onderwerp_UPL_Alpha = h1_upls,
        title_Alpha = h1_titles
    )
)
upl2onderwerpen = combine(groupby(upl2onderwerpen, :Onderwerp_UPL_Alpha), :title_Alpha => Ref ∘ unique => :title_Alpha)

h1_content = flatten(leftjoin(DataFrame(
    Onderwerp_UPL_Alpha = h1_upls,
), upl2onderwerpen, on=:Onderwerp_UPL_Alpha), :title_Alpha)

h1_content = h1_content[.~in.(h1_content.title_Alpha, Ref(["eherkenning", "yivinieuwemaniervaninloggen", "individuelebegeleiding"])), :]

rename!(h1_content, :Onderwerp_UPL_Alpha => :UPL_Link)
#select!(h1_content, :title_Alpha)
#unique!(h1_content)
h1_content = leftjoin(h1_content, content, on=:title_Alpha)
sort!(h1_content, [:UPL_Link, :Onderwerp_Id, :title, :Onderwerp_UPL_Alpha])

disallowmissing!(h1_content, error=false)

Arrow.write(joinpath(PROCESSED_DATA_PATH, "h1_content.arrow"), h1_content)

#using CSV
#
#for n in names(h1_content)
#   println(n)
#   if eltype(h1_content[:, n]) <: AbstractArray
#       setproperty!(h1_content, Symbol(n), ifelse.(ismissing.(h1_content[:,n]) .|| isnothing.(h1_content[:,n]) .|| isempty.(h1_content[:,n]), "[]", string.(h1_content[:,n])))
#   end
#end
#
#h1_content[isnothing.(h1_content.subTitle) ,:subTitle] .= ""
#
#CSV.write("horizon1_content.csv", h1_content)

