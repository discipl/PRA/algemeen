using DataFrames
using Arrow

const PROCESSED_DATA_PATH = joinpath("data", "processed")
const PROCESSED_DATA_PATH_old = "../../Onderzoek-UPL/data/processed/upl2sc.arrow"

const sc = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "sc.arrow")))
const upl = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "upl.arrow")))

#upl.UPLns = replace.(upl.UPL, " " => "")

# many 2 many relation
upl2sc = flatten(select(sc, :recordPosition, 
#UPL, :UPL_Slug, 
:UPL_Alpha, 
#:UPL_resourceIdentifier,
:n_UPL), [
#:UPL, :UPL_Slug, 
:UPL_Alpha, 
    #:UPL_resourceIdentifier
])

#@assert lowercase.(last.(split.(upl2sc.UPL_resourceIdentifier, "/"))) == upl2sc.UPL_Slug

#upl2sc.UPLns = replace.(upl2sc.UPL, " " => "")

#leftjoin!(upl2sc, select(upl, :UPL, :UPL_Id), on=:UPL)
#leftjoin!(upl2sc, select(upl, :UPL_Slug, :UPL_Id=>:UPL_Id2), on=:UPL_Slug)
#leftjoin!(upl2sc, select(upl, :UPLns, :UPL_Id=>:UPL_Id3), on=:UPLns)

leftjoin!(upl2sc, select(upl, :UPL_Alpha, :UPL_Id), on=:UPL_Alpha)

sort!(upl2sc, :recordPosition)

Arrow.write(joinpath(PROCESSED_DATA_PATH, "upl2sc.arrow"), upl2sc)

const upl2sc_old = DataFrame(Arrow.Table("../../Onderzoek-UPL/data/processed/upl2sc.arrow"))

const upl2sc = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "upl2sc.arrow")))




transform!(groupby(upl_links, :UPL), nrow => :UPL_nrow)
transform!(groupby(upl_links, :UPL_Slug), nrow => :UPL_Slug_nrow)
transform!(groupby(upl_links, :UPLns), nrow => :UPLns_nrow)

leftjoin!(upl_links, select(upl, :UPL, :UPL_Id), on=:UPL)
leftjoin!(upl_links, select(upl, :UPL_Slug, :UPL_Id=>:UPL_Id2), on=:UPL_Slug)
leftjoin!(upl_links, select(upl, :UPLns, :UPL_Id=>:UPL_Id3), on=:UPLns)

