using DataFrames
using YAML
using OrderedCollections
using PythonCall
using Distances
using Slugify

using GlobFromPython: glob
using HierarchicalConfigs: load_hierachicaly, dataframe2dictvec

const PRA_MODELS_PATH = joinpath("..", "data", "vindbaar")
const SUBJECTS_PATH = joinpath("..", "data", "onderwerp")

subjects_df = DataFrame(YAML.load_file.(glob("*/*.yaml", SUBJECTS_PATH), dicttype=OrderedDict{String, Any}))

themes_df = unique(flatten(subjects_df[:, [:language, :themes]], :themes))
subject_theme_combinations = flatten(
    subjects_df[:, [:lastmodified, :language, :name, :title, :themes]],
    :themes)
rename!(subject_theme_combinations, :themes => "Categorie.Titel")
subject_theme_combinations[:, "Categorie.Soort"] .=  "Thema"

ministeries_df = unique(flatten(subjects_df[:, [:language, :ministeries]], :ministeries))
subject_ministery_combinations = flatten(
    subjects_df[:, [:lastmodified, :language, :name, :title, :ministeries]],
    :ministeries)
rename!(subject_ministery_combinations, :ministeries => "Categorie.Titel")
subject_ministery_combinations[:, "Categorie.Soort"] .=  "Ministerie"

subject_SoortTitel_combinations = vcat(subject_theme_combinations, subject_ministery_combinations)

subject_SoortTitel_combinations[!,:subject_id] .= groupindices(groupby(subject_SoortTitel_combinations, [:language, :name]))

df = combine(groupby(subject_SoortTitel_combinations,
                     ["Categorie.Soort", "Categorie.Titel"]),
             :lastmodified => maximum => :lastmodified,
             :language => unique => :language,
             [:subject_id, :name, :title]
                 => ((w,x,y) -> Ref(OrderedDict.("Type".=>"Onderwerp", "Id".=>w, "Handle".=>x, "Titel".=>y)))
                 => :subjects,
)

icon_mapping = DataFrame([
  ("nl-NL", "Overheid en democratie", :Politiek),
  ("nl-NL", "Recht, veiligheid en defensie", :BezwaarEnBeroep),
  ("nl-NL", "Bouwen en wonen", :Hoogbouw),
  ("nl-NL", "Familie, zorg en gezondheid", :KindEnFamilie),
  ("nl-NL", "Werk", :EconomieWerkInkomen),
  ("nl-NL", "Klimaat, milieu en natuur", :NatuurLandschap),
  ("nl-NL", "Internationale samenwerking", :Ondernemen),
  ("nl-NL", "Onderwijs", :Kennis),
  ("nl-NL", "Economie", :Inkomen),
  ("nl-NL", "Belastingen, uitkeringen en toeslagen", :Toeslag),
  ("nl-NL", "Verkeer en vervoer", :Auto),
  ("nl-NL", "Migratie en reizen", :PaspoortIdkaartGecombineerd),
  ("en-GB", "Government and democracy", :Politiek),
  ("en-GB", "Justice, security and defence", :BezwaarEnBeroep),
  ("en-GB", "Building and housing", :Hoogbouw),
  ("en-GB", "Family, health and care", :KindEnFamilie),
  ("en-GB", "Work", :EconomieWerkInkomen),
  ("en-GB", "Nature and the environment", :NatuurLandschap),
  ("en-GB", "International cooperation", :Ondernemen),
  ("en-GB", "Education", :Kennis),
  ("en-GB", "Economy", :Inkomen),
  ("en-GB", "Taxes, benefits and allowances", :Toeslag),
  ("en-GB", "Transport", :Auto),
  ("en-GB", "Migration and travel", :PaspoortIdkaartGecombineerd),
  ("en-GB", "Ministry of Justice and Security"                         , :BezwaarEnBeroep),
  ("en-GB", "Ministry of Defence"                                      , :Vlag),
  ("en-GB", "Ministry of Infrastructure and Water Management"          , :OverDeStad),
  ("en-GB", "Ministry of Social Affairs and Employment"                , :EconomieWerkInkomen),
  ("en-GB", "Ministry of Finance"                                      , :Begroting),
  ("en-GB", "Ministry of Economic Affairs and Climate Policy"          , :Grafiek),
  ("en-GB", "Ministry of Agriculture, Nature and Food Quality"         , :Voeding),
  ("en-GB", "Ministry of Education, Culture and Science"               , :Kennis),
  ("en-GB", "Ministry of Health, Welfare and Sport"                    , :Sport),
  ("en-GB", "Ministry of the Interior and Kingdom Relations"           , :Koningsdag),
  ("en-GB", "Ministry of Foreign Affairs"                              , :Ondernemen),
  ("en-GB", "Ministry of General Affairs"                              , :Politiek),
  ("nl-NL", "Ministerie van Justitie en Veiligheid"                    , :BezwaarEnBeroep),
  ("nl-NL", "Ministerie van Defensie"                                  , :Vlag),
  ("nl-NL", "Ministerie van Infrastructuur en Waterstaat"              , :OverDeStad),
  ("nl-NL", "Ministerie van Sociale Zaken en Werkgelegenheid"          , :EconomieWerkInkomen),
  ("nl-NL", "Ministerie van Financiën"                                 , :Begroting),
  ("nl-NL", "Ministerie van Economische Zaken en Klimaat"              , :Grafiek),
  ("nl-NL", "Ministerie van Landbouw, Natuur en Voedselkwaliteit"      , :Voeding),
  ("nl-NL", "Ministerie van Onderwijs, Cultuur en Wetenschap"          , :Kennis),
  ("nl-NL", "Ministerie van Volksgezondheid, Welzijn en Sport"         , :Sport),
  ("nl-NL", "Ministerie van Binnenlandse Zaken en Koninkrijksrelaties" , :Koningsdag),
  ("nl-NL", "Ministerie van Buitenlandse Zaken"                        , :Ondernemen),
  ("nl-NL", "Ministerie van Algemene Zaken"                            , :Politiek),
], ["language", "Categorie.Titel", "Categorie.Icon"])

leftjoin!(df, icon_mapping, on=["language", "Categorie.Titel"])

id_mapping = DataFrame([
  (1, "nl-NL", "Overheid en democratie"),
  (2, "nl-NL", "Recht, veiligheid en defensie"),
  (3, "nl-NL", "Bouwen en wonen"),
  (4, "nl-NL", "Familie, zorg en gezondheid"),
  (5, "nl-NL", "Werk"),
  (6, "nl-NL", "Klimaat, milieu en natuur"),
  (7, "nl-NL", "Internationale samenwerking"),
  (8, "nl-NL", "Onderwijs"),
  (9, "nl-NL", "Economie"),
  (10,"nl-NL", "Belastingen, uitkeringen en toeslagen"),
  (11,"nl-NL", "Verkeer en vervoer"),
  (12,"nl-NL", "Migratie en reizen"),
  (1, "en-GB", "Government and democracy"),
  (2, "en-GB", "Justice, security and defence"),
  (3, "en-GB", "Building and housing"),
  (4, "en-GB", "Family, health and care"),
  (5, "en-GB", "Work"),
  (6, "en-GB", "Nature and the environment"),
  (7, "en-GB", "International cooperation"),
  (8, "en-GB", "Education"),
  (9, "en-GB", "Economy"),
  (10,"en-GB", "Taxes, benefits and allowances"),
  (11,"en-GB", "Transport"),
  (12,"en-GB", "Migration and travel"),
  (13, "en-GB", "Ministry of Justice and Security"),
  (14, "en-GB", "Ministry of Defence"),
  (15, "en-GB", "Ministry of Infrastructure and Water Management"),
  (16, "en-GB", "Ministry of Social Affairs and Employment"),
  (17, "en-GB", "Ministry of Finance"),
  (18, "en-GB", "Ministry of Economic Affairs and Climate Policy"),
  (19, "en-GB", "Ministry of Agriculture, Nature and Food Quality"),
  (20, "en-GB", "Ministry of Education, Culture and Science"),
  (21, "en-GB", "Ministry of Health, Welfare and Sport"),
  (22,"en-GB", "Ministry of the Interior and Kingdom Relations"),
  (23,"en-GB", "Ministry of Foreign Affairs"),
  (24,"en-GB", "Ministry of General Affairs"),
  (13, "nl-NL", "Ministerie van Justitie en Veiligheid"),
  (14, "nl-NL", "Ministerie van Defensie"),
  (15, "nl-NL", "Ministerie van Infrastructuur en Waterstaat"),
  (16, "nl-NL", "Ministerie van Sociale Zaken en Werkgelegenheid"),
  (17, "nl-NL", "Ministerie van Financiën"),
  (18, "nl-NL", "Ministerie van Economische Zaken en Klimaat"),
  (19, "nl-NL", "Ministerie van Landbouw, Natuur en Voedselkwaliteit"),
  (20, "nl-NL", "Ministerie van Onderwijs, Cultuur en Wetenschap"),
  (21, "nl-NL", "Ministerie van Volksgezondheid, Welzijn en Sport"),
  (22,"nl-NL", "Ministerie van Binnenlandse Zaken en Koninkrijksrelaties"),
  (23,"nl-NL", "Ministerie van Buitenlandse Zaken"),
  (24,"nl-NL", "Ministerie van Algemene Zaken"),
], ["Categorie.Id", "language", "Categorie.Titel"])

leftjoin!(df, id_mapping, on=["language", "Categorie.Titel"])

df[:, "Categorie.Handle"] .= lowercase.(replace.(replace.(Slugify.replaceUnicode.(df[:,"Categorie.Titel"]), r"[\s,]" => "-"), "--" => "-"))

rename!(df, :language => "Categorie.Taal",
            :lastmodified => "Categorie.Laatst_Bewerkt_Op",
            :subjects => "Categorie.Filters")

df[:, "Compleet"] .= true
using Random
Random.seed!(prod(convert.(UInt, collect("FilterIds"))))
df[:, "Id"] .= abs.(rand(Int, nrow(df)))
df[:, "Wordt_Onderhouden_Door"] .=  "ICTU"
df[:, "Type"] .=  "Categorie"

select!(df,
    "Compleet",
    "Id",
    "Wordt_Onderhouden_Door",
    "Type",
    "Categorie.Laatst_Bewerkt_Op",
    "Categorie.Soort",
    "Categorie.Id",
    "Categorie.Icon",
    "Categorie.Taal",
    "Categorie.Handle",
    "Categorie.Titel",
    "Categorie.Filters",
)

id2dirname(id) = Dict(x=>y for (x,y) in (Tuple.(eachrow(df[df[:, "Categorie.Taal"] .== "nl-NL", ["Categorie.Id", "Categorie.Titel"]]))))[id]

filenames = joinpath.(PRA_MODELS_PATH,
                      "Categorie",
                      df[:, "Categorie.Soort"],
                      id2dirname.(df[:, "Categorie.Id"]),
                      df[:, "Categorie.Taal"],
                      string.(df.Id, ".yaml")
)

mkpath.(dirname.(filenames))

YAML.write_file.(filenames, dataframe2dictvec(df))
