using HTTP
using JSON3
using Printf

const RAW_MINAZ_DATA_PATH = joinpath("data","raw","minaz")
const MAXIMUM_RECORDS = 200

#using Dates
#lastmodifiedsince = "/lastmodifiedsince/$(Dates.format(today(), "yyyymmdd"))"
lastmodifiedsince = ""

#const RAW_MINAZ_URL2SAVEPATH = [
RAW_MINAZ_URL2SAVEPATH = [
    "https://opendata.rijksoverheid.nl/v1/infotypes/news" => joinpath(RAW_MINAZ_DATA_PATH, "nieuwsberichten", "nl"),
    "https://opendata.government.nl/v1/infotypes/news" => joinpath(RAW_MINAZ_DATA_PATH, "nieuwsberichten", "en"),
    "https://opendata.rijksoverheid.nl/v1/documents" => joinpath(RAW_MINAZ_DATA_PATH, "documenten", "nl"),
    "https://opendata.government.nl/v1/documents" => joinpath(RAW_MINAZ_DATA_PATH, "documenten", "en"),
    "https://opendata.rijksoverheid.nl/v1/documents/infotypes" => joinpath(RAW_MINAZ_DATA_PATH, "informatietypenlijst", "nl"),
    "https://opendata.government.nl/v1/documents/infotypes" => joinpath(RAW_MINAZ_DATA_PATH, "informatietypenlijst", "en"),
    "https://opendata.rijksoverheid.nl/v1/infotypes/faq" => joinpath(RAW_MINAZ_DATA_PATH, "vraag-antwoordcombinaties", "nl"),
    "https://opendata.government.nl/v1/infotypes/faq" => joinpath(RAW_MINAZ_DATA_PATH, "vraag-antwoordcombinaties", "en"),
    "https://opendata.rijksoverheid.nl/v1/infotypes/ministry" => joinpath(RAW_MINAZ_DATA_PATH, "ministeries", "nl"),
    "https://opendata.government.nl/v1/infotypes/ministry" => joinpath(RAW_MINAZ_DATA_PATH, "ministeries", "en"),
    "https://opendata.rijksoverheid.nl/v1/infotypes/subject" => joinpath(RAW_MINAZ_DATA_PATH, "onderwerpen", "nl"),
    "https://opendata.government.nl/v1/infotypes/subject" => joinpath(RAW_MINAZ_DATA_PATH, "onderwerpen", "en"),
]

for (url, savepath) in RAW_MINAZ_URL2SAVEPATH
    println(url => savepath)
    mkpath(savepath)
    offset = 0
    while true
        print("offset: ", offset)
        resp = HTTP.get("$url$lastmodifiedsince/?output=json&rows=$MAXIMUM_RECORDS&offset=$offset")
        docs = JSON3.read(resp.body)
        println(", length(docs): ", length(docs))
        JSON3.write(joinpath(savepath, @sprintf("offset-%06i.json", offset)), docs)
        sleep(0.1)
        length(docs) == MAXIMUM_RECORDS || break
        # 10 docs overlap voor het geval dat er nieuwe documents beschikbaar komen terwijl dit script draait
        # wordt later opgelost met 'unique' na het inladen
        offset += length(docs) - 10
    end
end

using JSON3, DataFrames

for lang in ["nl", "en"]
    docs = vcat(JSON3.read.(readdir(joinpath("data", "raw", "minaz", "documenten", lang), join=true))...)
    df = DataFrame(docs)
    unique!(df)
    base_savepath = joinpath(RAW_MINAZ_DATA_PATH, "informatietypendocs", lang)
    for typerow in eachrow(sort(combine(groupby(df, :type), nrow), :nrow))
        savepath = joinpath(base_savepath, typerow.type)
        mkpath(savepath)
        @show typerow savepath
        for row in eachrow(df[df.type.==typerow.type, :])
            print(rownumber(row))
            local resp
            try
                resp = HTTP.get("$(row.dataurl)/?output=json")
            catch e
                @show e row.dataurl row
                continue
            end
            print(",")
            open(joinpath(savepath, row.id*".json"), "w") do f
                write(f, String(resp.body))
            end
            sleep(0.1)
        end
    end
end

dtypes = [
# ministeries hebben geen dataurl veld, dus er zijn hiervoor geen meer velden te verkrijgen dan er al is.
#"ministeries", 
"onderwerpen", "vraag-antwoordcombinaties", "nieuwsberichten"]

for datatype in dtypes
    for lang in ["nl", "en"]
        df = begin 
            docs = vcat(collect.(JSON3.read.(readdir(joinpath("data", "raw", "minaz", datatype, lang), join=true)))...)
            all_possible_keys = unique(vcat(collect.(keys.(docs))...))
            default_doc = Dict(x=>missing for x in all_possible_keys)
            docs_with_all_fields = mergewith.(coalesce, Ref(default_doc), docs) 
            DataFrame(docs_with_all_fields)
        end
        unique!(df)
        savepath = joinpath(RAW_MINAZ_DATA_PATH, "docs", datatype, lang)
        mkpath(savepath)
        @show savepath nrow(df)
        for row in eachrow(df)
            print(rownumber(row))
            local resp
            try
                resp = HTTP.get("$(row.dataurl)/?output=json")
            catch e
                @show e row.dataurl row
                continue
            end
            print(",")
            open(joinpath(savepath, row.id*".json"), "w") do f
                write(f, String(resp.body))
            end
            sleep(0.1)
        end
    end
end
