module Browse

using Genie, Genie.Requests, Genie.Renderer.Json
using OrderedCollections
using DataFrames
using SwaggerMarkdown

import ..Main.DataModel

browse_response = """
    responses:
      '200':
        description: OK
        content:
          application/json:
            schema:
              type: object
              properties:
                themes:
                  type: array
                  description: Remaining themes which could be used to filter further.
                ministeries:
                  type: array
                  description: Remaining ministeries which could be used to filter further.
                subjects:
                  type: array
                  description: Remaining filtered subjects.
"""

# Docs in Open API v3.0 format
@swagger """
/browse/{language}:
  get:
    description: Browse through subjects, themes and ministeries by filtering on all of them. 
    parameters:
      - in: path
        name: language
        schema:
          type: string
          enum: [nl-NL, en-GB]
        required: true
        description: "The used language."
        example: nl-NL
      - in: query
        name: themes
        schema:
          type: array
          items:
            type: string
            example: Familie, zorg en gezondheid
        style: pipeDelimited
        required: false
        description: Themes to filter subjects with.
      - in: query
        name: ministeries
        schema:
          type: array
          items:
            type: string
            example: "Ministerie van Sociale Zaken en Werkgelegenheid"
        style: pipeDelimited
        required: false
        description: Ministeries to filter subjects with.
      - in: query
        name: subjects
        schema:
          type: array
          items:
            type: string
            example: 18 jaar worden
        style: pipeDelimited
        required: false
        description: Optional selection of subjects to use for the filtering.
$browse_response
  post:
    description: Browse through subjects, themes and ministeries by filtering on all of them. 
    summary: Browse through subjects, themes and ministeries. 
    parameters:
      - in: path
        name: language
        schema:
          type: string
          enum: [nl-NL, en-GB]
        required: true
        description: "The used language."
        example: nl-NL
    requestBody:
      description: Browse through subjects, themes and ministeries by filtering on all of them. 
      required: false
      content:
        application/json:
          schema:
            type: object
            properties:
              themes: 
                description: Themes to filter subjects with.
                type: array
                items:
                  type: string
                default: []
                example: ["Familie, zorg en gezondheid"]
              ministeries:
                description: Ministeries to filter subjects with.
                type: array
                items:
                  type: string
                default: []
              subjects:
                description: Optional selection of subjects to use for the filtering.
                type: array
                items:
                  type: string
                default: []
                example: [18 jaar worden, Geldzaken, Zorgverzekering]
$browse_response
"""

route("/browse", method = GET) do
    get_browse("nl-NL")
end
route("/browse/:language", method = GET) do
    get_browse(payload(:language))
end
function get_browse(language)
    themes = convert(Vector{String}, split(getpayload(:themes, ""), "|", keepempty=false))
    ministeries = convert(Vector{String}, split(getpayload(:ministeries, ""), "|", keepempty=false))
    subjects = convert(Vector{String}, split(getpayload(:subjects, ""), "|", keepempty=false))
    return Genie.Renderer.Json.json(browse([language], themes, ministeries, subjects))
end

route("/browse", method = POST) do
    post_browse("nl-NL")
end
route("/browse/:language", method = POST) do
    post_browse(payload(:language))
end
function post_browse(language)
    payload_dict = jsonpayload()
    if isnothing(payload_dict)
        payload_dict = Dict{String, Any}()
    end
    themes = convert(Vector{String}, copy(get(payload_dict, "themes", String[])))
    ministeries = convert(Vector{String}, copy(get(payload_dict, "ministeries", String[])))
    subjects = convert(Vector{String}, copy(get(payload_dict, "subjects", String[])))
    return Genie.Renderer.Json.json(browse([language], themes, ministeries, subjects))
end

# TODO refactor: decide if this constructed dataset (filters and subjects_df)
# should be in DataModel (it could also stay here since its specific for the
# functionality of this endpoint

# TODO this sould also be updated when `models` changes when using the Editor,
# so maybe its better to put this in DataModel
const filters = flatten(DataModel.categorieen, "Categorie.Filters")
filters[:, "Filter.Type"] = (x->x["Type"]).(filters[:,"Categorie.Filters"])
filters[:, "Filter.Id"] = (x->x["Id"]).(filters[:,"Categorie.Filters"])
filters[:, "Filter.Handle"] = (x->x["Handle"]).(filters[:,"Categorie.Filters"])
filters[:, "Filter.Titel"] = (x->x["Titel"]).(filters[:,"Categorie.Filters"])
select!(filters, Not("Categorie.Filters"))

const subjects_df = unstack(
    combine(groupby(filters, [
                     #"Compleet",
                     #"Id",
                     #"Wordt_Onderhouden_Door",
                     "Type",
                     #"Categorie.Laatst_Bewerkt_Op",
                     "Categorie.Soort",
                     #"Categorie.Id",
                     #"Categorie.Icon",
                     "Categorie.Taal",
                     #"Categorie.Handle",
                     #"Categorie.Titel",
                     "Filter.Type",
                     "Filter.Id",
                     "Filter.Handle",
                     "Filter.Titel",
                    ]),
            "Categorie.Titel" => Ref,
    ), "Categorie.Soort", "Categorie.Titel_Ref", ; fill=String[])

function browse(languages::Vector{<:AbstractString}, themes::Vector{String}, ministeries::Vector{String}, subjects::Vector{String})
    filtered = subjects_df[
        issubset.(Ref(themes), subjects_df[:,"Thema"]) .&
        issubset.(Ref(ministeries), subjects_df[:, "Ministerie"]), :]
    if !isempty(subjects) 
        filtered = filtered[in.(filtered[:, "Filter.Titel"], Ref(subjects)), :]
    end
    if !isempty(languages)
        filtered = filtered[in.(filtered[:, "Categorie.Taal"], Ref(languages)), :]
    end
    themes = union([], filtered[:, "Thema"]...)
    ministeries = union([], filtered[:, "Ministerie"]...)
    return OrderedDict(:themes => themes, :ministeries => ministeries, :subjects => filtered[:, "Filter.Titel"])
end

end # module
