module DataModel

using PythonCall
using OrderedCollections
import YAML
using DataFrames

using GlobFromPython: glob
using HierarchicalConfigs: load_hierachicaly_with_sources, dataframe2dictvec

import ..Main.ConditionParser as CP
import ..Main.SchemeModel
import ..Main.GlobalConstants.EXTERNAL_DATA_PATH
import ..Main.GlobalConstants.PRA_MODELS_PATH

const fasttext = pyimport("fasttext")
const ft = fasttext.load_model(joinpath(EXTERNAL_DATA_PATH, "fasttext-cc.nl.64.bin"))

const zoekdescriptors = DataFrame([x => Union{}[] for x in [
 "Id",
 "Wordt_Onderhouden_Door",
 "Zoekdescriptor.Laatst_Bewerkt_Op",
 "Zoekdescriptor.Taal",
 "Zoekdescriptor.Variant",
 "Zoekdescriptor.Model",
 "Zoekdescriptor.Titel",
 "Zoekdescriptor.Korte_Omschrijving",
 "Zoekdescriptor.Filters",
 "Zoekdescriptor.Zoektermen",
 "Type",
]])
const regelingen = DataFrame()
const levensgebeurtenissen = DataFrame()
const categorieen = DataFrame()
const notifications = CP.Notification[]
function update_model_dfs(models)
    global zoekdescriptors, regelingen, levensgebeurtenissen, categorieen
    empty!(zoekdescriptors)
    empty!(regelingen)
    empty!(levensgebeurtenissen)
    empty!(categorieen)
    empty!(notifications)
    # This automaticaly filters out deleted models since those will not have an "Compleet" key
    foreach(x->push!(zoekdescriptors, x, cols=:subset, promote=true),
            filter(x->"Compleet" ∈ keys(x) && x["Compleet"] && "Zoekdescriptor.Model" ∈ keys(x), models))
    zoekdescriptors[!, SchemeModel.TERM_COL_NAME] = coalesce.(zoekdescriptors[!, SchemeModel.TERM_COL_NAME], Ref([]))
    zoekdescriptors[!, SchemeModel.FILTER_COL_NAME] = coalesce.(zoekdescriptors[!, SchemeModel.FILTER_COL_NAME], Ref([]))
    foreach(x->push!(regelingen, x, cols=:union, promote=true),
            filter(x->"Compleet" ∈ keys(x) && x["Compleet"] && x["Type"] == "Regeling", models))
    regelingen[!, "Regeling.Relevante_Levensgebeurtenissen"] = coalesce.(regelingen[:,"Regeling.Relevante_Levensgebeurtenissen"], Ref([]))
    foreach(x->push!(levensgebeurtenissen, x, cols=:union, promote=true),
            filter(x->"Compleet" ∈ keys(x) && x["Compleet"] && x["Type"] == "Levensgebeurtenis", models))
    foreach(x->push!(categorieen, x, cols=:union, promote=true),
            filter(x->"Compleet" ∈ keys(x) && x["Compleet"] && x["Type"] == "Categorie", models))
    append!(notifications, vcat(CP.regeling2notifications.(dataframe2dictvec(regelingen))...))
end
update_model_dfs(
    load_hierachicaly_with_sources(x->YAML.load_file(x, dicttype=OrderedDict{String, Any}),
                                   glob("**/*.yaml", PRA_MODELS_PATH), basepart=PRA_MODELS_PATH)[1]
)

"""
Updates the terms and termmat global variables which are used by some endpoints
"""
function index_searchterms()
    global terms, termmat
    terms = combine(groupby(flatten(zoekdescriptors[!, [SchemeModel.TERM_COL_NAME]], SchemeModel.TERM_COL_NAME), SchemeModel.TERM_COL_NAME), nrow => :Occurs_In_N_Docs)
    terms.Term_Vec = pyconvert.(Vector{Float32}, ft.get_word_vector.(terms[!, SchemeModel.TERM_COL_NAME]))
    terms = filter(:Term_Vec => !all ∘ Broadcast.BroadcastFunction(iszero), terms)
    termmat = [terms.Term_Vec...;;]
    select!(terms, Not(:Term_Vec))
end
index_searchterms()

function occuring_values_of(models, fieldname)
    filter(!ismissing, unique(get(x, fieldname, missing) for x in models))
end

end # module
