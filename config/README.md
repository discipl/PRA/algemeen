# Demo server                           

To run the PRA-prototype you can set up a demo server. On this server you run the following:
1. PRA backend API. 
2. PRA Model Editor, as a web application.
3. Webserver as remote proxy.
4. OAuth2-proxy as OIDC-client to an Identity Provider; the configuration is based on the Gitlab Oauth2 provider.
5. Certbot to obtain and renew a Let's Encrypt TLS-certificate.

The PRA frontend can run as a web application on Gitlab Pages [here](https://discipl.gitlab.io/PRA/pra-front-end) or as an app on a mobile device.
The PRA backend API and the PRA Model Editor are contained in a single Docker image.

For instructions how to run the PRA frontend on a mobile device, see [here](https://gitlab.com/discipl/PRA/pra-front-end/-/blob/main/README.md).

## Prerequisites

1. A fully qualified domain name (FQDN).
2. A Linux-server with Docker and Docker Compose, at least 8GB RAM.

## Customize your configuration

1. Create a root directory for the demo environment, in `/opt/pra`.
2. Copy the configuration files in this directory into that root directory.
3. Build and publish a Docker image of the backend API and PRA Model Editor, see the [instructions](https://gitlab.com/discipl/PRA/algemeen/-/tree/main/Server/README.md).
4. Adapt the `docker-compose.yml` file, replace items marked by `<...>` with the appropriate values:
    - Set the FQDN of your server in the `APP_HOST` enviroment variable of the *api_editor* service.
    - Set the Docker repository where you have published the Docker image of the api_editor in the section of *api_editor* service.
5. Adapt the `nginx/conf/nginx.conf` file, replace items marked by `<FQDN of backend server>` with the actual FQDN.
6. Obtain a Let's Encrypt certificate, by running `docker compose run certbot certonly`, and choose option 1. Fill in appropriate information at the prompts.
7. In /opt/pra/git, clone the Gitlab project [Algemeen](https://gitlab.com/discipl/PRA/algemeen). (The Server/public directory contains the PRA-models; a local copy is needed if making changes to its contents. Changes must be committed outside of the editor, on the command line.)
8. Configure the PRA Model Editor as an application in Gitlab: in *Group > Settings > Applications* add an application with the following parameters:
    - Redirect URI: `https://<FQDN>/oauth2/callback`
    - Scopes: `read_api, read_user, openid, profile, email`
    
    Note: you will need suitable Gitlab admin privileges for this.
9. Adapt the `oauth2-proxy/oauth2-proxy.cfg` file, replace items marked by `<...>` with the appropriate values:
    - FQDN of the backend server (note: multiple occurrences)
    - email domain name of all email addresses that, if associated with a Gitlab account, are authorized to use the PRA editor.
    - Gitlab project name of which users are authorised to access the PRA Model Editor.
    - `client_id` and `client_secret` configured in/provided by the Gitlab OAuth provider (one time only, use the *Application ID* for `client_id` and *Secret* for `client_secret`.
    - `cookie_secret`
10. Optionally, add individual email addresses associated with Gitlab-accounts that you want to authorize to `oauth2-proxy/auth-users.cfg`. If there are none, ensure this file is empty. Example: if all users of Gitlab project discipl/PRA with email address in domain example.com are allowed, as well as the Gitlab account in the same project associated with email address `someone@anotherdomain.com`, put the Gitlab project name and example.com domain name in `oauth2-proxy.cfg` and `someone@anotherdomain.com` in `auth-users.cfg`.

## How to start and stop services

`docker compose up -d` and `docker compose down`, respectively.

Check that services run correctly on the following URL's. Note that it might take a while for the services to start up completely.

`https://FQDN/editor` (for the editor, you should be redirected to the Gitlab OpenID Connect provider to authenticate if you do not yet have a valid cookie)

`https://FQDN/docs` (for the Swagger documentation of the API)
