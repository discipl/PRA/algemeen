module EditorDataInterface

using OrderedCollections
import DataFrames as DF
import YAML
import Stipple
import Genie
import Dates
import Random

using GlobFromPython: glob
using HierarchicalConfigs: dataframe2dictvec, dictvec2dataframe,
    calculate_fields_distribution, distribute_fields_over_hierarchies,
    load_hierachicaly_with_sources

import ..Main.DataModel
import ..Main.SchemeModel
import ..Main.GlobalConstants.PRA_MODELS_PATH

mutable struct SharedTree{RM<:Stipple.ReactiveModel}
    items::DF.DataFrame
    models::Vector{OrderedDict{String, Any}}
    fieldsources::Vector{Dict{String, BitVector}}
    clients::Dict{Symbol, RM}
    # tree representation of items
    tree::Vector{Dict{Any, Any}}
    reactive_variable_name::String
end
#function SharedTree{RM<:Stipple.ReactiveModel}()
#    SharedTree{RM}("tree_nodes")
#end
function SharedTree{RM}(reactive_variable_name) where RM<:Stipple.ReactiveModel
    # TODO? maybe paths as Vector{Symbol} to improve performance and ram usage
    model_filenames = glob("**/*.yaml", PRA_MODELS_PATH)
    file_dicts = YAML.load_file.(model_filenames, dicttype=OrderedDict{String, Any})

    splitted_full_paths = splitpath.(model_filenames)
    splitted_paths = getindex.(splitted_full_paths, range.(length(splitpath(PRA_MODELS_PATH))+1, length.(splitted_full_paths)))
    splitted_dirs = getindex.(splitted_paths, range.(1, length.(splitted_paths).-1))
    model_names = first.(splitext.(last.(splitted_paths)))

    items = DF.DataFrame(:path => splitted_dirs)
    DF.transform!(DF.groupby(items, :path), DF.nrow => :files_in_dir)
    items.path = ifelse.(items.files_in_dir .== 1, items.path, vcat.(items.path, model_names))
    DF.select!(items, DF.Not(:files_in_dir))

    models, fieldsources = load_hierachicaly_with_sources(file_dicts, items.path)

    items.modelindex = eachindex(models)

    needed_parents = unique(Iterators.flatten((x->(x[1:n] for n in 1:length(x)-1)).(items.path)))
    missing_paths = setdiff(needed_parents, items.path)
    append!(items, DF.DataFrame(:path => missing_paths, :modelindex => nothing), cols=:union)
    sort!(items, :path)
    items.rownumber .= eachindex(items.path)
    path2rownumber = Dict((items.path .=> items.rownumber)..., String[] => nothing)
    items.parent = getindex.(Ref(path2rownumber), getindex.(items.path, range.(1,length.(items.path).-1)))
    items.deleted .= false
    clients = Dict{Symbol, RM}()
    r = SharedTree(items, models, fieldsources, clients, Dict{Any, Any}[], reactive_variable_name)
    r.tree = build_tree(r)
    r
end
Broadcast.broadcastable(m::SharedTree) = Ref(m)

function add_new_client!(shared_tree::SharedTree{RM}, model::RM) where RM <: Stipple.ReactiveModel
    ch = Symbol(model.channel__)
    shared_tree.clients[ch] = model
    Timer(Stipple.PURGE_CHECK_DELAY[], interval=Stipple.PURGE_CHECK_DELAY[]) do timer
        #println("checking if the model for $ch is still needed")
        if !(ch in keys(Stipple.LAST_ACTIVITY))
            println("$ch has no LAST_ACTIVITY so its model will be deleted")
            display(Stipple.LAST_ACTIVITY)
            delete!(shared_tree.clients, ch)
            close(timer)
            @show keys(shared_tree.clients)
        end
    end
end
function get_client_model(shared_tree::SharedTree, ch)
    shared_tree.clients[Symbol(ch)]
end


get_parent(items::DF.DataFrame, rownumber::Int) = items[rownumber,:]

get_children(items::DF.DataFrame, path::AbstractString) = get_children(items, splitpath(path))
get_children(items::DF.DataFrame, path::AbstractVector{<:Any}) = get_children(items, convert(Vector{String}, path))
function get_children(items::DF.DataFrame, path::AbstractVector{<:AbstractString})
    is_parent = items.path .== Ref(path)
    @assert sum(is_parent) == 1
    parent_rownum = first(items.rownumber[is_parent])
    get_children(items, parent_rownum)
end
function get_children(items::DF.DataFrame, rownumber::Union{Int, Nothing})
    items[items.parent .== rownumber .&& .~items.deleted, :]
end


descendants_of(shared_tree::SharedTree, rownumber::Int) = descendants_of(shared_tree.items, rownumber)
function descendants_of(items::DF.DataFrame, rownumber::Union{Int, Nothing})
    possible_parents = get_children(items, rownumber).rownumber
    return vcat(possible_parents, descendants_of.(Ref(items), possible_parents)...)
end

all_ancestors(shared_tree::SharedTree, rownumber::Int) = all_ancestors(shared_tree.items, rownumber)
all_ancestors(items::DF.DataFrame, rownumber::Int) = all_ancestors(items, items[rownumber, :])
function all_ancestors(items::DF.DataFrame, item::DF.DataFrameRow)
    p = items[items.rownumber .== item.parent, :]
    # Base case: when item.parent == nothing item has no parent
    DF.nrow(p) == 0 && return Int[]
    @assert DF.nrow(p) == 1 # check if item does not have multiple parents (should never occur)
    p = first(p)
    return vcat(p.rownumber, all_ancestors(items, p))
end

build_tree(shared_tree::SharedTree, path::AbstractVector{<:Any}) = build_tree(shared_tree, convert(Vector{String}, path))
build_tree(shared_tree::SharedTree, path::AbstractVector{<:AbstractString}) = build_tree(shared_tree, get_parent(shared_tree.items, path))
function build_tree(shared_tree::SharedTree, parent::DF.DataFrameRow, ancestors=[])
    type2icon = Dict(
        "Regeling" => "receipt_long",
        #"Regeling" => "lab_profile",
        #"Regeling" => "contract",
        #"Regeling" => "contract_edit",
        #"Regeling" => "rule",
        "Levensgebeurtenis" => "event",
        #missing => "unknown_med",
        #missing => "question_mark",
        #missing => "select",
        missing => "api",
        #"Thema" => "category",
        #"Ministerie" => "account_balance",
        "Categorie" => "category",
    )

    compleet2iconcolor = Dict(
        true => "blue-6",
        false => "blue-3",
        #missing => "info",
        #missing => "accent",
        #missing => "warning",
        missing => "grey",
    )

    r = Dict()
    r["rownumber"] = parent.rownumber
    r["label"] = get_label(parent)
    if isnothing(parent.modelindex)
        r["icon"] = type2icon[missing]
        r["iconcolor"] = compleet2iconcolor[missing]
    else
        parent_model = shared_tree.models[parent.modelindex]
        r["icon"] = type2icon[parent_model["Type"]]
        r["iconcolor"] = compleet2iconcolor[parent_model["Compleet"]]
    end
#    r["selectable"] = true
#    r["expandable"] = true
    ancestors_including_itself = vcat(ancestors, parent.rownumber)
    r["ancestors"] = ancestors_including_itself
    # when item is no parent the children prop will be an empty list
    r["children"] = build_tree.(shared_tree, eachrow(get_children(shared_tree.items, parent.rownumber)), Ref(ancestors_including_itself))
    return r
end

build_tree(shared_tree::SharedTree) = build_tree.(shared_tree, eachrow(get_children(shared_tree.items, nothing)))
# rev sort to make new items appear on top of the list
#build_tree(shared_tree::SharedTree) = build_tree(sort(shared_tree.items, :rownumber, rev=true))

current_tree(shared_tree::SharedTree) = shared_tree.tree

isparent_mask(items::DF.DataFrame) = map(items.rownumber) do x
    any(items.parent .== x)
end
all_parents(shared_tree::SharedTree) = shared_tree.items[isparent_mask(shared_tree.items), :rownumber]

get_modelindex(shared_tree::SharedTree, rownumber::Nothing) = nothing
get_modelindex(shared_tree::SharedTree, rownumber::Int) = shared_tree.items[rownumber, :modelindex]

get_model(shared_tree::SharedTree, rownumber::Nothing) = OrderedDict()
function get_model(shared_tree::SharedTree, rownumber::Int)
    modelindex = get_modelindex(shared_tree, rownumber)
    if isnothing(modelindex)
        return OrderedDict()
    else
        return deepcopy(shared_tree.models[get_modelindex(shared_tree, rownumber)])
    end
end

get_item(shared_tree::SharedTree, nothing) = OrderedDict("rownumber"=>nothing, "label"=>"", "hasmodel"=>false)
get_item(shared_tree::SharedTree, rownumber::Int) = OrderedDict(
    "rownumber"=>shared_tree.items[rownumber, :rownumber],
    "label"=>get_label(shared_tree, rownumber),
    "hasmodel"=>!isnothing(shared_tree.items[rownumber, :modelindex]),
)

get_label(shared_tree::SharedTree, rownumber::Nothing) = ""
get_label(shared_tree::SharedTree, rownumber::Int) = last(shared_tree.items[rownumber, :path])
get_label(item::DF.DataFrameRow) = last(item.path)
get_label(items::DF.DataFrame, rownumber) = last(items[rownumber, :path])

function set_label!(items::DF.DataFrame, rownumber, label)
    items[rownumber, :path] = vcat(items[rownumber, :path][begin:end-1], label)
end


get_treepath(shared_tree::SharedTree, rownumber::Nothing) = nothing
get_treepath(shared_tree::SharedTree, rownumber::Int) = shared_tree.items[rownumber, :path]

function haschanges(shared_tree::SharedTree, item::AbstractDict, model::AbstractDict)
    @show item != get_item(shared_tree, item["rownumber"])
    @show model != get_model(shared_tree, item["rownumber"])
    println("<item>")
    println()
    display(item)
    println()
    println("</item>")
    println("<get_item(shared_tree, $(item["rownumber"]))>")
    println()
    display(get_item(shared_tree, item["rownumber"]))
    println()
    println("</get_item(shared_tree, $(item["rownumber"]))>")
    println("<model>")
    println()
    display(model)
    println()
    println("</model>")
    println("<get_model(shared_tree, $(item["rownumber"]))>")
    println()
    display(get_model(shared_tree, item["rownumber"]))
    println()
    println("</get_model(shared_tree, $(item["rownumber"]))>")
    (item != get_item(shared_tree, item["rownumber"])) ||
    (model != get_model(shared_tree, item["rownumber"]))
end

function update_tree_on_all_connected_clients(shared_tree::SharedTree)
    shared_tree.tree = build_tree(shared_tree)
    connected_channels = Symbol.(Iterators.flatten(getfield.(Genie.WebChannels.connected_clients(), :channels)))
    intersect!(connected_channels, keys(shared_tree.clients))
    #unique!(connected_channels) intersect! makes it alreay unique
    for model in (shared_tree.clients[x] for x in connected_channels)
        getfield(model, Symbol(shared_tree.reactive_variable_name))[] = shared_tree.tree
    end
end

function save_item!(shared_tree::SharedTree, item::AbstractDict, model::AbstractDict)
    # if the label is edited, the tree should be updated
    if get_label(shared_tree.items, item["rownumber"]) != item["label"]
        set_label!(shared_tree.items, item["rownumber"], item["label"])
    end
    should_update_models = false
    #if shared_tree.items[item["rownumber"], :modelindex] != item["modelindex"]
    modelindex = shared_tree.items[item["rownumber"], :modelindex]
    # model is deleted
    if !isnothing(modelindex) && !item["hasmodel"]
        @show modelindex item["hasmodel"]
        shared_tree.models[modelindex] = OrderedDict()
        should_update_models = true
        shared_tree.items[item["rownumber"], :modelindex] = nothing
    end
    #println("selectedRownumber, index: $(item["modelindex"])")
    if item["hasmodel"]
        if "Type" in keys(model) && model["Type"] in keys(SchemeModel.type2fields)
            fields_of_itemtype = SchemeModel.type2fields[model["Type"]]
            model_copy = filter(x->x.first in fields_of_itemtype, model)
        else
            model_copy = deepcopy(model)
        end
        # if a newly added model was not saved yet
        # model is created
        if isnothing(modelindex)
        #if item["modelindex"] == length(shared_tree.models)+1
            depth = length(shared_tree.items[item["rownumber"], :path])
            fieldsources = Dict(
                k => BitVector(ones(Bool, depth))
                for k in keys(model_copy)
            )
            push!(shared_tree.models, model_copy)
            push!(shared_tree.fieldsources, fieldsources)
            shared_tree.items[item["rownumber"], :modelindex] = length(shared_tree.models)
            should_update_models = true
        # model is updated
        else
            prev_model = shared_tree.models[modelindex]
            changed_fields = [ fieldname
                for fieldname in union(keys(prev_model), keys(model))
                if get(prev_model, fieldname, nothing) != get(model, fieldname, nothing)
            ]
            if !isempty(changed_fields)
                depth = length(shared_tree.items[item["rownumber"], :path])
                prev_fieldsources = shared_tree.fieldsources[modelindex]
                fieldsources= Dict(
                    # when the field is changed it will be defined in the current model by making the fieldsource bitvector refer to itself (all ones)
                    k => (k in changed_fields ? BitVector(ones(Bool, depth)) : prev_fieldsources[k])
                    for k in keys(model_copy)
                )
                shared_tree.models[modelindex] = model_copy
                shared_tree.fieldsources[modelindex] = fieldsources
                should_update_models = true
            end
        end
    end
    #update models when prev or current index is not nothing
    if should_update_models
        update_models_field_inheritance(shared_tree)
        println("updating model dfs")
        DataModel.update_model_dfs(shared_tree.models)
        println("updating searchtermindex")
        DataModel.index_searchterms()
    end
    update_tree_on_all_connected_clients(shared_tree)
    return get_model(shared_tree, item["rownumber"])
end

function delete_items!(shared_tree::SharedTree, rownumbers::AbstractVector{Int})
    @show descendants_of.(shared_tree, rownumbers)
    can_delete = filter(rownumbers) do x
        issubset(descendants_of(shared_tree, x), rownumbers)
    end
    @show can_delete
    shared_tree.items[can_delete, :deleted] .= true
    model_indices_to_delete = filter(!isnothing, shared_tree.items[can_delete, :modelindex])
    @show model_indices_to_delete
    display(shared_tree.items[can_delete,:])
    shared_tree.models[model_indices_to_delete] .= Ref(OrderedDict())
    update_tree_on_all_connected_clients(shared_tree)
    not_deleted = setdiff(rownumbers, can_delete)
    println("updating model dfs.")
    DataModel.update_model_dfs(shared_tree.models)
    println("updating searchtermindex.")
    DataModel.index_searchterms()
    not_deleted
end

function move_items_to!(shared_tree::SharedTree, rownumbers::AbstractVector{Int}, to::Int)
    shared_tree.items[rownumbers, :parent] .= to
    p = shared_tree.items[to, :]
    shared_tree.items[rownumbers, :path] .= vcat.(Ref(p.path), last.(shared_tree.items[rownumbers, :path]))
    update_models_field_inheritance(shared_tree)
    update_tree_on_all_connected_clients(shared_tree)
    #TODO update seleted model in case fields are inherited
end

function update_models_field_inheritance(shared_tree::SharedTree)
    models_own_fieldnames = keys.(filter.(x->all(x.second), shared_tree.fieldsources))
    plain_models = ((x,y)->filter(z->z.first in y, x)).(shared_tree.models, models_own_fieldnames)

    ordered_paths = sort(
        shared_tree.items[.!isnothing.(shared_tree.items.modelindex), [:path, :modelindex, :rownumber]],
        :modelindex)

    #models, fieldsources = load_hierachicaly_with_sources(plain_models, ordered_paths.path)
    refered_models = plain_models[ordered_paths.modelindex]
    models, fieldsources = load_hierachicaly_with_sources(refered_models, ordered_paths.path)

    shared_tree.models = models
    shared_tree.fieldsources = fieldsources

    # update models indeces since indices which did not occur in items where not used
    shared_tree.items[ordered_paths.rownumber, :modelindex] = eachindex(ordered_paths.path)
    nothing
end

function add_new_item!(shared_tree::SharedTree, parent_rownumber::Int; label="Item $(DF.nrow(shared_tree.items))")
    new_rownumber = DF.nrow(shared_tree.items)+1
    new_item = (rownumber=new_rownumber,
                parent=parent_rownumber,
                path=vcat(shared_tree.items[parent_rownumber, :path], label),
                deleted=false,
                modelindex=nothing,)
    push!(shared_tree.items, new_item)
    #TODO refactor: move _treepath construction into SchemeModel (or maybe DataModel?)
    update_tree_on_all_connected_clients(shared_tree)
    return new_rownumber
end

function add_model(shared_tree::SharedTree)
    new_model = OrderedDict{String, Any}()
    return new_model
end

function write_models(shared_tree::SharedTree)
    items_to_write_mask = .!isnothing.(shared_tree.items.modelindex) .&& .!shared_tree.items.deleted
    modelpaths = joinpath.(shared_tree.items[items_to_write_mask, :path])
    filenames = joinpath.(PRA_MODELS_PATH, modelpaths)
    m = shared_tree.models[shared_tree.items[items_to_write_mask, :modelindex]]
    d = dictvec2dataframe(m)

    c = calculate_fields_distribution(filenames, d, basepart=PRA_MODELS_PATH, last_level_fields = [
        "Compleet",
        "Id",
        "Zoekdescriptor.Laatst_Bewerkt_Op",
        "Zoekdescriptor.Taal",
        "Zoekdescriptor.Variant",
        "Zoekdescriptor.Titel",
        "Zoekdescriptor.Korte_Omschrijving",
        "Zoekdescriptor.Zoektermen",
        "Regeling.Titel",
        "Regeling.Subtitel",
        "Regeling.Omschrijving",
        "Regeling.Dev.Source_URL",
        "Levensgebeurtenis.Id",
        "Levensgebeurtenis.Titel",
        "Levensgebeurtenis.Omschrijving",
        "Levensgebeurtenis.Relevante_Filters",
    ])
    dv = dataframe2dictvec(d)
    cv = dataframe2dictvec(c)

    h = distribute_fields_over_hierarchies.(filenames, dv, cv, basepart=PRA_MODELS_PATH, defaults_filename=nothing)

    towrite = mergewith(merge, h...)

    fields_order = union(values(SchemeModel.type2fields)...)
    field_sorter = k->Dict(fields_order .=> 1:length(fields_order))[k]
    towrite_content = sort.(values(towrite), by=field_sorter)
    towrite_filenames = joinpath.(keys(towrite), string.(get.(towrite_content, "Id", "defaults"), ".yaml"))

    rm(PRA_MODELS_PATH, recursive=true)
    mkpath.(dirname.(towrite_filenames))
    YAML.write_file.(towrite_filenames, towrite_content)
    nothing
end

function init_field(shared_tree::SharedTree, rownumber::Int, name::Symbol, ::SchemeModel.GloballyUniqueForField)
    return abs(Random.rand(Int))
end
function init_field(shared_tree::SharedTree, rownumber::Int, name::Symbol, ::SchemeModel.CurrentDatetime)
    return Dates.now()
end
function init_field(shared_tree::SharedTree, rownumber::Int, name::Symbol, ::SchemeModel.CurrentFilePath)
    return join(get_treepath(shared_tree, rownumber),".")
end

function minimal_model(shared_tree::SharedTree, rownumber::Int, type::AbstractString)
    minimal_model = OrderedDict()
    for v_allOf in getfield(SchemeModel.item_scheme.scheme.possible_objects, Symbol(type)).scheme.merge_objects
        for required_field in v_allOf.scheme.required
            v = getfield(v_allOf.scheme.properties, required_field)
            minimal_model[string(required_field)] = init_field(shared_tree, rownumber, required_field, v.initializer)
        end
    end
    minimal_model
end

end # module
