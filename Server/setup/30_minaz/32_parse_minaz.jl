using JSON3, DataFrames, PythonCall

regelingen = DataFrame(vcat(JSON3.read.(readdir("data/raw/minaz/informatietypendocs/nl/regeling/", join=true))...))
formulieren = DataFrame(vcat(JSON3.read.(readdir("data/raw/minaz/informatietypendocs/nl/formulier/", join=true))...))

combine(groupby(flatten(regelingen[:,[:subjects]], :subjects), :subjects), nrow)
combine(groupby(flatten(formulieren[:,[:subjects]], :subjects), :subjects), nrow)

g = pyimport("glob")

docs = begin
    #rows = JSON3.read.(readdir("data/raw/minaz/informatietypendocs/en/", join=true))
    rows = JSON3.read.(pyconvert(Vector{String}, g.glob("data/raw/minaz/docs/**/*.json", recursive=true)))
    all_keys = unique(vcat(collect.(keys.(rows))...))
    default_row = Dict(x=>missing for x in all_keys)
    rows = mergewith.(coalesce, Ref(default_row), rows) 
    df = DataFrame(rows)
    unique!(df)
end
