using Genie

import ..Main.GlobalConstants.APP_BASEPATH
import ..Main.GlobalConstants.SERVER_PORT

import ..Main.EditorBuilder as EB
import ..Main.EditorDataInterface as EDI

Server = include("Server.jl")
using .Server

write_models() = EDI.write_models(EB.shared_tree)

Genie.config.run_as_server = true
Genie.config.cors_allowed_origins = ["*"]
Genie.config.cors_headers["Access-Control-Allow-Origin"] = "*"
Genie.config.cors_headers["Access-Control-Allow-Methods"] = "GET,POST,PUT,DELETE,OPTIONS"
Genie.config.base_path = APP_BASEPATH
Genie.config.websockets_base_path = APP_BASEPATH

#up(PORT, "0.0.0.0"; async = false, ws_port=PORT)
#up(SERVER_PORT, "127.0.0.1"; async = true, ws_port=SERVER_PORT)
up(SERVER_PORT, "0.0.0.0"; async = true, ws_port=SERVER_PORT)
