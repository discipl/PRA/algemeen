using GenieFramework
const PRA_MODELS_PATH = joinpath("..", "..", "..", "data", "vindbaar")
const SCHEMA_PATH = joinpath("..", "..", "public", "schemas", "vindbaar.yaml")
Genie.loadapp() # load app
up(8080, "0.0.0.0", ws_port=8080) # start server
