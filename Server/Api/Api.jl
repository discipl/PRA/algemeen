module Api

using Genie, Genie.Requests, Genie.Renderer, Genie.Renderer.Json
#using Arrow
using DataFrames
using Tables
using JSONTables
using JSON3
using YAML

using OrderedCollections
using Dates

using SwagUI
using SwaggerMarkdown

import ..Main.DataModel
import ..Main.SchemeModel
import ..Main.GlobalConstants.APP_LOCATION

const Browse = include(joinpath("Browse.jl"))
using .Browse
const Content = include(joinpath("Content.jl"))
using .Content
const Item = include(joinpath("Item.jl"))
using .Item
const Categories = include(joinpath("Categories.jl"))
using .Categories
const CheckNotifications = include(joinpath("CheckNotifications.jl"))
using .CheckNotifications

# build a swagger document from markdown
info = Dict{String, Any}()
info["title"] = "PRA backend API Documentation"
info["version"] = "0.0.1"
openApi = OpenAPI("3.0", info)
swagger_document = build(openApi)

route("/docs") do
    @info string(Genie.Requests.getrequest())
    println("======== /docs request ========")
    println(Genie.Requests.getrequest())
    render_swagger(swagger_document)
end

route("/") do
    @info string(Genie.Requests.getrequest())
    println("======== / request ========")
    println(Genie.Requests.getrequest())
    Genie.Renderer.redirect("/docs", 302)
end

end # module

