# PRA Model Specification

### status of this document

This document contains the initial specification of so called PRA models
and is not finished.

## PRA models

PRA models contain the knowledge the PRA app needs to let a user find, explain and
use a service that entails from official law. For instance a Dutch law states that
civilians can request a benefit from their municipality under specific conditions.
From this a web service can be made available that accepts such request. The PRA app
uses published PRA models to categorize these regulations to help users find them,
explain them to them and let them use them.

First, these models are based on law texts through containing CALCULEMUS/FLINT models
(as supported by the discipl software stack (see https://gitlab.com/discipl/PRA/discipl-python)).
A CALCULEMUS/FLINT model typically is contained in a .flint.json file having an official
ID as it's name, though the name of this file does not matter.

Secondly the PRA model enriches the CALCULEMUS/Flint models with discipl-python specific
implementation of the FLINT Fact functions though a separate python module. These are contained
in a .py file with the same ID as name in the same folder as the FLINT model json file.
A template fact function implementation file can be generated from a FLINT model json file.
It will contain a class with all fact functions as methods along with the flint explanation
and sources as comment for the programmer to use. In the future something with unit test specifications
could be done for the more complicated functions. Most functions are probably very simple
and unambiguous though. Note that all fact functions must have a unique name for a single source of law
(each law source gets its own json file which can refer to each other).

Thirdly the PRA model contains a Rasa model with which the users are capable of

- **find** finding the actions in the FLINT model users can execute given their goal and situation.
  A regulation typically states that certain kinds of people can execute a certain action to get
  a specific result they are in need of. To let users find these specific actions with a specific result
  , which we could call a service, in terms of their own need it is necessary to categorize these services
  to what within the Discipl framework is called "non controversial needs". More information about this below.

- **explain** use the FLINT model with the goal to explain the regulation to them (in accessible language).
  The official regulation is often stated in complex and abstract jurisdictional language
  and often only in a specific language (Dutch for instance). The Rasa model contains the explanation in
  more accessible language and possibly translations into a range of other languages (either translated by hand or
  automatically). The PRA app could also auto translate to languages not contained in the Rasa model.

- **test** use the FLINT model with the goal to determine whether they meet the conditions or not (a test calculation).
  The user can enter fictive information which does not have to be verified to be true, though in some cases
  it helps the user to get more confidence to use their information than can be verified to be true. Of course
  this goal goes hand in hand with explaining the regulation.

- **act** use the FLINT model to actually execute an action specified within the regulation. For this the PRA model additionally
  needs to have the information to find probably the webservice to deliver a request to and to be able to
  request status information on previous requests and so on. At this moment it is yet unclear how this is to be coordinated
  but the idea is to support projects on regels.overheid.nl and VIL on this.

These four goals form 4 main intents in the Rasa model in every PRA model

Outside of these regulation specific PRA models the PRA app contains a core PRA model containing a core Rasa model
that let users find and use the PRA models as well as keep track of actions started through the **act** intent.
This core model also lets users keep track of verifiable information held in one or more verifiable claim wallets (as supported by the discipl software stack
which will include W3C VC compatible wallets as well as a forthcoming European Digital Identity wallet (which is pretty much the same concept))
to be used in test calculations or actions. It also enables users to find services for which the test calculation has specific results
given information held in wallets.

## Finding services using categorisation on non controversial needs through ONFAS

The PRA app must support end users to find services (in dutch: regeling) through indicating so called non controversial needs.
These are verbs that are not specific to people, location, time etc. that indicate a Why someone would need something and always
a Why a child could have. There are only a fairly limited list of such needs (think of food, shelter, support, stability etc. have a look at: https://www.cnvc.org/training/resource/needs-inventory). On the basis of such a need as a Why, end users must be able to filter on aspects of such need (What, Where, When, How, ...).  in the following ways
or a combination thereof:

- non controversial need (Why) : as described above. For the dutch example use case (IIT) for instance: **Subsistence, Support, Stability, Food, Water, Shelter**
- name (What) : the name of the service. For the dutch example use case (IIT) for instance: **Individuele Inkomenstoeslag**
- location (Where) : where the service is valid. For the dutch example use case (IIT) for instance: **Utrecht, the Netherlands**
- date (When) : valid from / thru. For the dutch example use case (IIT) for instance: > **1st april 2022**
- life event (When) : life events for which the service may be of particular interest. For instance: **become an adult. Wedding, Death, ...**
- organisational domain (through whom) : governmental domain the service belongs to (see https://www.noraonline.nl/wiki/Domeinen). For the dutch example use case (IIT) for instance: **Subsidies uitkeringen en toeslagen**
- organisation (in dutch: "bestuursorgaan" as defined in law Awb) responsible for the execution of the service (through whom). For the dutch example use case (IIT) for instance: **Gemeente Utrecht (OIN:xxx)**
- description (How): description of the service in few sentences

(Note that this list may not be complete)

End users should be able to search on keywords or through breadcrumb navigation, or through answering a first question of the chatterbot: "What do you need?". The user can start
searching on any of the fields mentioned above and refine their search with the other fields.

The PRA app enables end users to search through all services published (as PRA model) this way. If services are not supported (having a PRA model published)
the PRA app could try to refer the end user to https://www.overheid.nl/dienstverlening/uitgebreid-zoeken

For services to be found, a PRA model contains for each supported language a .onfas.json file containing a ONFAS (Open Needs Focussed Artifact Standard (yet to be defined)) descriptor which will look something like:

```
{
    "onfas": "1.0",
    "language": "dutch"
    "need" : {
      "Levensonderhoud",
      "Ondersteuning",
      "Stabiliteit",
      "Eten",
      "Voedsel",
      "Drinken",
      "Water",
      "Onderdak",
      "Inkomen"
    },
    "name" : "Individuele Inkomenstoeslag",
    "location" : "Utrecht, Nederland",
    "date" : {
      "from" : "2022-04-01"
    },
    "life-event" : {
      "18 jaar worden",
      "Scheiden",
      "Met pensioen gaan"
    },
    "domain" : "Subsidies uitkeringen en toeslagen",
    "organsiation" : "Gemeente Utrecht (OIN:xxx)",
    "description" : "De Individuele Inkomenstoeslag is een aanvullende inkomensondersteuning, een vorm van bijzondere bijstand, die op aanvraag door gemeenten onder voorwaarden verstrekt kan worden aan personen woonachtig in die gemeente die een langdurig laag inkomen hebben en ook geen vermogen hebben dit op te vangen. De aanvrager moet dan minimaal 21 jaar zijn en jonger dan de pensioen gerechtigde leeftijd hebben en ook geen uitzicht hebben op inkomensverbetering...",
    "precondition" : [
      {
        "fact" : "[search-precondition]",
        "function" : "[21 jaar en ouder],[jonger dan AOW gerechtigde leeftijd]"
        "sources" [

        ],
        explanation : ""
      },
      {
        "fact" : "[21 jaar en ouder]",
        "sources" [

        ],
        explanation : ""
      },
      ...
    ]
}
```

These ONFAS descriptors are defined for one or more languages and contain all the information needed for an end user to engage with (explain, test and or act in relation to it).
When languages are not supported, they could be auto-translated though this might result in poor searching experience. Also when searching, all fields in the descriptor denote keywords that can be matched with user search input. This matching could include the usage of language models too to improve searching.

In an additional more complicated search filter, the user can be matched with a description of (simplified) requirements that denote for whom this service might be intended for (rule of thumb version). This would refer to FLINT facts specially introduced for this service which works just like Flint models (see next section) so with additional python code implementation for the facts mentioned in the descriptor to be executed in a (very) limited environment. For the dutch example use case (IIT) for instance this would refer to some of the more simple conditions published in the municipalities' website for requesting this service: https://pki.utrecht.nl/Loket/product/0c17f7cd409dc999eb351883a138ca3d for instance the condition whether the user is over 21. The user can see the result for this simplified condition (seems eligible yes/no) and can filter on this. This of course only works when the user has given the PRA app access to for instance a SSI wallet containing their age or birth date for this purpose. This additional filter functionality, performing on the fly test's on a rule of thumb version of the requirements for the service can also be used for pro-active services. Note that besides PRA app to wallet communication, that should typically be contained in the end users device, the user can be certain no search information leaves the device the PRA application is hosted on, unless explicitly allowed by the user.

The PRA application will have to download all PRA models or at least all ONFAS descriptors of all services that can be found through the PRA app. In the end this will be quite a
considerable amount of data. Within the Netherlands, a quick search on all available services (as can be found through overheid.nl) gives the amount of 50.000 services. Given a
PRA model size (mainly a compressed flint and trained rasa model) of 25Mb this would require the PRA app to download 1,25 Tb which is too much. To a certain degree, we do want the PRA app be able to work offline too however and not leak information about who is looking at what. To be able to search and find services without downloading all underlying PRA models that include trained Rasa models, a lightweight PRA model could be defined (containing only ONFAS descriptors) which would probably give a download of 50Mb for all services together.

It is envisioned search statistics can be shared by users anonymously (statistics around keywords searched, matched in descriptors per region) and visualised in a public dashboard. This might help find gaps in what people need and what is offered to engage with these needs.

## Explaining regulations in different levels of languages

The CALCULEMUS/FLINT models contain legal texts that probably not suited for most end users. The PRA app should give a clear explanation of the legal texts in their own level of accessibility (for instance : simplified, advanced, legal). Detecting at what level to communicate to the user should be performed automatically on the fly or on indication of the end user. When a user asks for it, the PRA should always be able to link an explanation of concepts between levels. Take for instance:

```
PRA app : you need to be over 21 to make use of this service.
User    : why?
PRA app : because it is not intended for children for which __other services__ already apply. Legally because of article 36 of the __Participatiewet__
```

All this is contained in the Rasa model within the PRA model. How is yet to be determined.

## Letting users find out what use the service can be for them by performing a test.

A regulation often includes a description of precondition(s) for determining whether a person can make use of it. Also it often includes a description of what the result would be if a user makes use of it. A user might want to know whether a service is useful for them or not. In the CALCULEMUS/FLINT model, there are so called starting acts. These acts are not referenced by other acts or duties within the same PRA model. Typically there is only one such starting act with the end user as actor. These acts define preconditions that determin
the requirements a user must meet to make use of the service. Additionally, other acts involve facts that determin the result once a person would make use of the service and this might rely on additional data to be provided by the user. The PRA app should be able to use the FLINT model to test whether the user can make use of the service and what the result would be. While doing so, the PRA app should explain. Also the user must be able to enter / use fictive data and or be helped to use verifiable official data (and thereby prepare for actually making use of the service).

All this is contained in the Rasa model within the PRA model. How is yet to be determined.

## Letting users make use of services__

Once interested in a service that engages with a users' need, the user can make use of the service through the PRA app. For this it helps the user get all required verifiable information and submit this along with an appropriate request to a webservice specifically hosted for the service. The webservice can be found on the basis of a legal reference. For the dutch example use case (IIT) for instance: "service:https://lokaleregelgeving.overheid.nl/CVDR616379" that should refer to the appropriate public webservice of the gemeente Utrecht. How is yet to be determined. The PRA app must however be able to verify the webservice is actually hosted by the organisation responsible for the execution of the regulation.
