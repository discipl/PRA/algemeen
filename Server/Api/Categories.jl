module Categories

using Genie, Genie.Requests, Genie.Renderer.Json
using HierarchicalConfigs: dataframe2dictvec
using SwaggerMarkdown

import ..Main.DataModel
import ..Main.GlobalConstants.SCHEME_LOCATION

# This are just for creating docs, so they do not need to be updated after using the Editor
const kinds = unique(DataModel.categorieen[:, "Categorie.Soort"])
const languages = unique(DataModel.categorieen[:, "Categorie.Taal"])

response = """
    responses:
      '200':
        description: OK
        content:
          application/json:
            schema:
              type: object
              properties:
                results:
                  type: array
                  items:
                    \$ref: '$SCHEME_LOCATION#/Categorie'
"""

# Docs in Open API v3.0 format
@swagger """
/categories/{kind}/{language}:
  get:
    description: All category models of the same kind.
    parameters:
      - in: path
        name: kind
        schema:
          type: string
          enum: [$(join((x->"\"$x\"").(kinds), ","))]
        example: $(last(kinds))
        required: true
        description: The kind of categories to be retrieved.
      - in: path
        name: language
        schema:
          type: string
          enum: [$(join((x->"\"$x\"").(languages), ","))]
        required: true
        description: "The used language."
        example: $(last(languages))
$response
  post:
    description: All category models of the same kind.
    parameters:
      - in: path
        name: kind
        schema:
          type: string
          enum: [$(join((x->"\"$x\"").(kinds), ","))]
        example: $(first(kinds))
        required: true
        description: The kind of categories to be retrieved.
      - in: path
        name: language
        schema:
          type: string
          enum: [$(join((x->"\"$x\"").(languages), ","))]
        required: true
        description: "The used language."
        example: $(first(languages))
$response

/categories:
  post:
    summary: Get categories.
    requestBody:
        description: Criteria for the wanted item.
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                kind:
                  description: The kind of categories to be retrieved.
                  type: string
                  enum: [$(join((x->"\"$x\"").(kinds), ","))]
                  example: $(first(kinds))
                language:
                  description: The used language.
                  type: string
                  enum: [$(join((x->"\"$x\"").(languages), ","))]
                  example: $(first(languages))
$response
"""

route("/categories/:kind/:language", method = GET) do
    return categories_response(payload(:kind), payload(:language))
end
route("/categories/:kind/:language", method = POST) do
    return categories_response(payload(:kind), payload(:language))
end
route("/categories", method = POST) do
    return categories_response(jsonpayload()["kind"], jsonpayload()["language"])
end

function categories_response(kind::AbstractString, language::AbstractString)
    results = DataModel.categorieen[(DataModel.categorieen[:, "Categorie.Soort"] .== kind) .& (DataModel.categorieen[:, "Categorie.Taal"] .== language), :]
    return json((;
        results = dataframe2dictvec(results)
    ))
end

end # module Categories
