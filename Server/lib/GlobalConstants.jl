module GlobalConstants

const PROCESSED_DATA_PATH = joinpath("data", "processed")
const EXTERNAL_DATA_PATH = joinpath("data", "external")
const PRA_MODELS_PATH = get(ENV, "PRA_MODELS_PATH", joinpath("public", "pra-models"))
const SCHEME_PATH = joinpath("public", "schemes", "pra-models.yaml")
const SERVER_PORT = parse(Int, get(ENV, "SERVER_PORT", "8080"))
const APP_HOST = get(ENV, "APP_HOST", "localhost")
const APP_PORT = get(ENV, "APP_PORT", "")
const APP_BASEPATH = get(ENV, "APP_BASEPATH", "")
const APP_LOCATION = "//$APP_HOST$(isempty(APP_PORT) ? "" : ":$APP_PORT")$(isempty(APP_BASEPATH) ? "" : "/$APP_BASEPATH")"
const SCHEME_LOCATION = "$APP_LOCATION/$(join(splitpath(SCHEME_PATH)[2:end], "/"))"

@show SERVER_PORT
@show APP_LOCATION
@show SCHEME_LOCATION

end
