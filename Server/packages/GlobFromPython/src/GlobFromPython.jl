# When using PythonCall with precompile the module will cause a segmentation fault
__precompile__(false)
module GlobFromPython

export glob

using PythonCall

const pyglob = pyimport("glob")

"""
Wrapper function for Python's glob which supports recursive globbing
with double astrisks (**/). Will interpret "/" always as a path seperator, also
on windows.
"""
glob(pattern) = pyconvert(Vector{String}, pyglob.glob(joinpath(split(pattern,"/")...),
                                                      recursive=true))

"""
Python's glob with a `prefix` paramter, like Julia's glob.
"""
glob(pattern, prefix) = glob("$prefix/$pattern")

end # module GlobFromPython
