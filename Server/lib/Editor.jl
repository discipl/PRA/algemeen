module Editor

using OrderedCollections
using YAML
using DataFrames

using GlobFromPython
using HierarchicalConfigs

import ..Main.EditorDataInterface as EDI
import ..Main.DataModel
import ..Main.SchemeModel

using ..Main.FieldsEditor

import ..Main.EditorBuilder as EB

#setup of the Genie Framework environment
using GenieFramework
@genietools

# reactive code
@app EditorReactiveModel begin
    @in initialized = false
    @out channel = ""
    @in call::Any = nothing
    @in splitterM = 30.0
    @in leftTab = "models_tree"
    @in tree_selected::Any = nothing
    @in tree_expanded::Vector{Int} = []
    @in tree_ticked = Int[]
    @out tree_nodes::Vector{Dict{String, Any}} = []
    @in selectedItem = OrderedDict{String, Any}("rownumber"=>nothing, "label"=>"", "hasmodel"=>false)
    @in selectedModel = OrderedDict()
    @in localdata = OrderedDict()
#    @out changed_model_by_self = false
    #@onchange splitterM begin
        #@show splitterM
    #end
    @onchange tree_selected if isnothing(selectedItem["rownumber"]) || selectedItem["rownumber"] != tree_selected
        @show tree_selected selectedItem
        #if !isnothing(selectedRownumber)
            # save previous selected
            #EDI.save_item!(EB.shared_tree, selectedRownumber, selectedLabel, selectedIndex, selectedModel)
        #end
        if EDI.haschanges(EB.shared_tree, selectedItem, selectedModel)
            notify(__model__,
               "There are unsaved modifications to the selected item. Use the 'REVERT' or 'SAVE' button before changing the selection.",
                :warning,
                ;
                icon="edit",
                caption="Unsaved changes",
                timeout=1_000*10,
                position="center",
                closeBtn="OK",
            )
            # set selected back to what it was
            tree_selected = selectedItem["rownumber"]
        else
            selectedItem = EDI.get_item(EB.shared_tree, tree_selected)
            selectedModel = EDI.get_model(EB.shared_tree, tree_selected)
            EB.call_inits(__model__)
        end
    end
#    @onchange selectedModel begin
#        if changed_model_by_self
#            changed_model_by_self = false
#        elseif "Type" in keys(selectedModel)
#            minimal_model = EDI.minimal_model(EB.shared_tree, tree_selected, selectedModel["Type"])
#            filter!(minimal_model) do p
#                !(first(p) in keys(selectedModel))
#            end
#            if !isempty(minimal_model)
#                changed_model_by_self = true
#                merge!(selectedModel, minimal_model)
#                notify(selectedModel)
#            end
#        end
#    end
#    @onchange tree_expanded begin
#    end
#    @onchange tree_ticked begin
#    end
#    @onchange selectedLabel begin
## this is commented to make label changes revertable
##        if !isnothing(tree_selected)
##            items[tree_selected, :_label] = selectedLabel
##            #tree_nodes = EDI.build_tree(sort(items, :_rownumber, rev=true))
##            update_tree()
##        end
#    end
    @onchange call if !isnothing(call)
        @show call
        fnname = Symbol(call[1])
        @show fnname
        if fnname == :show_all_ticked
            to_show = unique(vcat(EDI.all_ancestors.(EB.shared_tree, tree_ticked)...))
            union!(tree_expanded, to_show)
            notify(tree_expanded)
        elseif fnname == :hide_non_ticked
            to_show = unique(vcat(EDI.all_ancestors.(EB.shared_tree, tree_ticked)...))
            intersect!(tree_expanded, to_show)
            notify(tree_expanded)
        elseif fnname == :delete_ticked
            not_deleted = EDI.delete_items!(EB.shared_tree, tree_ticked)
            if !isempty(not_deleted)
                notify(__model__,
                   "The following $(length(not_deleted)) item$(length(not_deleted) > 1 ? "s" : "") could not be deleted because $(length(not_deleted) > 1 ? "they have" : "it has") unticked descendants: $(join(EDI.get_label.(EB.shared_tree, not_deleted), ", "))",
                    :warning,
                    ;
                    icon="delete",
                    caption="Deletion warning",
                    timeout=1_000*10,
                    position="center",
                    closeBtn="Close",
                )
            end
            if tree_selected in tree_ticked && !(tree_selected in not_deleted)
                selectedModel = OrderedDict()
                # previous rownumber is set to nothing since it does not exist anymore
                # this prevents cousing a save_item! when changing tree_selected
                #selectedRownumber = nothing
                selectedItem = OrderedDict("rownumber"=>nothing, "label"=>"", "hasmodel"=>false)
                tree_selected = nothing
            end
            tree_ticked = not_deleted
        elseif fnname == :move_selected_to_here
            # TODO: prevent items from being moved underneath itself (this is already blocked from the frondend)
            here_rownumber, = call[2:end]
            @show here_rownumber
            EDI.move_items_to!(EB.shared_tree, [tree_selected], here_rownumber)
            union!(tree_expanded, here_rownumber)
            notify(tree_expanded)
        elseif fnname == :move_ticked_to_here
            # TODO: prevent items from being moved underneath itself (this is already blocked from the frondend)
            here_rownumber, = call[2:end]
            @show here_rownumber
            EDI.move_items_to!(EB.shared_tree, tree_ticked, here_rownumber)
            union!(tree_expanded, here_rownumber)
            notify(tree_expanded)
        elseif fnname == :add_new_here
            here_rownumber, = call[2:end]
            tree_selected = EDI.add_new_item!(EB.shared_tree, here_rownumber)
            union!(tree_expanded, here_rownumber)
            notify(tree_expanded)
        elseif fnname == :delete
            selectedItem["hasmodel"] = false
            notify(selectedItem)
            selectedModel = OrderedDict()
        elseif fnname == :revert
            selectedItem = EDI.get_item(EB.shared_tree, tree_selected)
            selectedModel = EDI.get_model(EB.shared_tree, tree_selected)
        elseif fnname == :save
            selectedModel = EDI.save_item!(EB.shared_tree, selectedItem, selectedModel)
        elseif fnname == :edit
            model = EDI.add_model(EB.shared_tree)
            selectedModel = model
            selectedItem["hasmodel"] = true
            notify(selectedItem)
        else
            arglist =call[2:end]
            EB.call(fnname, __model__, arglist)
        end
        call=nothing
    end
end

Stipple.client_data(::EditorReactiveModel) = Stipple.client_data(treeRef=js"null", fieldsScrollAreaRef=js"null", options=js"{}", updater=js"0")

@mounted EditorReactiveModel """
    console.log('This app has just been mounted!!!')
    Vue.prototype.console = console
    //Main_Editor_EditorReactiveModel.\$refs.fieldsScrollArea.setScrollPercentage('vertical', 1.0)
"""

#TODO delete this
#, @on("hook:mounted", R"this.console.log($refs.fieldsScrollArea); $refs.fieldsScrollArea.setScrollPercentage('vertical', 1.0)")
#@watch EditorReactiveModel [
#    :initialized => """function(newValue) {
#    this.console.log('initialized=', newValue)
#    //this.\$refs.fieldsScrollArea.setScrollPercentage('vertical', 1.0)
#    this.fieldsScrollAreaRef.setScrollPercentage('vertical', 1.0, 100)
#}
#"""
#]

#js_methods(::EditorReactiveModel) = EB.render_js_methods
@methods EditorReactiveModel EB.render_js_methods

EB.register_js_method(
    :updateSelected => """function(newValue) {
        if (newValue === null) {
            this.console.log("collapsing:", this.selectedItem)
            if (this.selectedItem.rownumber !== null)
                this.treeRef.setExpanded(this.selectedItem.rownumber, false)
        } else {
            this.treeRef.setExpanded(newValue, true)
        }
        this.tree_selected = newValue
    }""")
    # TODO? maybe _allDescendantsOf is a better name?
EB.register_js_method(
    :_allNodesFrom => """function(nodes) {
        return nodes.map(x=>x.rownumber).concat(...nodes.filter(x=>x.children !== undefined).map(x=>x.children).map(this._allNodesFrom))
    }""")
EB.register_js_method(
    :tickAllChildren => """function(node) {
        const childIds = node.children.map(x=>x.rownumber)//.filter(x=>!this.treeRef.isTicked(x))
        this.treeRef.setTicked(childIds, true)
    }""")
EB.register_js_method(
    :tickAllFromHere => """function(node) {
        this.treeRef.setTicked(this._allNodesFrom([node]), true)
    }""")
EB.register_js_method(
    :untickAllChildren => """function(node) {
        const childIds = node.children.map(x=>x.rownumber)//.filter(x=>this.treeRef.isTicked(x))
        this.treeRef.setTicked(childIds, false)
    }""")
EB.register_js_method(
    :untickAllFromHere => """function(node) {
        this.treeRef.setTicked(this._allNodesFrom([node]), false)
    }""")
EB.register_js_method(
    :expandAllChildren => """function(node) {
        this.treeRef.setExpanded(node.rownumber, true)
        node.children.map(x=>x.rownumber)
                     .filter(x=>!this.treeRef.isExpanded(x))
                     .forEach(x=>this.tree_expanded.push(x))
    }""")
EB.register_js_method(
    :expandAllFromHere => """function(node) {
        this._allNodesFrom([node]) // this results in rownumbers
            .filter(x=>!this.treeRef.isExpanded(x))
            .forEach(x=>this.tree_expanded.push(x))
    }""")
EB.register_js_method(
    :collapseAllChildren => """function(node) {
        this.treeRef.setExpanded(node.rownumber, true)
        const toCollapse = node.children.map(x=>x.rownumber).filter(x=>this.treeRef.isExpanded(x))
        this.tree_expanded.sort(x=>toCollapse.includes(x))
        this.tree_expanded.splice(this.tree_expanded.length-toCollapse.length, toCollapse.length)
    }""")
EB.register_js_method(
    :collapseAllFromHere => """function(node) {
        const toCollapse = this._allNodesFrom([node]).filter(x=>this.treeRef.isExpanded(x))
        this.tree_expanded.sort(x=>toCollapse.includes(x))
        this.tree_expanded.splice(this.tree_expanded.length-toCollapse.length, toCollapse.length)
    }""")
    #:_allAncestorsOf => """function(nodes) {
    #   too hard for js
    #   this is now available via the 'ancestors' property of a node
    #}""",
EB.register_js_method(
    :moveSelectedMsg => """function(node) {
        if (this.tree_selected === null)
            return "Nothing selected"
        else if (node.ancestors.includes(this.tree_selected))
            return "Can not move something down its own tree"
        else
            return "Move selected (with all its descendants) here"
    }""")
EB.register_js_method(
    :moveTickedMsg => """function(node) {
        if (this.tree_ticked.length === 0)
            return "Nothing ticked"
        else if (this.tree_ticked.some(x=>node.ancestors.includes(x)))
            return "Can not move something down its own tree"
        else
            return "Move all ticked (with all their descendants) here"
    }""")
EB.register_js_method(
    :filterFn => """function(fieldname, stringOptions) {
      this.options[fieldname] = stringOptions
      return function(val, update, abort) {
        update(() => {
          const needle = val.toLowerCase()
          this.options[fieldname] = stringOptions.filter(v => v.toLowerCase().indexOf(needle) > -1)
        })
      }
    }""")

treenav() = tree("",
  style="padding: 16px 0;",
  ref="treeRef",
  # Put the ref in a (client only) datafield for better performance
  @on("hook:mounted", R"treeRef=$refs.treeRef;
                        this.console.log('tree mounted and is now accessible via the treeRef client variable');"),
  :dense,
  :no__transition,
  nodes=R"tree_nodes",
  node__key="rownumber",
  selected=R"tree_selected",
  expanded=R"tree_expanded",
  ticked=R"tree_ticked",
  @on("update:selected", R"updateSelected"),
  @on("update:expanded", R"newValue => tree_expanded = newValue"),
  @on("update:ticked", R"newValue => tree_ticked = newValue"),
  selected__color="accent",
  tick__strategy="strict", [
    template("", var"v-slot:default-header"="prop", [
        Html.div(class="row items-center", [
            icon(R"prop.node.icon", color=R"prop.node.iconcolor"),
            "{{ prop.node.label }}",
        ]),
        tree_contextmenu()
    ]),
    template("", var"v-slot:default-body"="prop", [
        Html.div("", @iif(R"prop.node.description"), [
            "{{ prop.node.description }}",
        ])
    ])
])

const menu = StippleUI.menu

tree_contextmenu() = menu("", :touch__position, :context__menu, [
    list("", :dense, style="min-width: 100px;", [
        item("", :clickable, :v__close__popup,
          @click(R"call=['add_new_here', prop.node.rownumber]"), [
            itemsection("Add new here")
        ]),
        item("", :clickable, [
            itemsection("Move ... to here"),
            itemsection("", :side, [ icon("keyboard_arrow_right") ]),
            menu("", anchor="top end", self="top start", [
                list([
                    item("", :dense, :clickable, :v!__close__popup,
                      @click(R"if (!(tree_selected === null || prop.node.ancestors.includes(tree_selected))) call=['move_selected_to_here', prop.node.rownumber]"),
                      disabled=R"tree_selected === null || prop.node.ancestors.includes(tree_selected)",
                      title=R"moveSelectedMsg(prop.node)", [
                        itemsection("the selected one")
                    ]),
                    item("", :dense, :clickable, :v__close__popup,
                      @click(R"if (!(tree_ticked.length === 0 || tree_ticked.some(x=>prop.node.ancestors.includes(x)))) call=['move_ticked_to_here', prop.node.rownumber]"),
                      disabled=R"tree_ticked.length === 0 || tree_ticked.some(x=>prop.node.ancestors.includes(x))",
                      title=R"moveTickedMsg(prop.node)", [
                        itemsection("all the ticked ones")
                    ])
                ])
            ])
        ]),
    ]),
    list("", :dense, @iif(R"prop.node.children.length !== 0"), [
        separator(),
        item("", :clickable, [
            itemsection("Tick all ..."),
            itemsection("", :side, [ icon("keyboard_arrow_right") ]),
            menu("", anchor="top end", self="top start", [
                list([
                    item("", :dense, :clickable, :v__close__popup,
                      @click(R"tickAllChildren(prop.node)"), [
                        itemsection("children")
                    ]),
                    item("", :dense, :clickable, :v__close__popup,
                      @click(R"tickAllFromHere(prop.node)"), [
                        itemsection("from here")
                    ])
                ])
            ])
        ]),
        item("", :clickable, [
            itemsection("Untick all ..."),
            itemsection("", :side, [ icon("keyboard_arrow_right") ]),
            menu("", anchor="top end", self="top start", [
                list([
                    item("", :dense, :clickable, :v__close__popup,
                      @click(R"untickAllChildren(prop.node)"), [
                        itemsection("children")
                    ]),
                    item("", :dense, :clickable, :v__close__popup,
                      @click(R"untickAllFromHere(prop.node)"), [
                        itemsection("from here")
                    ])
                ])
            ])
        ])
    ]),
    list("", :dense, @iif(R"prop.node.children.some(x=>x.children.length !== 0)"), [
        separator(),
        item("", :clickable, [
            itemsection("Expand all ..."),
            itemsection("", :side, [ icon("keyboard_arrow_right") ]),
            menu("", anchor="top end", self="top start", [
                list([
                    item("", :dense, :clickable, :v__close__popup,
                      @click(R"expandAllChildren(prop.node)"), [
                        itemsection("children")
                    ]),
                    item("", :dense, :clickable, :v__close__popup,
                      @click(R"expandAllFromHere(prop.node)"), [
                        itemsection("from here")
                    ])
                ])
            ])
        ]),
        item("", :clickable, [
            itemsection("Collapse all ..."),
            itemsection("", :side, [ icon("keyboard_arrow_right") ]),
            menu("", anchor="top end", self="top start", [
                list([
                    item("", :dense, :clickable, :v__close__popup,
                      @click(R"collapseAllChildren(prop.node)"), [
                        itemsection("children")
                    ]),
                    item("", :dense, :clickable, :v__close__popup,
                      @click(R"collapseAllFromHere(prop.node)"), [
                        itemsection("from here")
                    ])
                ])
            ])
        ])
    ])
])

ui() = splitter(:splitterM, class="full-height", [
    template("", "v-slot:before", class="full-height", [
        Html.div(class="full-height", style="display: flex; flex-direction: column;", [
            q__tabs("",
                var"v-model"="leftTab",
                :dense,
                "inline-label",
                [
                    q__tab("", name="models_tree", label="Models Tree", icon="sort")
                    q__tab("", name="flint_facts", label="Flint Facts",
                        #icon="playlist_add_check_circle",
                        icon="gavel",
                        # TODO make this more general for when/if other Type's of models can be linked to Flint
                        var":disable"="selectedModel.Type != 'Regeling'"
                    )
            ])
            separator("")
            q__tab__panels("",
                var"v-model"="leftTab",
                "keep-alive",
                animated=false,
                swipeable=false,
                class="full-height full-width",
                [
                    q__tab__panel("",
                        name="models_tree",
                        class="full-height full-width",
                        style="padding:0;",
                        [
                            Html.div(class="full-height", style="display: flex; flex-direction: column;", [
                                Html.div(class="", [
                    #                btn("", padding="none", color="primary",
                    #                  #icon="priority",
                    #                  icon="assignment_return", [
                    #                    tooltip("Untick all")
                    #                ]),
                                    Html.div("", class="q-pa-md q-gutter-sm", [
                                        btn("", padding="none", color="primary", :unelevated,
                                          #icon="priority",
                                          #icon="assignment_turned_in",
                                          #icon="preview_off",
                                          icon="indeterminate_check_box",
                                          @click(R"call=['hide_non_ticked']"), [
                                            tooltip("Collapse all not contain tickeds")
                                        ]),
                                        btn("", padding="none", color="primary", :unelevated,
                                          #icon="priority",
                                          #icon="assignment_turned_in",
                                          #icon="preiew",
                                          icon="add_box",
                                          @click(R"call=['show_all_ticked']"), [
                                            tooltip("Expand all containing ticked")
                                        ]),
                                        btn("", padding="none", color="negative", :unelevated,
                                          icon="delete",
                                          @click(R"call=['delete_ticked']"), [
                                            tooltip("Delete all ticked")
                                        ]),
                                    ]),
                                    separator("", style="margin-right: 16px;")
                                ])
                                scrollarea("",
                                    class="full-height", style="max-width: 100%; flex-grow: 1;", [
                                    template("", "v-slot:default", class="full-height", [
                                        treenav(),
                                    ])
                                ])
                            ])

                    ])
                    q__tab__panel("",
                        name="flint_facts",
                        class="full-height full-width",
                        style="padding:0;",
                        [
                            q__table("",
                                class="full-height full-width",
                                # TODO make this more general for when/if other Type's of models can be linked to Flint
                                var":data"="selectedModel['Flint.Frames']",
                                var":columns"="[
                                    {name: 'facts_used_for_a_condition', label: 'Verwerkt', field: row => (localdata.facts_used_for_a_condition || []).includes(row.id) || (m=>{
                                            for (const cc of selectedModel['Regeling.Content']) {
                                                for (const cb of cc.ContentBlocks){
                                                    if (cb.ContentBlockType == 'HtmlContentBlock' && cb.Body.includes(m)){
                                                        return true
                                                    }
                                                }
                                            }
                                            return false
                                        })(row.mark) ?  '&#9989;' : '' },
                                    {name: 'label', label: 'Fact', field: 'label', align:'left'},
                                    ]",
                                var"row-key"="id",
                                selection="single",
                                var":selected.sync"="localdata['flintFactSelected']",
                                var":pagination"="{rowsPerPage: 15}",
                            )
                    ])
            ])
        ])
    ]),
    template("", "v-slot:after", class="full-height full-width", @iif("selectedItem['rownumber'] !== null && tree_selected"), [
        Html.div("", class="full-height full-width", style="display: flex; flex-direction: column;", @iif("selectedItem['hasmodel']"), [
            Html.div("", class="q-pa-md q-gutter-sm", [
                textfield("Bestandsnaam", Symbol("selectedItem['label']"), :outlined, :square, :stack__label, bg__color="white")
                btn("Delete fields",  color="grey", :unelevated,
                  icon="delete",
                  @click(R"call=['delete']"))
                btn("Revert",  color="blue-3", :unelevated,
                  icon="restore_page",
                  @click(R"call=['revert']"))
                btn("Save",  color="primary", :unelevated,
                  icon="save",
                  @click(R"call=['save']"))
                btn("View",  color="secondary", :unelevated,
                  icon="open_in_new",
                  type="a",
                  href! = "'https://discipl.gitlab.io/PRA/pra-front-end/' + (selectedModel['Type'] == 'Regeling' ? 'regulations' : 'life-events') + '/' + selectedModel['Id']",
                  target="_blank",
                  @iif("(selectedModel['Type'] == 'Regeling' || selectedModel['Type'] == 'Levensgebeurtenis') && selectedModel['Compleet']")
                )
            ])
            separator("", style="margin-right: 16px;")
            scrollarea("",
                ref="fieldsScrollArea",
                @on("hook:mounted", R"fieldsScrollAreaRef=$refs.fieldsScrollArea"), #TODO is this ref needed?
                class="full-height full-width", style="flex-grow: 1;", [
                template("", "v-slot:default", [
                    Html.div("", class="full-height full-width", style="padding: 0 16px;", [
                        FieldsEditor.fieldseditor()
                    ])
                ])
            ])
        ])
        Html.div("", "v-else", [
            Html.div("", class="q-pa-md q-gutter-sm", [
                textfield("Bestandsnaam", Symbol("selectedItem['label']"), :outlined, :square, :stack__label, bg__color="white")
                btn("Add fields",  color="green-4", :unelevated,
                  icon="edit",
                  @click(R"call=['edit']"))
                btn("Revert",  color="blue-3", :unelevated,
                  icon="restore_page",
                  @click(R"call=['revert']"))
                btn("Save",  color="primary", :unelevated,
                  icon="save",
                  @click(R"call=['save']"))
            ])
        ])
    ])
])

mytheme() = [
    "<link href=\"https://fonts.googleapis.com/css?family=Material+Icons\" rel=\"stylesheet\" />",
    "\n",
    # Put back StippleUI's changes to quasar
    style("""\n
    .q-btn {
        text-transform: uppercase;
    }
    .q-btn--no-uppercase {
        text-transform: none;
    }
    :root {
        --q-color-primary: #1976d2;
        --q-color-secondary: #26a69a;
        --q-color-accent: #9c27b0;
        --q-color-positive: #21ba45;
        --q-color-negative: #bd5631;
    """),
    # Our own stylesheet
    style("""\n
    body {
        background: #f1f3f7;
    }
    html, body {
        height: 100%;
    }
    .q-btn {
        font-weight: 700;
        line-height: 2.0em;
    }
    :root {
        --st-text-1: #302f2f;
        --st-text-2: #575454;
        --st-text-3: #5a6460;
        --st-dashboard-module: #fefefe;
        --st-dashboard-line: #e2e2e2;
        --st-dashboard-bg: #f1f3f7;
        --st-slider--track: #cad2d2;
        --st-skeleton: #e1e5ee;
    }
    .stipple-core h5 {
        font-size: 1rem;
        line-height: 1.5rem;
        font-weight: 700;
        color: var(--st-text-1);
    }
    """),
    style(
        # because chopper.app uses Base62 for its sentenceId's we use the case sensitive [class~="<class-name>"] syntax instead of the case insensitive .<class-name> syntax
        join("""
            mark[class~="$markid"] {
                background: none;
            }
            [class~="$markid"] mark[class~="$markid"] {
                /* background: #c6ff00; */
                background: #18ffff;
            }
        """ for markid in "mark-" .* string.(0:1000, base=36))
    ),
#    style("""\n
#    .stipple-core .q-input .q-field__native, .stipple-core .q-select .q-field__native {
#         font-weight: initial;
#         font-size: initial;
#     }
#     .stipple-core .q-chip {
#         font-weight: initial;
#     }
#     """),
     ""]
#push!(Stipple.Layout.THEMES[], mytheme)
@deps EditorReactiveModel mytheme

# Remove stipplecore stylesheet
popfirst!(Stipple.Layout.THEMES[])

# ingore core_theme for this function becouse there is a bug in Stipple when it is false
function Stipple.deps(m::EditorReactiveModel; core_theme::Bool = true)
    invoke(Stipple.deps, Tuple{Stipple.ReactiveModel}, m; core_theme=true)
end
# Stipple bug workaround (another fix for same bug as the above fix)
#function Stipple.injectdeps(v::AbstractVector{Any}, M)
#    @show "M" v
#    Stipple.injectdeps(convert(Vector{AbstractString}, filter((x -> x isa AbstractString), v)), M)
#end

function Stipple.injectdeps(v::Vector{AbstractString}, M::Type{<:EditorReactiveModel})
    invoke(Stipple.injectdeps, Tuple{Vector{AbstractString}, Type{<:Stipple.ReactiveModel}}, v, M)
end

EB.register_shared_tree(EDI.SharedTree{EditorReactiveModel}("tree_nodes"))

function build_page()
    global model
#    println("======== /editor request ========")
#    println("request:")
#    println(Genie.Requests.getrequest())
#    println()
#    println("response:")
#    println(Genie.Responses.getresponse())
#    println()
#    println("Requests getheaders()")
#    display(Genie.Requests.getheaders())
#    println("Response getheaders()")
#    display(Genie.Responses.getheaders())
    model = @init(EditorReactiveModel)
    on(model.isready) do _
        model.tree_nodes[] = EDI.current_tree(EB.shared_tree)
        if !model.initialized[]
            #model.tree_expanded[] = EDI.all_parents(shared_tree)
            model.tree_expanded[] = EDI.get_children(EB.shared_tree.items, nothing).rownumber
            model.initialized[] = true
        end
        model.channel[] = model.channel__
        model.selectedItem[] = EDI.get_item(EB.shared_tree, model.tree_selected[])
        EB.call_inits(model)
    end
    @show model.channel__
    display(Stipple.LAST_ACTIVITY)
    display(Stipple.sorted_channels())
    EDI.add_new_client!(EB.shared_tree, model)
    page(model,
        class="container full-height",
        title="PRA-Model Editor",
        head_content=join([
            Stipple.sesstoken(),
            Genie.Assets.favicon_support(),
        ]),
        core_theme=false,
        ui() |> prettify,
        @showif(:isready)
    ) |> html
end

route("/editor") do
#    @info string(Genie.Requests.getrequest())
    build_page()
end

end # module Editor
