The server can be run directly via your local julia installation (most convenient when developing) or as a docker container (better for production).

## Development environment

When running the server via the local development environment the server will automatically
load changes when saved, without the need to restart the server.

### Install Julia

The recommended way to install Julia is via the [juliaup version manager](https://github.com/JuliaLang/juliaup).
Pick one of the installation methods listed in their GitHub's README.

Then run:

```bash
juliaup add 1.10.4
```

### Initialize development environment

In the Server directory:

```bash
julia +1.10.4 --project -e 'using Pkg; Pkg.instantiate()'
```

### Run development server

In the Server directory:

```bash
APP_PORT=8080 julia +1.10.4 --project --interactive -e 'using Genie; Genie.loadapp()'
```

## Docker

### Build docker image

In the Server directory:

```bash
docker build . -t pra-server
docker tag pra-server:latest <docker-repo>/pra-server:latest
docker push <docker-repo>/pra-server
```

### Run local docker container

In the Server directory:

```bash
docker run \
    -p 8080:8080 \
    --mount type=bind,source="$(pwd)"/public/pra-models,target=/genie/Server/public/pra-models \
    -e APP_HOST='localhost' \
    -e APP_PORT='8080' \
    -it \
    --name pra_server \
    --rm \
    -d \
    pra-server:latest
```

### Run docker container on Azure VM

In the Server directory:

```bash
docker run \
    -p 8080:8080 \
    --mount type=bind,source="$(pwd)"/public/pra-models,target=/genie/Server/public/pra-models \
    -e APP_HOST='<FQDN of backend server>' \
    -it \
    --name pra_server \
    --rm \
    -d \
    <docker-repo>/pra-server:latest
```

### Attach to running container

```bash
docker attach pra_server
```

### Detach from running container

<kbd>Control</kbd>+<kbd>P</kbd> then <kbd>Control</kbd>+<kbd>Q</kbd>

### Stop detached container

```bash
docker stop pra_server
```

### Stop attached container

<kbd>Control</kbd>+<kbd>C</kbd> and/or <kbd>Control</kbd>+<kbd>D</kbd>
