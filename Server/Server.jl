module Server

using Genie, Genie.Requests, Genie.Renderer, Genie.Renderer.Json
#using Arrow
using DataFrames
using Tables
using JSONTables
using JSON3
using YAML

using OrderedCollections
using Dates

using SwagUI
using SwaggerMarkdown

const Api = include(joinpath("Api", "Api.jl"))
using .Api

end # module

