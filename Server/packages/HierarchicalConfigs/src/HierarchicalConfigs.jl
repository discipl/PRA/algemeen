module HierarchicalConfigs

export load_hierachicaly,
       load_hierachicaly_with_sources,
       calculate_fields_distribution,
       distribute_fields_over_hierarchies,
       dataframe2pairvecs,
       dataframe2dictvec,
       pairvecs2dataframe,
       dictvec2dataframe

using DataFrames
using OrderedCollections

"""
Convert Integer to a BitVector in little endian bitorder.

# Examples
```jldoctest
julia> int2bitvec(3,4)
4-element BitVector:
 1
 1
 0
 0
```
"""
function int2bitvec(u::Integer, size::Integer)
    @assert u < 2 ^ size
    res = BitVector(undef, size)
    res.chunks .= u%UInt64
    res
end

"""
Construct all possible bitvectors of given size.

# Examples
```jldoctest
julia> allbitvecs(3)
8-element Vector{BitVector}:
 [0, 0, 0]
 [1, 0, 0]
 [0, 1, 0]
 [1, 1, 0]
 [0, 0, 1]
 [1, 0, 1]
 [0, 1, 1]
 [1, 1, 1]
```
"""
allbitvecs(size::Integer) = int2bitvec.(0:2^size-1, size)

"""
Construct all possible hierachical splited direcories which could define
defaults for the given path, in the order in which new definitions should
overwrite/extend the previous definition.

# Examples
```jldoctest
julia> ordered_hierachical_defaults(["A", "BB", "CCC"])
8-element Vector{Vector{String}}:
 []
 ["A"]
 ["BB"]
 ["A", "BB"]
 ["CCC"]
 ["A", "CCC"]
 ["BB", "CCC"]
 ["A", "BB", "CCC"]
```
"""
ordered_hierachical_defaults(pathvec::AbstractVector) = getindex.(Ref(pathvec), allbitvecs(length(pathvec)))

"""
Construct all possible hierachical paths which could define defaults for the
given path, in the order in which new definitions should overwrite/extend the
previous definition.

# Examples
```jldoctest
julia> ordered_hierachical_defaults("A/BB/CCC/DDDD")
16-element Vector{String}:
 ""
 "A"
 "BB"
 "A/BB"
 "CCC"
 "A/CCC"
 "BB/CCC"
 "A/BB/CCC"
 "DDDD"
 "A/DDDD"
 "BB/DDDD"
 "A/BB/DDDD"
 "CCC/DDDD"
 "A/CCC/DDDD"
 "BB/CCC/DDDD"
 "A/BB/CCC/DDDD"
```
"""
ordered_hierachical_defaults(path::AbstractString) = joinpath.(vcat.("", ordered_hierachical_defaults(splitpath(path))))

"""
Construct all possible hierachical paths starting after given `basepart` which
could define defaults for the given path, in the order in which new definitions
should overwrite/extend the previous definition.

# Examples
```jldoctest
julia> ordered_hierachical_defaults("xx/yy/zz/A/BB/CCC", "xx/yy/zz")
8-element Vector{String}:
 "xx/yy/zz"
 "xx/yy/zz/A"
 "xx/yy/zz/BB"
 "xx/yy/zz/A/BB"
 "xx/yy/zz/CCC"
 "xx/yy/zz/A/CCC"
 "xx/yy/zz/BB/CCC"
 "xx/yy/zz/A/BB/CCC"
"""
function ordered_hierachical_defaults(path::AbstractString, basepart::AbstractString)
    @assert startswith(path, basepart)
    pathvec = splitpath(path)
    basevec = splitpath(basepart)
    varying_path_part = pathvec[length(basevec)+1:end]
    joinpath.(vcat.(Ref(basevec), ordered_hierachical_defaults(varying_path_part)))
end

"""
Load all given files while using definitions in parent files as defaults,
following a hierachy of the little endian order.
"""
function load_hierachicaly(loader::Function, paths::AbstractVector{<:AbstractString}, ;
                           basepart::AbstractString="")
    df = DataFrame(path=paths, dirname=dirname.(paths), defs=loader.(paths))
    dir2defs = begin
        defaults_df = unique(df, :dirname, keep=:noduplicates)
        Dict(defaults_df.dirname .=> defaults_df.defs)
    end
    ohd_paths = filter!.(in(keys(dir2defs)), ordered_hierachical_defaults.(df.dirname, basepart))
    default_defs = Broadcast.BroadcastFunction(getindex).(Ref(Ref(dir2defs)), ohd_paths)
    (x->merge(x...)).(vcat.(default_defs, df.defs))
end

"""
Load all given files while using definitions in parent files as defaults,
following a hierachy of the little endian order. Also retuns where each
field definition is defined.
"""
function load_hierachicaly_with_sources(loader::Function, paths::AbstractVector{<:AbstractString}, ;
                                        basepart::AbstractString="")
    defs = loader.(paths)
    splitted_full_dirs = splitpath.(dirname.(paths))
    dirs_splitted = getindex.(splitted_full_dirs, range.(length(splitpath(basepart))+1, length.(splitted_full_dirs)))
    load_hierachicaly_with_sources(defs, dirs_splitted)
end

"""
Give addtions to the given definitions according to the given paths,
following a hierachy of the little endian order. Also retuns in which definition each
field definition was originalliy defined.
"""
function load_hierachicaly_with_sources(defs::AbstractVector{<:AbstractDict},
                                        dirs_splitted::AbstractVector{<:AbstractVector{<:AbstractString}})
    df = DataFrame(dirs_splitted=dirs_splitted, defs=defs)
    dir2defs = begin
        # only use files which are the only one present in a directory as default defenitions
        defaults_df = unique(df, :dirs_splitted, keep=:noduplicates)
        Dict(defaults_df.dirs_splitted .=> defaults_df.defs)
    end
    # memory efficient vec of bitvecs which uses a unique bitvecs for each
    # bitvec length and references to te one thats equal to the path length
    bitvecs = begin
        dirlens = length.(df.dirs_splitted)
        dirlen_range = minimum(dirlens):maximum(dirlens)
        dirlen2bitvecs = Dict(dirlen_range .=> allbitvecs.(dirlen_range))
        getindex.(Ref(dirlen2bitvecs), dirlens)
    end
    # generate all possible default paths which could exist for the given paths
    # ohd means other hierarichal defaults
    # path means list of strings of directory (an absolute path representation within the context of models)
    # bitvec means a list of bools representing the relative path
    all_ohd_paths = Broadcast.BroadcastFunction(getindex).(Ref.(df.dirs_splitted), bitvecs)
    # get only the default paths which are present in the given paths
    # same as: ohd_paths_bitvecs = filter!.(in(keys(dir2defs)) ∘ first, ((x, y) -> x .=> y).(all_ohd_paths, bitvecs))
    ohd_paths_bitvecs = filter!.(in(keys(dir2defs)) ∘ first, Broadcast.BroadcastFunction(=>).(all_ohd_paths, bitvecs))
    # split into only paths (to be able to get the contents of the path) and
    # source bitvecs (to be able to make a relative source reference)
    # (another solution would be to put the file path in each of the fields
    # that the file defines, and than later determine the relative source
    # reference. But this would reqruie much more memory since a string is
    # needed for each field defenintion)
    ohd_paths = Broadcast.BroadcastFunction(first).(ohd_paths_bitvecs)
    ohd_bitvecs  = Broadcast.BroadcastFunction(last).(ohd_paths_bitvecs)
    # for each file get all other relevant field definitions
    default_defs = Broadcast.BroadcastFunction(getindex).(Ref(Ref(dir2defs)), ohd_paths)
    # supplement/overwirte field definition for each file
    defs = (x->merge(x...)).(vcat.(default_defs,
                                   # at last, overwrite all defaults with the definitions in the actual file
                                   df.defs))
    # make relative source references for each field in each relevant file for each file
    bitvecs_defs = ((x,y) -> Dict.(Broadcast.BroadcastFunction(=>).(keys.(x), Ref.(y)))).(default_defs, ohd_bitvecs)
    # supplement/overwite the used file for the field definition for each field in in the resulting definitions
    def_sources = (x->merge(x...)).(vcat.(bitvecs_defs,
                                          # use 'all true' bitvecs for all fields which are in the actual file
                                          Dict.(Broadcast.BroadcastFunction(=>).(keys.(df.defs), Ref.(last.(bitvecs))))))
    (defs, def_sources)
end

"""
Calculates the highest linear hierarichal level for each of the fields where it
can be defined. The hierarchy is dertermend from the filenames. The keyword
argument `last_level_fields` can be used to force certain fields to be defined
in the lowest hierachical level.
"""
function calculate_fields_distribution(filenames::AbstractVector{<:AbstractString}, content::AbstractDataFrame, ;
                                       last_level_fields::AbstractVector{<:AbstractString}=String[],
                                       basepart::AbstractString="")
    levels = begin
        raw_levels = split.(filenames, "/")
        getindex.(raw_levels, range.(length(splitpath(basepart))+1, length.(raw_levels).-1))
    end
    max_hierarchy = maximum(length.(levels))
    # determine the unique fields for each level
    level_dfs = []
    for level in 1:max_hierarchy
        # when a level is shorter than the current level length we will use the
        # previous level becouse we need to groupby something altough all
        # fields levels in these groups will always be set to missing afterwards.
        level_levels = getindex.(levels, range.(1, min.(level, length.(levels))))
        df = transform(groupby(insertcols(content, :__GroupCol => level_levels), :__GroupCol),
            # if always the same value is defined for a field for certain level, then
            # the field might as well be defined on that level, so we mark that
            # field with the correpsonding level. Otherwise we put missing
            # there so that `coalesce` will pick a marked field from a lower
            # level if present.
            names(content) .=> (x-> length(x) == 1 && !ismissing(first(x)) ? level : missing) ∘ unique .=> names(content),
        )
        select!(df, Not(:__GroupCol))
        allowmissing!(df)
        # set levels which are shorter than current level to missing so that these will later be set to all ones vectors
        df[length.(level_levels) .!= level, :] .= missing
        push!(level_dfs, df)
    end
    # pick highest level for each field
    fielddefenition_at_level = coalesce.(level_dfs...)
    # overwrite the given `last_level_fields` to the lowest level (when present)
    fielddefenition_at_level[!, last_level_fields] .= ifelse.(ismissing.(fielddefenition_at_level[!, last_level_fields]), missing, length.(levels))
    # when a level is missing while there is a value defined (happens when
    # mulitple files are in a directory), we will define it also in the last level
    fielddefenition_at_level = coalesce.(fielddefenition_at_level, ifelse.(ismissing.(content), missing, length.(levels)))
    # transform levels to bitvecs
    fielddefenition_at_level = passmissing(int2bitvec).((2 .^ fielddefenition_at_level .- 1), length.(levels))
    fielddefenition_at_level
end

"""
Distributes the field definitions over the fieldnames according to the given
locations of each field definition, resulting in a dictionary which maps each
of the given filenames to a dictionary containing fields to be defined in the
file.
"""
function distribute_fields_over_hierarchies(filename::AbstractString, defs::AbstractDict, locs::AbstractDict, ;
                                            basepart::AbstractString="",
                                            defaults_filename="defaults.yaml",
                                            default_fields=["Compleet"=>false])
    splitted_dir = splitpath(dirname(filename))[length(splitpath(basepart))+1:end]
    files2fields = Dict{String, OrderedDict}()
    for fieldname in keys(defs)
        bitvec = locs[fieldname]
        ismissing(bitvec) && throw("Handling missing locations is not implemented (TODO? We could just skip those keys and not distribute the values (which actualy also should be missing) VS being informed when this happens)")
        if all(bitvec)
            dir = filename
        else
            if isnothing(defaults_filename)
                dir = joinpath(basepart, splitted_dir[bitvec]...)
            else
                dir = joinpath(basepart, splitted_dir[bitvec]..., defaults_filename)
            end
        end
        if dir ∉ keys(files2fields)
            files2fields[dir] = OrderedDict{String, Any}(default_fields)
        end
        files2fields[dir][fieldname] = defs[fieldname]
    end
    files2fields
end

dictvec2dataframe(dictvec) = vcat(DataFrame.(Ref.(dictvec))..., cols=:union)
#pairvecs2dataframe(v) = vcat(DataFrame.(Ref.(NamedTuple.(v)))..., cols=:union)
pairvecs2dataframe(v) = vcat(DataFrame.(Ref.(v))..., cols=:union)

#TODO check if Tables.namedtupleiterator is a better option
function dataframe2pairvecs(df)
    pairvecs = collect.(pairs.(eachrow(df)))
    filter!.(((_,v),)->!ismissing(v), pairvecs)
    pairvecs
end
dataframe2dictvec(T::Type, df) = T.(map.((((k,v),)->String(k) => v), dataframe2pairvecs(df)))
dataframe2dictvec(df) = dataframe2dictvec(OrderedDict, df)

end # module HierarchicalConfigs
