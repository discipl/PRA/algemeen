module CheckNotifications

using Genie, Genie.Requests, Genie.Renderer.Json
using SwaggerMarkdown

import ..Main.DataModel as DM
import ..Main.ConditionParser as CP

response = """
    responses:
      '200':
        description: OK
        content:
          application/json:
            schema:
              type: object
              properties:
                notifications:
                  type: array
                  description: Relevent notifications for the given context variables
                  items:
                    type: object
                    properties:
                      Item.Id:
                        type: integer
                      Item.Taal:
                        type: string
                      Notificatie.Id:
                        type: integer
                      Notificatie.Tekst:
                        type: string
"""

# Docs in Open API v3.0 format
@swagger """
/check-notifications:
  post:
    summary: Get all relevant notifications for the values of the context variables.
    requestBody:
        description: Values of the context variables
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                context:
                  type: object
                  properties:
                    Boolean:
                      description: The values of all the boolean context variables.
                      type: object
                      default: {}
                      example: {"is MKB": true}
                    String:
                      description: The values of all the string context variables.
                      type: object
                      default: {}
                      example: {"hoofd SBI-code": "010203"}
                    StringCollection:
                      description: The values of all the string collection context variables.
                      type: object
                      default: {}
                      example: {"SBI-codes": ["010203", "010204"]}
$response
"""

route("/check-notifications", method = POST) do
    payload_dict = jsonpayload()
    if isnothing(payload_dict)
        payload_dict = Dict{String, Any}()
    end
    context_variables = convert(Dict{Symbol, Dict}, copy(get(payload_dict, "context", Dict{Symbol, Bool}())))
    return notifications_for(context_variables)
end

function notifications_for(context_variables)
    computed_variables = Dict()
    relevant_notifications = DM.notifications[coalesce.(
        CP.evaluate.(DM.notifications, Ref(context_variables), Ref(computed_variables)),
        false
    )]
    return json((;
        results = [(; var"Item.Id" = x.item_id,
                      var"Item.Taal" = x.language,
                      var"Notificatie.Id" = x.notification_id,
                      var"Notificatie.Tekst" = x.text )
                   for x in relevant_notifications]
    ))
end

end # module
