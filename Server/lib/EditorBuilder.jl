module EditorBuilder

import ..Main.EditorDataInterface as EDI

const inits = Dict{String, Function}()
const calls = Dict{Symbol, Function}()
const js_methods = Dict{Symbol, String}()

function register_init(fn::Function, name::String)
    #TODO make it possible to push by calling build_editable_fields only once and serving the saved result
    #push!(inits, fn)
    inits[name] = fn
end

function call_inits(model)
    for init_fn in values(inits)
        init_fn(model)
    end
end

function register_call(call_fn::Function, fn_name::Symbol)
    calls[fn_name] = call_fn
    nothing
end

function register_call(call_fn::Function, fn_name::String)
    calls[Symbol(fn_name)] = call_fn
    nothing
end

function call(fn_name, model, arglist)
    if fn_name in keys(calls)
        calls[fn_name](model, arglist...)
    else
        println("call $fn_name is not registerd. Got args: $arglist") 
    end
end

function register_js_method(method_name::Symbol, method_sting::AbstractString)
    js_methods[method_name] = method_sting
end

function register_js_method(method::Pair{Symbol, <:AbstractString})
    register_js_method(method...)
end

function render_js_methods()
    return join("$method_name: $method_sting" for (method_name, method_sting) in pairs(js_methods)
     ",\n")
end

shared_tree = nothing

function register_shared_tree(x::EDI.SharedTree)
    global shared_tree
    shared_tree = x
end

end # module
