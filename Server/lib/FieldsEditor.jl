module FieldsEditor

using GenieFramework

import ..Main.SchemeModel as SM
import ..Main.DataModel as DM

import ..Main.EditorDataInterface as EDI
import ..Main.EditorBuilder as EB
import ..Main.FlintParser as FP

function fieldseditor()
    join([
        build_editable_fields(SM.item_scheme, (JsVarKey("selectedModel"),)),
        Html.div("", @iif("!selectedModel['Type']"), [
            h5([span(SM.basis_scheme.name), hint(SM.basis_scheme)]),
            Html.div("", class="q-gutter-y-md column", [ build_editable_fields(SM.basis_scheme, (JsVarKey("selectedModel"),)) ]),
            separator()
        ])
    ])
end

abstract type JsKey end

struct JsFixedKey{T<:Union{Int, String}} <: JsKey
    key::T
end
JsFixedKey(x::Symbol) = JsFixedKey(string(x))
struct JsVarKey{T<:Union{Int, String}} <: JsKey
    key::T
end
JsVarKey(x::Symbol) = JsVarKey(string(x))

jsrepr(x::JsFixedKey) = "'$(x.key)'"
jsrepr(x::JsVarKey) = "$(x.key)"

jsrepr(reactive_datafield::Tuple{Vararg{<:JsKey}}) = jsrepr(reactive_datafield[1])*join("[$(jsrepr(x))]" for x in reactive_datafield[2:end])

jsplain(x::Vector) = "["*join(x,",")*"]"
jsplain(x::Tuple) = "["*join(x,",")*"]"

function build_editable_fields(x::SM.OneOfScheme, reactive_datafield::Tuple{Vararg{<:JsKey}})
    html_list = String[]
    for (discriminator_value, scheme) in pairs(x.possible_objects)
        push!(html_list,
            Html.div("",
                     #class="q-gutter-y-md column",
                     @iif("$(jsrepr((reactive_datafield..., JsFixedKey(x.discriminator)))) == '$discriminator_value'"), [
                build_editable_fields(scheme, reactive_datafield)
            ])
        )
    end
    return join(html_list)
end

function build_editable_fields(x::SM.AllOfScheme, reactive_datafield::Tuple{Vararg{<:JsKey}})
    html_list = String[]
    for v in x.merge_objects
        if !isempty(v.name)
            push!(html_list,
                h5([span(v.name), hint(v.description)])
            )
        end
        push!(html_list,
            #:expand__separator,
            Html.div("", class="q-gutter-y-md column", [ build_editable_fields(v.scheme, reactive_datafield) ]),
            separator(style="margin-top: 16px;")
        )
    end
    return join(html_list)
end

function build_editable_fields(x::SM.ObjectScheme, reactive_datafield::Tuple{Vararg{<:JsKey}})
    html_list = String[]
    for (property_name, value_scheme) in pairs(x.properties)
        occursin(".Dev.", string(property_name)) && continue
        push!(html_list,
            editablefield((reactive_datafield..., JsFixedKey(string(property_name))), value_scheme)
        )
    end
    return join(html_list)
end

function build_editable_fields(x::SM.ValueScheme, reactive_datafield::Tuple{Vararg{<:JsKey}})
    build_editable_fields(x.scheme, reactive_datafield)
end

function build_editable_fields(x::SM.ArrayScheme, reactive_datafield::Tuple{Vararg{<:JsKey}})
    build_editable_fields(x.items, reactive_datafield)
end

function hint(x::SM.Description)
    return btn(
        "",
        #icon="lightbulb",
        #icon="help",
        icon="question_mark",
        :outline,
        :round,
        :dense,
        size="xs", color="blue", [
            tooltip(x.text)
    ])
end
function hint(::SM.NoDescription)
    return btn(
        "",
        #icon="lightbulb",
        #icon="help",
        icon="question_mark",
        :outline,
        :round,
        :dense,
        :disable,
        size="xs",
        color="blue-3", [
            tooltip("No description")
    ])
end
function hint(str::String)
        return btn(
            "",
            #icon="lightbulb",
            #icon="help",
            icon="question_mark",
            :outline,
            :round,
            :dense,
            :disable,
            size="xs",
            color="blue-3", [
                tooltip(str)
        ])
end
function hint(v::SM.ValueScheme)
    hint(v.description)
end

const TEXTFIELD_KWARGS = Dict(
    :dense=>"",
    :stack__label=>"",
    :square=>"",
    :filled=>"",
    :bg__color=>"white",
    #:outlined=>"",
    #:standout=>"",
    #:label=>name,
)

function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme)
    editablefield(reactive_datafield, v, v.scheme, v.format)
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.StringScheme, ::SM.MaintainersFormat)
    textfield("", Symbol(jsrepr(reactive_datafield)), label=v.name, [template("", "v-slot:after", [hint(v)])], ; TEXTFIELD_KWARGS...)
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.StringScheme, ::SM.SingleLineTextFormat)
    textfield("", Symbol(jsrepr(reactive_datafield)), label=v.name, [template("", "v-slot:after", [hint(v)])], ; TEXTFIELD_KWARGS...)
end
# TODO make more general for other URL's or change Format name to make this specific for FLint URL's
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.StringScheme, ::SM.UrlFormat)
    get_flint_facts_call = "get_flint_facts___$(replace(jsrepr(reactive_datafield),r"\W"=>"_"))"
    #EB.register_init("flintFactsCategorized") do model
        #model.localdata[]["flintFactsCategorized"] = FP.categorize_used_facts(get(model.selectedModel[], "Regeling.FlintFacts", missing))
        #notify(model.localdata)
    #end
    EB.register_call(get_flint_facts_call) do model, isTrusted
        flint_frames = FP.flintdict2framestable(FP.load_json_url(model.selectedModel[][reactive_datafield[2].key]))

        model.selectedModel[]["Flint.Frames"] = flint_frames
        #model.localdata[]["flintFactsCategorized"] = FP.categorize_used_facts(used_facts)
        model.leftTab[] = "flint_facts"
        notify(model.selectedModel)
        notify(model.localdata)
    end
    join([
        textfield("",
            Symbol(jsrepr(reactive_datafield)),
            label=v.name,
            var"@blur"="function(value){call=['$get_flint_facts_call', value]}",
            [template("", "v-slot:after", [hint(v)])],
            ; TEXTFIELD_KWARGS...)
        Html.p("", [Html.a("{{$(jsrepr(reactive_datafield))}}", var":href"=jsrepr(reactive_datafield), target="_blank")])
    ])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.ArrayScheme, ::SM.TableFormat)
    EB.register_init("flintFactSelected") do model
        model.localdata[]["flintFactSelected"] = []
    end
#    scrollarea("", style="width:100%; height:400px;", [
#        template("", "v-slot:default", [
#            q__table("",
#                title=v.name,
#                var":data"=jsrepr(reactive_datafield),
#                var":columns"="[
#                    {name: 'facts_used_for_a_condition', label: 'Verwerkt', field: row => (localdata.facts_used_for_a_condition || []).includes(row.function) ?  '&#9989;' : '' },
#                    {name: 'function', label: 'Fact', field: 'function', align:'left'},
#                    {name: 'contexttype', label: 'Type', field: 'contexttype'},
#                    {name: 'is_used_in', label: 'In Flint gebruikt bij', field: row => row.is_used_in.join(', ')},
#                    ]",
#                var"row-key"="function",
#                selection="single",
#                var":selected.sync"="localdata['flintFactSelected']",
#                [
#            ])
#        ])
#    ])
    return ""
#'&#9745;'
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.StringScheme, ::SM.SlugFormat)
    textfield("", Symbol(jsrepr(reactive_datafield)), label=v.name, [template("", "v-slot:after", [hint(v)])], type="url", ; TEXTFIELD_KWARGS...)
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.StringScheme, ::SM.IconFormat)
    textfield("", Symbol(jsrepr(reactive_datafield)), label=v.name, [template("", "v-slot:after", [hint(v)])], ; TEXTFIELD_KWARGS...)
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.StringScheme, ::SM.GrondslagFormat)
    textfield("", Symbol(jsrepr(reactive_datafield)), label=v.name, [template("", "v-slot:after", [hint(v)])], type="url", ; TEXTFIELD_KWARGS...)
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.StringScheme, ::SM.MultiLineTextFormat)
    textfield("", Symbol(jsrepr(reactive_datafield)), label=v.name, [template("", "v-slot:after", [hint(v)])],
        :autogrow,
        #type="textarea",
        ; TEXTFIELD_KWARGS...)
end

EB.register_js_method(:textEditMark => """function(refname){
  const name = this.localdata.flintFactSelected[0].mark
  const edit = this.\$refs[refname][0]
  edit.caret.restore()
  let parent_node = edit.caret.parent
  let found_same_mark = false
  while (!parent_node.classList.contains("q-editor__content")){
    if (parent_node.classList.contains(name)){
      found_same_mark = true
      break
    }
    parent_node = parent_node.parentNode
  }
  if (found_same_mark){
    // remove the mark when its found
    // TODO fix: when the mark is the only child of another element that tag is removed
    edit.caret.range.selectNodeContents(parent_node)
    const parent_innerHTML = parent_node.innerHTML
    const extracted = edit.caret.range.extractContents()
    console.log(extracted)
    //edit.runCmd('insertHTML', extracted)
    edit.runCmd('insertHTML', parent_innerHTML)
    edit.focus()
  } else {
    // create the mark when its not found
    // TODO fix: when the selection is precisely one element then the element is removed
    // TODO fix: when selection is at the start of another element the start of that tag is moved to the end of the selection/mark
    mark = document.createElement('mark')
    mark.setAttribute("class", name)
    selected_contents = edit.caret.range.cloneContents()
    const looptimes =selected_contents.childNodes.length
    for (let i=0; i<looptimes; i++){
      const cn = selected_contents.childNodes[0]
      mark.appendChild(cn)
    }
    edit.runCmd('insertHTML', mark.outerHTML)
    edit.focus()
  }
}""")
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.StringScheme, ::SM.HtmlTextFormat)
    this_editor_ref = "html_text_editor___$(replace(jsrepr(reactive_datafield),r"\W"=>"_"))"
    join([label([
        span(v.name), hint(v)]),
        q__editor("", @bind(jsrepr(reactive_datafield)), paragraph__tag="p",
            class! = "localdata.flintFactSelected.length == 0 ? '' : localdata.flintFactSelected[0].mark",
            toolbar! = """[
              ['viewsource'],
              [{
                //label: \$q.lang.editor.formatting,
                fixedLabel: true,
                icon: \$q.iconSet.editor.formatting,
                //list: 'no-icons',
                options: [ 'p', 'h2', 'h3', 'h4', 'h5', 'h6' ]
              }],
              ['bold', 'italic', 'removeFormat'],
              ['token', 'link', 'custom_btn'],
              ['mark'],
              [ //'quote',
                'unordered', 'ordered'],
              ['undo', 'redo'],
              ['fullscreen'],
            ]""",
            definitions! = """{
              mark: {
                //label: 'Mark',
                icon: 'gavel',
                tip: 'Mark the selected text as explenation of currently selected Flint Fact',
                handler: () => textEditMark($(jsplain(jsrepr.(reactive_datafield)[2:end])).join('_')),
                disable: () => localdata['flintFactSelected'].length == 0,
              }
            }""",
            :square,
            ref! = "$(jsplain(jsrepr.(reactive_datafield)[2:end])).join('_')",
            @iif("$(jsrepr(reactive_datafield)) !== undefined"),[
              template("", "v-slot:mark", [
                btn("Mark", icon="gavel")
              ])
            ]),
        btn("Add rich text", @click("$(jsrepr(reactive_datafield)) = ''; selectedModel = selectedModel; updater+=1"), "v-else", :unelevated),
    ])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.StringScheme, ::SM.ItemTypeFormat)
    init_required_fields_call = "init_required_fields"
    # Back-end
    EB.register_call(init_required_fields_call) do model, reactive_datafield, new_type
        # TODO: rewrite this to use reactive_datafield to make it work for other discriminator fields
        minimal_model = EDI.minimal_model(EB.shared_tree, model.tree_selected[], new_type)
        filter!(minimal_model) do p
            !(first(p) in keys(model.selectedModel[]))
        end
        merge!(model.selectedModel[], minimal_model)
        model.selectedModel[]["Type"] = new_type
        notify(model.selectedModel)
    end
    # Front-end
    join([q__select("",
        var":value"=jsrepr(reactive_datafield),
        var"@input"="function(value){call=['$init_required_fields_call', $(js_attr(getfield.(reactive_datafield, :key))), value]}",
        label=v.name,
        [template("", "v-slot:after", [hint(v)])],
        options! = js_attr(SM.item_types),
        ; TEXTFIELD_KWARGS...
    )])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.StringScheme, ::SM.ContentTypeFormat)
    join([]) # This field is already build with format: content
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.StringScheme, ::SM.ContentBlockTypeFormat)
    join([])# This field is already build with format: content-blocks
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.StringScheme, ::SM.LanguageFormat)
    join([q__select("", @bind(jsrepr(reactive_datafield)), label=v.name, [template("", "v-slot:after", [hint(v)])],
                    options! = js_attr(SM.languages),
                    # TODO make filterable dorpdown selection working
                    #options! = "options['$name']",
                    #@on("filter","()=>filterFn('$name', $(js_attr(SM.languages)))"),
                    :use__input, :hide__selected, :fill__input, input__debounce="0",
                    ; TEXTFIELD_KWARGS...
                    )])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.StringScheme, ::SM.DateTimeFormat)
    join([textfield("", Symbol(jsrepr(reactive_datafield)), label=v.name, [template("", "v-slot:after", [hint(v)])], :readonly, ; TEXTFIELD_KWARGS...)])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.StringScheme, ::SM.NoFormat)
    join([textfield("", Symbol(jsrepr(reactive_datafield)), label=v.name, [template("", "v-slot:after", [hint(v)])], ; TEXTFIELD_KWARGS...)])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.IntegerScheme, ::SM.TypeLinkFormat)
    join([textfield("", Symbol(jsrepr(reactive_datafield)), label=v.name, [template("", "v-slot:after", [hint(v)])], :readonly, ; TEXTFIELD_KWARGS...)])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.IntegerScheme, ::SM.FixedFormat)
    join([textfield("", Symbol(jsrepr(reactive_datafield)), label=v.name, [template("", "v-slot:after", [hint(v)])], :readonly, ; TEXTFIELD_KWARGS...)])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.StringScheme, ::SM.FixedFormat)
    join([textfield("", Symbol(jsrepr(reactive_datafield)), label=v.name, [template("", "v-slot:after", [hint(v)])], :readonly, ; TEXTFIELD_KWARGS...)])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.BooleanScheme, ::SM.ComputedFormat)
    join([toggle("", Symbol(jsrepr(reactive_datafield)), [span(v.name), hint(v)], ; TEXTFIELD_KWARGS...)])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.StringScheme, ::SM.VariantFormat)
    join([textfield("", Symbol(jsrepr(reactive_datafield)), label=v.name, [template("", "v-slot:after", [hint(v)])], :readonly, ; TEXTFIELD_KWARGS...)])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.StringScheme, ::SM.ModelFormat)
    join([textfield("", Symbol(jsrepr(reactive_datafield)), label=v.name, [template("", "v-slot:after", [hint(v)])], :readonly, ; TEXTFIELD_KWARGS...)])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.ArrayScheme, ::SM.ChipsFormat)
    join([q__select("", @bind(jsrepr(reactive_datafield)), label=v.name, :use__input, :use__chips, :multiple, :hide__dropdown__icon, input__debounce="0", new__value__mode="add-unique", [template("", "v-slot:after", [hint(v)])], ; TEXTFIELD_KWARGS...)])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.ArrayScheme, ::SM.ContentFormat)
    add_content_container_call = "add_content_container___$(replace(jsrepr(reactive_datafield),r"\W"=>"_"))"
    remove_content_container_call = "remove_content_container___$(replace(jsrepr(reactive_datafield),r"\W"=>"_"))"
    # Back-end
    EB.register_call(add_content_container_call) do model, content_fieldname
        if content_fieldname ∉ keys(model.selectedModel[])
            model.selectedModel[][content_fieldname] = OrderedDict[]
        end
        push!(model.selectedModel[][content_fieldname], SM.new_contentcontainer(maximum(vcat([x["ContentId"] for x in model.selectedModel[][content_fieldname]], [0]))+1))
        notify(model.selectedModel)
    end
    EB.register_call(remove_content_container_call) do model, content_fieldname, content_container_jsindex
        deleteat!(model.selectedModel[][content_fieldname], content_container_jsindex+1)
        notify(model.selectedModel)
    end
    # Front-end
    join([
        Html.div("", key! = "updater", class="q-pa-md row items-start q-gutter-md", [
            label([span(v.name), hint(v)])
            card("", @recur("(content_item, content_item_i) in $(jsrepr(reactive_datafield))"), var"v-bind:key"="content_item.ContentId", style="width: 100%;", [
                cardsection("", class="q-pa-md row", [
                    Html.div("", class="col", [
                        btn("",
                            icon="delete_forever",
                            #icon="delete_sweep",
                            :square, :dense, size="md", padding="none", title="Delete this content container", :unelevated,
                            @click("call=['$remove_content_container_call', '$(last(reactive_datafield).key)', content_item_i]"),
                            )
                    ])
                    span("Content container {{ content_item['ContentId'] }}", class="text-subtitle2 col-5")
                    q__select("", @bind("content_item['ContentType']"), class="col-6", label="ContentType", [template("", "v-slot:after", [hint("Type contentcontainer")])], options! = js_attr(SM.content_types), :dense)
                ])
                separator("", :inset)
                cardsection("", [
                    #build_editable_fields(v, (JsVarKey("content_item"),))
                    build_editable_fields(v, (reactive_datafield..., JsVarKey("content_item_i")))
                    #build_editable_fields(v, "$(jsrepr((reactive_datafield..., i)))")
                ])
            ])
            card("", [
              btn("", :square, icon="post_add",
                  @click("call=['$add_content_container_call', '$(last(reactive_datafield).key)']"),
                  [tooltip("Add content container")])
            ])
        ])
    ])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.ArrayScheme, ::SM.ContentBlocksFormat)
    remove_content_block_call = "remove_content_block___$(replace(jsrepr(reactive_datafield),r"\W"=>"_"))"
    add_content_block_call= "add_content_block___$(replace(jsrepr(reactive_datafield),r"\W"=>"_"))"
    prepend_new_content_block_call = "prepend_new_content_block___$(replace(jsrepr(reactive_datafield),r"\W"=>"_"))"
    move_content_block_call= "move_content_block___$(replace(jsrepr(reactive_datafield),r"\W"=>"_"))"
    # Back-end
    # remove_content_block method
    # TODO now, after refactoring, it its possible to get the
    # content_fieldname fron the reactive_datafield, and register multiple
    # versions of this call for each different reactive_datafield (which now
    # already happens) (maybe this is really easy with get_shallow_array_field?)
    EB.register_call(remove_content_block_call) do model, content_container_jsindex, contentblock_jsindex
        # TODO this is a bit hacky, what if the patern of the name of the content field changes??
        content_fieldname = "$(model.selectedModel[]["Type"]).Content"
        deleteat!(model.selectedModel[][content_fieldname][content_container_jsindex+1]["ContentBlocks"], contentblock_jsindex+1)
        notify(model.selectedModel)
    end
    # add_content_block method
    EB.register_call(add_content_block_call) do model, content_container_jsindex
            # TODO find way to put content_fieldname in this call
            #content_fieldname, content_container_jsindex, = call[2:end]
        # TODO this is a bit hacky, what if the patern of the name of the content field changes??
        content_fieldname = "$(model.selectedModel[]["Type"]).Content"
# This is commented because not needed anymore, an empty CotentBlocks is always made with SchemeModel.new_contentcontainer()
# Although, it would be more save to always this first TODO: remove this or make it more save (This probably can also be solved with get_shallow_array_field)
#            if "ContentBlocks" ∉ keys(selectedModel[content_fieldname][content_container_jsindex+1])
#                selectedModel[content_fieldname][content_container_jsindex+1]["ContentBlocks"] = OrderedDict[]
#            end
        push!(model.selectedModel[][content_fieldname][content_container_jsindex+1]["ContentBlocks"], SM.new_contentblock())
        notify(model.selectedModel)
    end
    # prepend_new_content_block method
    EB.register_call(prepend_new_content_block_call) do model, content_container_jsindex, contentblock_jsindex
        # TODO this is a bit hacky, what if the patern of the name of the content field changes??
        content_fieldname = "$(model.selectedModel[]["Type"]).Content"
        insert!(model.selectedModel[][content_fieldname][content_container_jsindex+1]["ContentBlocks"], contentblock_jsindex+1, SM.new_contentblock())
        notify(model.selectedModel)
    end
    # move_content_block method
    EB.register_call(move_content_block_call) do model, from_content_container_jsindex, from_contentblock_jsindex, to_content_container_jsindex, to_contentblock_jsindex
        # TODO this is a bit hacky, what if the patern of the name of the content field changes??
        content_fieldname = "$(model.selectedModel[]["Type"]).Content"
        item_to_move = popat!(model.selectedModel[][content_fieldname][from_content_container_jsindex+1]["ContentBlocks"], from_contentblock_jsindex+1)
        #println("moving: $(item_to_move["Body"])")
        #println("move $from_content_container_jsindex, $from_contentblock_jsindex to $to_content_container_jsindex, $to_contentblock_jsindex")
        if to_contentblock_jsindex+1 < 1
            #println("A")
            if to_content_container_jsindex+1 <= 1 # (==) will probably be sufficient
                #println("ContentBlock is moved before everything that exists")
                ncc = SM.new_contentcontainer(maximum(vcat([x["ContentId"] for x in model.selectedModel[][content_fieldname]], [0]))+1)
                pushfirst!(model.selectedModel[][content_fieldname], ncc)
                to_content_container_jsindex = 0 # this should already be 0 be we set it to 0 just in case its something lower
                to_contentblock_jsindex = 0
            else
                #println("ContentBlock is moved to the last item of the container above")
                to_content_container_jsindex -= 1
                to_contentblock_jsindex = length(model.selectedModel[][content_fieldname][to_content_container_jsindex+1]["ContentBlocks"])
            end
        elseif to_contentblock_jsindex+1 > length(model.selectedModel[][content_fieldname][to_content_container_jsindex+1]["ContentBlocks"])+(from_content_container_jsindex == to_content_container_jsindex) # +1 when already popped in same container
            #println("B $(to_contentblock_jsindex+1) > $(length(selectedModel[content_fieldname][to_content_container_jsindex+1]["ContentBlocks"]))")
            if to_content_container_jsindex+1 >= length(model.selectedModel[][content_fieldname]) # (==) will probably be sufficient
                #println("ContentBlock is moved after everything that exists")
                ncc = SM.new_contentcontainer(maximum(vcat([x["ContentId"] for x in model.selectedModel[][content_fieldname]], [0]))+1)
                push!(model.selectedModel[][content_fieldname], ncc)
                to_content_container_jsindex = length(model.selectedModel[][content_fieldname])-1
                to_contentblock_jsindex = 0
            else
                #println("ContentBlock is moved to the first item of the container below")
                to_content_container_jsindex += 1
                to_contentblock_jsindex = 0
            end
        end
        #println("became: $from_content_container_jsindex, $from_contentblock_jsindex to $to_content_container_jsindex, $to_contentblock_jsindex")
        #println("inserting into: $(selectedModel[content_fieldname][to_content_container_jsindex+1])")
        insert!(model.selectedModel[][content_fieldname][to_content_container_jsindex+1]["ContentBlocks"], to_contentblock_jsindex+1, item_to_move)
        notify(model.selectedModel)
    end
    # Front-end
    join([
        Html.div("", @recur("(content_block_item, j) in $(jsrepr(reactive_datafield))"), var"v-bind:key"="content_block_item.ContentBlockId", [
            btn("", style="width:100%;", :dense, :flat, size="sm", padding="none",
                icon="add",
                #icon="add_card",
                #icon="add_box",
                @click("call=['$prepend_new_content_block_call', content_item_i, j]"),
                [tooltip("Add content block")])
            separator("")
            Html.div("", class="row", [
                btn("", :flat, size="sm", icon="remove", class="col",
                    @click("call=['$remove_content_block_call', content_item_i, j]"),
                    [tooltip("Remove content")])
                btn("", :flat, size="sm", icon="arrow_upward", class="col",
                    @click("call=['$move_content_block_call', content_item_i, j, content_item_i, j-1]"),
                    [tooltip("Move content up")])
                btn("", :flat, size="sm", icon="arrow_downward", class="col",
                    @click("call=['$move_content_block_call', content_item_i, j, content_item_i, j+1]"),
                    [tooltip("Move content down")])
                q__select("", @bind("content_block_item['ContentBlockType']"), label="ContentBlockType", class="col-6", [template("", "v-slot:after", [hint("Het soort content")])], options! = js_attr(SM.contentblock_types), ; TEXTFIELD_KWARGS...)
            ])
            #build_editable_fields(v, (JsVarKey("content_block_item"),))
            build_editable_fields(v, (reactive_datafield..., JsVarKey("j"),))
            #separator("", :inset)
        ]),
        btn("", :dense, :flat, size="sm", padding="none",
            icon="add",
            #icon="add_card",
            #icon="add_box",
            @click("call=['$add_content_block_call', content_item_i]"),
            [tooltip("Add content block")])
    ])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.IntegerScheme, ::SM.RelativeContentIdFormat)
    join([])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.IntegerScheme, ::SM.RelativeContentBlockIdFormat)
    join([])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.StringScheme, ::SM.ExpansionGroupFormat)
    join([])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::Any, ::SM.NoFormat)
    join([])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::Any, ::SM.LevensgebeurtenisLinkFormat)
    join([])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.ArrayScheme, ::SM.FiltersFormat)
    join([])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::SM.StringScheme, ::SM.CategoryFormat)
    join([
        q__select("", @bind(jsrepr(reactive_datafield)), label=v.name, [template("", "v-slot:after", [hint(v)])],
                  options! = js_attr(DM.occuring_values_of(EB.shared_tree.models, v.name)), ; TEXTFIELD_KWARGS...)
        ])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::Any, ::SM.ComputedBooleanLookupFormat)
    q__select("",
        var"v-model"=jsrepr((reactive_datafield..., JsFixedKey("variableName"))),
        label=v.name,
        [template("", "v-slot:after", [hint(v)])],
        # TODO dont hard-code 'Regeling.Condities'
        options! = "['[NOT SET]', ...(selectedModel['Regeling.Condities'] == undefined ? [] : selectedModel['Regeling.Condities'].map(x=>x.variableName))]",
        ; TEXTFIELD_KWARGS...
    )
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, s::SM.StringEnumScheme, ::SM.NoFormat)
    q__select("",
        @bind(jsrepr(reactive_datafield)),
        label=v.name,
        [template("", "v-slot:after", [hint(v)])],
        options! = js_attr(s.values),
        ; TEXTFIELD_KWARGS...
    )
end
# TODO make this more genic so that the field does not need to be shallow
function get_shallow_array_field(model, reactive_datafield)
    selectedModel = getfield(model, Symbol(reactive_datafield[1].key))
    arrayfieldname = reactive_datafield[2].key
    if !(arrayfieldname in keys(selectedModel[]))
        selectedModel[][arrayfieldname] = []
    end
    return selectedModel[][arrayfieldname]
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::Any, ::SM.ContextVariableDefinitionListFormat)
    add_contextvariable_call = "add_contextvariable___$(replace(jsrepr(reactive_datafield),r"\W"=>"_"))"
    EB.register_call(add_contextvariable_call) do model, args...
        field = get_shallow_array_field(model, reactive_datafield)
        new_knownvariable = OrderedDict(
            "variableName"=>"ContextGegeven zonder naam",
            "variableType"=>v.scheme.items.scheme.properties.variableType.scheme.values[1],
        )
        push!(field, new_knownvariable)
        notify(model.selectedModel)
        #SM._show(v)
    end
    join([
        label([span(v.name), hint(v)])
        Html.div("", class="q-pa-md row items-start q-gutter-md full-width", [
            Html.div("", class="row full-width", style="display:flex; flex-direction:row;", @recur("(obj, i) in $(jsrepr(reactive_datafield))"), [
                Html.div("", class="", style="flex-grow:1", [
                    editablefield((JsVarKey("obj"), JsFixedKey("variableName")),
                                  v.scheme.items.scheme.properties.variableName)
                ])
                Html.div("", class="", style="flex-grow:0; width: 200px;", [
                    editablefield((JsVarKey("obj"), JsFixedKey("variableType")),
                                  v.scheme.items.scheme.properties.variableType)
                ])
            ])
            card("", [btn("", :square, icon="post_add",
                    @click("call=['$add_contextvariable_call']"),
                    [tooltip("Add context data")])
            ])
        ])
    ])
end
refname2templatename(refname) = replace(lowercase(refname), r"[^a-z0-9]" => "")
function expressionscheme2args(arguments, scheme::SM.AllOfScheme)
    # search for the arguments property which should be in one one the merge_objects
    for (i, object_or_ref) in enumerate(scheme.merge_objects)
        object = resolve_scheme(object_or_ref).scheme
        if :arguments in keys(object.properties)
            return _expressionscheme2args(arguments, object.properties.arguments.scheme)
        end
    end
end
function _expressionscheme2args(arguments, scheme::SM.ObjectScheme)
    # return all arg names and all arg schemes of the object
    # it is assumed that an correct ObjectScheme is passed, such that it contains an :arguments property
    #return string.(keys(scheme.properties)), [resolve_scheme(x).scheme for x in values(scheme.properties)]
    return string.(keys(scheme.properties)), [x for x in values(scheme.properties)]
end
function _expressionscheme2args(arguments, scheme::SM.ArrayScheme)
    #return collect(keys(arguments)), [resolve_scheme(scheme.items).scheme for _ in keys(arguments)]
    return collect(keys(arguments)), [scheme.items for _ in keys(arguments)]
end
function _expressionscheme2args(arguments, scheme::SM.ValueScheme)
    return _expressionscheme2args(arguments, scheme.scheme)
end
nodePath2nodeKey(nodePath) = join(nodePath,">")
resolve_scheme(s::SM.ValueScheme) = s
resolve_scheme(s::SM.RecursiveRefScheme) = s.defs[s.ref]
# Not sure yet if this function should never be called with SM.ArrayScheme #TODO
#function condition2nodes(condition, v::SM.ArrayScheme, nodeKey)
#    arg_field_names, arg_field_schemes = expressionscheme2args(condition, v.items)
#    OrderedDict("label"=> "list",
#                 "body"=> refname2templatename(v.items.scheme.ref),
#                 "children"=> condition2nodes.(getindex.(Ref(condition), arg_field_names), arg_field_schemes, string.(nodeKey, ">", arg_field_schemes)),
#                 "nodeKey"=>nodeKey)
#end
function condition2nodes(operation, v::SM.ValueScheme, nodePath)
    condition2nodes(operation, v.scheme, nodePath)
end
function operation2description(operation::AbstractDict)
    if operation["operator"] == "ComputedBooleanLookup"
        "Waar wanneer de berekening van <$(operation["variableName"])> waar is."
    elseif operation["operator"] == "KnownBooleanLookup"
        "Waar wanneer de contextwaarde van <$(operation["variableName"])> waar is."
    elseif operation["operator"] == "AnyOf"
        "Waar wanneer er minstens één hiervan waar is:"
    elseif operation["operator"] == "AllOf"
        "Waar wanneer al het volgende waar is:"
    elseif operation["operator"] == "ContainsAnyStringOf"
        "Waar wanneer er overlap is tussen deze twee lijsten:"
    elseif operation["operator"] == "StringCollectionLiteral"
        listitem2repr = Dict(
            "NotSetYet" => (o)->"?",
            "StringLiteral" => (o)->"'$(o["value"])'",
            "KnownStringLookup" => (o)->"<$(o["variableName"])>",
        )
        "De lijst: "*join([listitem2repr[o["operator"]](o) for o in operation["arguments"]], ", ", " en ")
    elseif operation["operator"] == "KnownStringCollectionLookup"
        "De contextlijst <$(operation["variableName"])>."
    elseif operation["operator"] == "KnownStringLookup"
        "De contextwaarde van <$(operation["variableName"])>."
    elseif operation["operator"] == "StringLiteral"
        "De letterlijke waarde '$(operation["value"])'."
    else
        "Een $(operation["operator"]) operatie.."
    end
end
function condition2nodes(operation, v::SM.RecursiveRefScheme, nodePath)
    scheme = resolve_scheme(v)
    # get scheme of the operator of the operation at this level
    operator_scheme = resolve_scheme(scheme.scheme.possible_objects[Symbol(operation["operator"])]).scheme
    @show typeof(operator_scheme)
    node = OrderedDict{String, Any}()
    node["label"] = operation["operator"] == "NotSetYet" ? "Kies een van de $(friendly_expressionname(v.ref)) operaties" : operation2description(operation)
    node["operator"] = operation["operator"]
    node["expressionType"] = v.ref
    node["body"] = refname2templatename(operation["operator"])
    node["nodePath"] = nodePath
    node["nodeKey"] = nodePath2nodeKey(nodePath)
    node["flintFact"] = get(operation, "flintFact", "UnknownFact")
    if "arguments" in keys(operation)
        arg_keys, arg_schemes = expressionscheme2args(operation["arguments"], operator_scheme)
        @show arg_keys typeof.(arg_schemes)
        node["children"] = condition2nodes.(getindex.(Ref(operation["arguments"]), arg_keys), arg_schemes, [[nodePath..., x] for x in arg_keys])
    elseif "value" in keys(operation)
        node["value"] = operation["value"]
    elseif "variableName" in keys(operation)
        node["variableName"] = operation["variableName"]
    end
    return node
end
friendly_expressionname(expressionname) = expressionname[3:end]
expression_operation_select(expressionname::AbstractString, v, calls::NamedTuple) = expression_operation_select(expressionname, v, v.scheme, calls)
function expression_operation_select(expressionname::AbstractString, v, ::SM.OneOfScheme, calls)
#    template("", [
        q__select("",
            var"v-if"="prop.node.expressionType=='$expressionname'",
            var":value"="prop.node.operator",
            var"@input"="(newValue)=>call=['$(calls.change_operator_call)', condition_i, prop.node.nodePath, newValue]",
            label=friendly_expressionname(expressionname),
            options! = js_attr(keys(resolve_scheme(v).scheme.possible_objects)),
            ; TEXTFIELD_KWARGS...
        )
#    ]; [Symbol("v-slot:header-$(refname2templatename(expressionname))")=>"prop"]...)
end
function expression_operation_select(expressionname::AbstractString, v, scheme::SM.AllOfScheme, calls)
    join([
#        p("{{prop.node.header}}")
#        "$expressionname: $(typeof(scheme)) = head not implemented"
    ])
end
function expression_operation_select(expressionname::AbstractString, v, scheme::SM.ValueScheme, calls)
    expression_operation_select(expressionname, v, scheme.scheme, calls)
end
operationbody(operatorname::AbstractString, v, calls::NamedTuple, defs) = operationbody(operatorname, v, v.scheme, calls, defs)
# skip expressions which are no operations (like boolean expression and stringcollectionexpressoin, those are handeled with if statements in the body)
function operationbody(operatorname::AbstractString, v, ::SM.OneOfScheme, calls, defs)
    join([])
end
function operationbody(operatorname::AbstractString, v, scheme::Any, calls, defs)
    template("", [
        Html.div("", [(expression_operation_select(expressionname, def, calls) for (expressionname, def) in pairs(defs))...])
        if operatorname == "#/ComputedBooleanLookup"
            q__select("",
                var"v-model"="prop.node.variableName",
                var"@input"="(newValue)=>call=['$(calls.change_fieldvalue_call)', condition_i, prop.node.nodePath, 'variableName', newValue]",
                #label=friendly_expressionname(operatorname),
                label="Regeling.Conditie",
                options! = "selectedModel['Regeling.Condities'].map(x=>x.variableName).filter(x=>x!==obj.variableName)",
                ; TEXTFIELD_KWARGS...
            )
        elseif operatorname == "#/KnownBooleanLookup"
            Html.div("", class="row", style="gap:4px; margin-top:8px;", [
                q__select("", style="flex-grow:1;",
                    var"v-model"="prop.node.variableName",
                    var"@input"="(newValue)=>call=['$(calls.change_fieldvalue_call)', condition_i, prop.node.nodePath, 'variableName', newValue]",
                    #label=friendly_expressionname(operatorname),
                    label="Regeling.ContextGegeven van het type Boolean",
                    options! = "function(){if (selectedModel['Regeling.ContextGegevens']) return selectedModel['Regeling.ContextGegevens'].filter(x=>x.variableType === 'Boolean').map(x=>x.variableName)}()",
                    ; TEXTFIELD_KWARGS...
                )
                q__select("", style="flex-grow:0;",
                    var"v-model"="prop.node.flintFact",
                    var"@input"="(newValue)=>call=['$(calls.change_fieldvalue_call)', condition_i, prop.node.nodePath, 'flintFact', newValue]",
                    label="FlintFact",
                    var":options"="selectedModel['Flint.Frames']",
                    var":option-value"="id",
                    var":option-label"="label",
                    [
                        template("", "v-slot:prepend", [icon("gavel")])
                    ],
                    ; TEXTFIELD_KWARGS...
                )
            ])
        elseif operatorname == "#/KnownStringLookup"
            Html.div("", class="row", style="gap:4px; margin-top:8px;", [
                q__select("", style="flex-grow:1;",
                    var"v-model"="prop.node.variableName",
                    var"@input"="(newValue)=>call=['$(calls.change_fieldvalue_call)', condition_i, prop.node.nodePath, 'variableName', newValue]",
                    #label=friendly_expressionname(operatorname),
                    label="Regeling.ContextGegeven van het type String",
                    options! = "function(){if (selectedModel['Regeling.ContextGegevens']) return selectedModel['Regeling.ContextGegevens'].filter(x=>x.variableType === 'String').map(x=>x.variableName)}()",
                    ; TEXTFIELD_KWARGS...
                )
                q__select("", style="flex-grow:0;",
                    var"v-model"="prop.node.flintFact",
                    var"@input"="(newValue)=>call=['$(calls.change_fieldvalue_call)', condition_i, prop.node.nodePath, 'flintFact', newValue]",
                    label="FlintFact",
                    var":options"="selectedModel['Flint.Frames']",
                    var":option-value"="id",
                    var":option-label"="label",
                    [
                        template("", "v-slot:prepend", [icon("gavel")])
                    ],
                    ; TEXTFIELD_KWARGS...
                )
            ])
        elseif operatorname == "#/KnownStringCollectionLookup"
            Html.div("", class="row", style="gap:4px; margin-top:8px;", [
                q__select("", style="flex-grow:1;",
                    var"v-model"="prop.node.variableName",
                    var"@input"="(newValue)=>call=['$(calls.change_fieldvalue_call)', condition_i, prop.node.nodePath, 'variableName', newValue]",
                    #label=friendly_expressionname(operatorname),
                    label="Regeling.ContextGegeven van het type StringCollection",
                    options! = "function(){if (selectedModel['Regeling.ContextGegevens']) return selectedModel['Regeling.ContextGegevens'].filter(x=>x.variableType === 'StringCollection').map(x=>x.variableName)}()",
                    ; TEXTFIELD_KWARGS...
                )
                q__select("", style="flex-grow:0;",
                    var"v-model"="prop.node.flintFact",
                    var"@input"="(newValue)=>call=['$(calls.change_fieldvalue_call)', condition_i, prop.node.nodePath, 'flintFact', newValue]",
                    label="FlintFact",
                    var":options"="selectedModel['Flint.Frames']",
                    var":option-value"="id",
                    var":option-label"="label",
                    [
                        template("", "v-slot:prepend", [icon("gavel")])
                    ],
                    ; TEXTFIELD_KWARGS...
                )
            ])
        elseif operatorname == "#/StringLiteral"
            Html.div("", class="row", style="gap:4px; margin-top:8px;", [
                q__input("", style="flex-grow:0;",
                    var"v-model" = "prop.node.value",
                    var"@input"="(newValue)=>call=['$(calls.change_fieldvalue_call)', condition_i, prop.node.nodePath, 'value', newValue]",
                    label="value", ; TEXTFIELD_KWARGS...)
                q__select("", style="flex-grow:1;",
                    var"v-model"="prop.node.flintFact",
                    var"@input"="(newValue)=>call=['$(calls.change_fieldvalue_call)', condition_i, prop.node.nodePath, 'flintFact', newValue]",
                    label="FlintFact",
                    var":options"="selectedModel['Flint.Frames']",
                    var":option-value"="id",
                    var":option-label"="label",
                    [
                        template("", "v-slot:prepend", [icon("gavel")])
                    ],
                    ; TEXTFIELD_KWARGS...
                )
            ])
        else
            ""
        end
    ]; [Symbol("v-slot:body-$(refname2templatename(operatorname))")=>"prop"]...)
end
function expressiontemplate(defpair, calls, defs)
    refname, scheme = defpair
    join([
 #       expression_operation_select(refname, scheme, calls)
        operationbody(refname, scheme, calls, defs)
    ])
end
_initial_arguments_field_content(scheme::SM.ObjectScheme, defs) = make_operation("NotSetYet", defs)
_initial_arguments_field_content(scheme::SM.OneOfScheme, defs) = make_operation("NotSetYet", defs)
_initial_arguments_field_content(scheme::SM.StringScheme, defs) = ""
function _make_not_set_args(scheme::SM.AllOfScheme, defs)
    for (i, object_or_ref) in enumerate(scheme.merge_objects)
        object = resolve_scheme(object_or_ref).scheme
        if :arguments in keys(object.properties)
            return _make_not_set_args(object.properties.arguments.scheme, defs)
        end
    end
    # for the case that the operatoins scheme does not have an "arguments" property
    return OrderedDict{String,Any}()
end
function _make_not_set_args(scheme::SM.ObjectScheme, defs)
    @show typeof(scheme)
    return OrderedDict{String,Any}("arguments" => OrderedDict(
        string(k) => _initial_arguments_field_content(resolve_scheme(v).scheme, defs)
        for (k,v) in pairs(scheme.properties))
    )
end
function _make_not_set_args(scheme::SM.ArrayScheme, defs)
    return OrderedDict{String,Any}("arguments" => [_initial_arguments_field_content(resolve_scheme(scheme.items).scheme, defs)
                                       for _ in 1:3])
end
function _make_empty_fields(scheme::SM.AllOfScheme)
    r = OrderedDict{String, Any}()
    for object_or_ref in scheme.merge_objects
        object = resolve_scheme(object_or_ref).scheme
        for k in keys(object.properties)
            if k == :flintFact
                r[string(k)] = "FactNotSetYet"
            elseif !(k in [:operator, :arguments])
                r[string(k)] = ""
            end
        end
    end
    return r
end
function make_operation(operator, defs)
    # TODO solve this key conversion
    scheme = defs["#/$operator"]
    merge(OrderedDict{String,Any}("operator"=>operator), _make_not_set_args(scheme.scheme, defs), _make_empty_fields(scheme.scheme))
end
function nested_tree_field2list(nodes, fieldname)
    r = []
    for node in nodes
        if "children" in keys(node)
            append!(r, nested_tree_field2list(node["children"], fieldname))
        end
        if fieldname in keys(node)
            push!(r, node[fieldname])
        end
    end
    return r
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::Any, ::SM.ConditionListFormat)
    # Back-end
    # ====================================================
    localdatafieldname = replace(jsrepr(reactive_datafield),r"\W"=>"_")
    assignment_recursive_scheme = v.scheme.items.scheme.properties.assignment
    # Constructor
    EB.register_init(localdatafieldname) do model
        conditions = [x["assignment"] for x in get(model.selectedModel[], reactive_datafield[2].key, [])]
        trees = [[x] for x in condition2nodes.(conditions, Ref(assignment_recursive_scheme), Ref(["assignment"]))]
        model.localdata[][localdatafieldname] = OrderedDict{String,Any}()
        model.localdata[][localdatafieldname]["trees"] = trees
        model.localdata[][localdatafieldname]["trees_expanded"] = nested_tree_field2list.(trees, "nodeKey")
        model.localdata[]["facts_used_for_a_condition"] = vcat(nested_tree_field2list.(trees, "flintFact")...)
        notify(model.localdata)
    end
    # add_condition method
    add_condition_call = "add_condition___$(replace(jsrepr(reactive_datafield),r"\W"=>"_"))"
    EB.register_call(add_condition_call) do model, args...
        conditionfield = get_shallow_array_field(model, reactive_datafield)
        new_condition = OrderedDict{String,Any}(
            "variableName"=>"Conditie zonder omschrijving",
            "assignment"=>make_operation("NotSetYet", assignment_recursive_scheme.defs),
        )
        push!(conditionfield, new_condition)
        push!(model.localdata[][localdatafieldname]["trees"],
              [condition2nodes(new_condition["assignment"], assignment_recursive_scheme, ["assignment"])])
        push!(model.localdata[][localdatafieldname]["trees_expanded"], ["assignment"])
        notify(model.localdata)
        notify(model.selectedModel)
    end
    # change_operator method
    change_operator_call = "change_operator___$(replace(jsrepr(reactive_datafield),r"\W"=>"_"))"
    EB.register_call(change_operator_call) do model, condition_i, nodePath, new_operator
        # js 0-based to julia 1-based
        condition_i = condition_i+1
        expression = model.selectedModel[][reactive_datafield[2].key][condition_i]
        for fieldname in nodePath[begin:end-1]
            expression = expression[fieldname]["arguments"]
        end
        fieldname = nodePath[end]
        new_operation = make_operation(new_operator, assignment_recursive_scheme.defs)
        expression[fieldname] = new_operation
        model.localdata[][localdatafieldname]["trees"][condition_i] = [condition2nodes(
            model.selectedModel[][reactive_datafield[2].key][condition_i]["assignment"],
            assignment_recursive_scheme, ["assignment"]
        )]
        append!(
            model.localdata[][localdatafieldname]["trees_expanded"][condition_i],
            nodePath2nodeKey.(vcat.(Ref(nodePath), keys(get(new_operation, "arguments", []))))
        )
        model.localdata[]["facts_used_for_a_condition"] = vcat(nested_tree_field2list.(model.localdata[][localdatafieldname]["trees"], "flintFact")...)
        notify(model.selectedModel)
        notify(model.localdata)
    end
    change_fieldvalue_call = "change_fieldvalue___$(replace(jsrepr(reactive_datafield),r"\W"=>"_"))"
    EB.register_call(change_fieldvalue_call) do model, condition_i, nodePath, field, newValue
        # js 0-based to julia 1-based
        condition_i = condition_i+1
        expression = model.selectedModel[][reactive_datafield[2].key][condition_i]
        for fieldname in nodePath[begin:end-1]
            expression = expression[fieldname]["arguments"]
        end
        fieldname = nodePath[end]
        expression[fieldname][field] = newValue
        notify(model.selectedModel)
        model.localdata[][localdatafieldname]["trees"][condition_i] = [condition2nodes(
            model.selectedModel[][reactive_datafield[2].key][condition_i]["assignment"],
            assignment_recursive_scheme, ["assignment"]
        )]
        model.localdata[]["facts_used_for_a_condition"] = vcat(nested_tree_field2list.(model.localdata[][localdatafieldname]["trees"], "flintFact")...)
        notify(model.localdata)
    end
    calls = (;add_condition_call=add_condition_call,
              change_operator_call=change_operator_call,
              change_fieldvalue_call=change_fieldvalue_call)
    # Front-end
    # ====================================================
    join([
        label([span(v.name), hint(v)])
        Html.div("", class="q-pa-md row items-start q-gutter-md full-width", [
            Html.div("", class="full-width", @recur("(obj, condition_i) in $(jsrepr(reactive_datafield))"), [
                editablefield((JsVarKey("obj"), JsFixedKey("variableName")),
                              v.scheme.items.scheme.properties.variableName)
                tree("",
                    @iif("localdata['$localdatafieldname']"),
                    nodes! = "localdata['$localdatafieldname'].trees[condition_i] || []",
                    node__key="nodeKey",
                    "default-expand-all",
                    #var":selected"="null",
                    var":expanded.sync"="localdata['$localdatafieldname'].trees_expanded[condition_i]",
                    [
                        #template("", var"v-slot:default-header"="prop", [])
                        expressiontemplate.(collect(assignment_recursive_scheme.defs), Ref(calls), Ref(assignment_recursive_scheme.defs))...
                ])
            ])
            card("", [btn("", :square, icon="post_add",
                @click("call=['$add_condition_call', '$(last(reactive_datafield).key)']"),
                [tooltip("Add condition")])
            ])
        ])
    ])
end
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.ValueScheme, ::Any, ::SM.NotificationListFormat)
    # Back-end
    add_notification_call="add_notification___$(replace(jsrepr(reactive_datafield),r"\W"=>"_"))"
    EB.register_call(add_notification_call) do model, args...
        notificationfield = get_shallow_array_field(model, reactive_datafield)
        new_notification = OrderedDict(
            "NotificationId"=>abs(rand(Int)),
            "TextTemplate"=>"",
            "Criterion"=>OrderedDict("operator"=>"ComputedBooleanLookup",
                                     "variableName"=>"[NOT SET]"),
        )
        push!(notificationfield, new_notification)
        notify(model.selectedModel)
    end
    # Frond-end
    join([
        label([span(v.name), hint(v)])
        Html.div("", class="q-pa-md row items-start q-gutter-md full-width", [
            Html.div("", class="row full-width", style="display:flex; flex-direction:row;", @recur("(obj, i) in $(jsrepr(reactive_datafield))"), [
                Html.div("", class="", style="flex-grow:1", [
                    editablefield((JsVarKey("obj"), JsFixedKey("TextTemplate")),
                                  v.scheme.items.scheme.properties.TextTemplate)
                ])
                Html.div("", class="", style="flex-grow:0", [
                    editablefield((JsVarKey("obj"), JsFixedKey("Criterion")),
                                  v.scheme.items.scheme.properties.Criterion)
                ])
            ])
            card("", [btn("", :square, icon="post_add",
                    @click("call=['$add_notification_call']"),
                    [tooltip("Add notification")])
            ])
        ])
    ])
end
# default catch all
function editablefield(reactive_datafield::Tuple{Vararg{<:JsKey}}, v::SM.SchemeContainer, ::Any, ::SM.Format)
    join([])
end

println("FieldsEditor included")

end # module
