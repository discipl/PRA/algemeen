using GlobFromPython: glob
using HierarchicalConfigs: load_hierachicaly_with_sources, pairvecs2dataframe, distribute_fields_over_hierarchies
using OrderedCollections
using YAML

using PythonCall
const markdownify = pyimport("markdownify").markdownify

const PRA_MODELS_PATH = joinpath("..", "data", "vindbaar")
filenames = glob("**/*.yaml", PRA_MODELS_PATH)
models_values, models_sources = load_hierachicaly_with_sources(filenames, basepart=PRA_MODELS_PATH) do x
    YAML.load_file(x, dicttype=OrderedDict{String, Any})
end


for model_values in models_values
    if "Zoekdescriptor.Korte_Omschrijving" in keys(model_values)
        model_values["Zoekdescriptor.Korte_Omschrijving"] = pyconvert(String, markdownify(model_values["Zoekdescriptor.Korte_Omschrijving"]))
    end
end

#pyconvert.(String, markdownify.(collect(skipmissing(get.(models_values, "Zoekdescriptor.Korte_Omschrijving", missing)))))
#get.(models_values, "Zoekdescriptor.Korte_Omschrijving", missing)


h = distribute_fields_over_hierarchies.(filenames, models_values, models_sources, basepart=PRA_MODELS_PATH)

towrite = mergewith(merge, h...)

towrite_filenames = collect(keys(towrite))
towrite_content = collect(values(towrite))

YAML.write_file.(towrite_filenames, towrite_content)

unique(towrite_filenames)

