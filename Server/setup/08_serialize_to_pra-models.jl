using DataFrames
using Arrow
using JSONTables
using YAML
using JSON3
using OrderedCollections

const PROCESSED_DATA_PATH = joinpath("data", "processed")
const PRA_MODELS_PATH = joinpath("..","data","vindbaar")

content_all = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "content.arrow")))
# content_all.Grondslagen = Broadcast.BroadcastFunction(OrderedDict ∘ pairs).(content_all.Grondslagen)

levensgebeurtenissen = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "levensgebeurtenissen.arrow")))
lv_questions = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "levensgebeurtenissen_questions.arrow")))

content = DataFrame(Arrow.Table(joinpath(PROCESSED_DATA_PATH, "h1_content.arrow")))
content.Grondslagen = Broadcast.BroadcastFunction(OrderedDict ∘ pairs).(content.Grondslagen)
content.Type .= "Regeling"
#content.title_Alpha = "regeling." .* content.title_Alpha

upl_link_rename_mapping = [
    "digid" => "wetdigitaleoverheid",
    "studiefinanciering" => "wetstudiefinanciering2000",
    "zorgverzekeringsplicht" => "zorgverzekeringswet",
    "zorgtoeslag" => "wet-op-de-zorgtoeslag",
    "orgaandonatie" => "wet-op-de-orgaandonatie",
    "onderwijsbijdrageenschoolkostenvotegemoetkoming" => "wet-tegemoetkoming-onderwijsbijdrage-en-schoolkosten",
    "heffingskortinguitbetaling" => "wetinkomstenbelasting2001",
]

upl_link_to_delete = [
    "loonbelasting",
]

title_Alpha_to_delete = [
    "uittrekselbasisregistratiepersonen",
]

pra_model_grondslagen = [
 "wetdigitaleoverheid" => "jci1.3:c:BWBR0048156&z=2023-07-01&g=2023-07-01",
 "wetstudiefinanciering2000" => "jci1.3:c:BWBR0011453&hoofdstuk=2&paragraaf=2.1&artikel=2.1&z=2023-09-01&g=2023-09-01",
 "participatiewet" => "jci1.3:c:BWBR0015703&hoofdstuk=2&paragraaf=2.2&artikel=11&z=2023-07-01&g=2023-07-01",
 "zorgverzekeringswet" => "jci1.3:c:BWBR0018450&hoofdstuk=2&paragraaf=2.1&artikel=2&z=2023-01-01&g=2023-01-01",
 "wet-op-de-zorgtoeslag" => "jci1.3:c:BWBR0018451&artikel=2&z=2023-01-01&g=2023-01-01",
 "wet-op-de-orgaandonatie" => "jci1.3:c:BWBR0008066&z=2022-01-01&g=2022-01-01",
 "wet-tegemoetkoming-onderwijsbijdrage-en-schoolkosten" => "jci1.3:c:BWBR0012438&z=2023-08-01&g=2023-08-01",
 "wetinkomstenbelasting2001" => "jci1.3:c:BWBR0011353&hoofdstuk=1&artikel=1.1&z=2023-07-01&g=2023-07-01",
]

pra_models = DataFrame(NamedTuple{(:UPL_Link, :PRA_Model, :Juriconnect)}.([
    ["digid", "wetdigitaleoverheid", "jci1.3:c:BWBR0048156&z=2023-07-01&g=2023-07-01",],
    ["studiefinanciering", "wetstudiefinanciering2000", "jci1.3:c:BWBR0011453&hoofdstuk=2&paragraaf=2.1&artikel=2.1&z=2023-09-01&g=2023-09-01",],
    ["participatiewet", "participatiewet", "jci1.3:c:BWBR0015703&hoofdstuk=2&paragraaf=2.2&artikel=11&z=2023-07-01&g=2023-07-01",],
    ["zorgverzekeringsplicht", "zorgverzekeringswet", "jci1.3:c:BWBR0018450&hoofdstuk=2&paragraaf=2.1&artikel=2&z=2023-01-01&g=2023-01-01",],
    ["zorgtoeslag", "wet-op-de-zorgtoeslag", "jci1.3:c:BWBR0018451&artikel=2&z=2023-01-01&g=2023-01-01",],
    ["orgaandonorregistratie", "wet-op-de-orgaandonatie", "jci1.3:c:BWBR0008066&z=2022-01-01&g=2022-01-01",],
    ["onderwijsbijdrageenschoolkostenvotegemoetkoming", "wet-tegemoetkoming-onderwijsbijdrage-en-schoolkosten", "jci1.3:c:BWBR0012438&z=2023-08-01&g=2023-08-01",],
    ["heffingskortinguitbetaling", "wetinkomstenbelasting2001", "jci1.3:c:BWBR0011353&hoofdstuk=1&artikel=1.1&z=2023-07-01&g=2023-07-01",],
]))

content = content[.!in.(content.UPL_Link, Ref(upl_link_to_delete)), :]
content = content[.!in.(content.title_Alpha, Ref(title_Alpha_to_delete)), :]

content = content[.!(content.UPL_Link .== "orgaandonorregistratie" .&& content.Authority .!= "Ministerie van Volksgezondheid, Welzijn en Sport"),:]

content[contains.(content.title_Alpha, "studietoeslag"), :UPL_Link] .= "participatiewet"
content[content.language .== "nl", :language] .= "nl-NL"
content[content.language .== "en", :language] .= "en-GB"

leftjoin!(content, pra_models, on=:UPL_Link)

content.Variant = ((args...)->join([args...], ".")).(
    "regeling",
    content.PRA_Model,
    content.authorityScheme,
    replace.(content.Authority, r"[ ,]"=> "_"),
    content.language)

content.Model = ((args...)->join([args...], ".")).("regeling", content.PRA_Model)

content.Subjects = vcat.(content.Subjects, "18 jaar worden")

regelingen_cols = [
 :Content_Id => :Id,
 :Authority =>  "Wordt_Onderhouden_Door",
 :Modified =>          "Zoekdescriptor.Laatst_Bewerkt_Op",
 :language =>          "Zoekdescriptor.Taal",
 :Variant =>      "Zoekdescriptor.Variant",
 :Model =>        "Zoekdescriptor.Model",
 :title =>             "Zoekdescriptor.Titel",
 :Short_Description => "Zoekdescriptor.Korte_Omschrijving",
 :Subjects =>          "Zoekdescriptor.Filters",
 :Terms =>             "Zoekdescriptor.Zoektermen",
 :Type,
 :Onderwerp_Id =>         "Regeling.Id",
 :PRA_Model =>            "Regeling.Naam",
 :Juriconnect =>          "Regeling.Grondslag",
 :title =>                "Regeling.Titel",
 :subTitle =>             "Regeling.Subtitel",
 :Description =>          "Regeling.Omschrijving",
 :Grondslagen =>          "Regeling.Relvante_Grondslagen",
 :levensgebeurtenis_id => "Regeling.Bijbehorende_Levensgebeurtenis",
 :Levensgebeurtenissen => "Regeling.Relevante_Levensgebeurtenissen",
 :authorityScheme =>      "Regeling.Authoriteitsoort",
 :Authority =>            "Regeling.Authoriteit",
 :audience__ =>           "Regeling.Doelgroep",
#:Onderwerp_UPL_Alpha,
 :Source =>         "Regeling.Dev.Bron",
 :recordPosition => "Regeling.Dev.Bronrij",
 :UPL_Link =>       "Regeling.Dev.Waarschijnlijke_UPL",
 :UPL_Alpha =>      "Regeling.Dev.Relevante_UPLs",
 :title_Alpha =>    "Regeling.Dev.Bestandsnaam",
 :Source_URL =>     "Regeling.Dev.Source_URL",
#:N_Onderwerp_Contents,
#:Onderwerp_Id,
]

content = select(content, regelingen_cols...)

for colname in names(content)
    content[!, colname] .= coalesce.(content[!, colname], nothing)
end

qa = flatten(lv_questions, [:answerOptions__id, :answerOptions__text, :answerOptions__feedback, :answerOptions__helpText])
qa.QA = qa.text .* " = " .* qa.answerOptions__text

qa[:, [:id, :answerOptions__id, :QA]]

leftjoin!(levensgebeurtenissen, combine(groupby(qa, :levensgebeurtenis_id), :QA => Ref), on=:levensgebeurtenis_id)

levensgebeurtenissen.Id .= rownumber.(eachrow(levensgebeurtenissen)) .+ maximum(content_all.Content_Id)
levensgebeurtenissen.Type .= "Levensgebeurtenis"
levensgebeurtenissen.title_Alpha = "levensgebeurtenis." .* replace.(lowercase.(levensgebeurtenissen.title), r"[^a-z0-9]"=>"")

levensgebeurtenissen.Terms = split.(replace.(lowercase.(levensgebeurtenissen.title), r"[^a-z0-9]"=>" "))
levensgebeurtenissen.Subjects = (x->[x]).(levensgebeurtenissen.organisation)
levensgebeurtenissen.Variant = ((args...)->join([args...], ".")).(
    levensgebeurtenissen.title_Alpha,
    levensgebeurtenissen.language)

levensgebeurtenissen_cols = [
 "Id",
 :creator => "Wordt_Onderhouden_Door",
 "publicationDate" =>  "Zoekdescriptor.Laatst_Bewerkt_Op",
 "language" =>         "Zoekdescriptor.Taal",
 :Variant =>      "Zoekdescriptor.Variant",
 :title_Alpha =>       "Zoekdescriptor.Model",
 "title" =>            "Zoekdescriptor.Titel",
 "title" =>            "Zoekdescriptor.Korte_Omschrijving",
 :Subjects =>          "Zoekdescriptor.Filters",
 :Terms =>             "Zoekdescriptor.Zoektermen",
 :Type,
 "levensgebeurtenis_id" => "Levensgebeurtenis.Id",
 "title" =>                "Levensgebeurtenis.Titel",
 "description" =>          "Levensgebeurtenis.Omschrijving",
 "QA_Ref" =>               "Levensgebeurtenis.Relevante_Filters",
]

levensgebeurtenissen = select(levensgebeurtenissen, levensgebeurtenissen_cols...)

content_unique_defined = copy(content)
max_hierarchy = maximum(length.(split.(content[:,"Zoekdescriptor.Variant"], ".")))
content_unique_defined[!,names(content)] .= max_hierarchy+1

level_dfs = []
for level in 1:max_hierarchy
    df = transform(groupby(transform(content, "Zoekdescriptor.Variant" => ByRow(x -> split(x, ".")[1:level]) => :GroupCol), :GroupCol),
        names(content) .=> (x-> x == 1 ? level : missing) ∘ length ∘ unique .=> names(content)
    )
    select!(df, Not(:GroupCol))
    push!(level_dfs, df)
end

fielddefenition_per_Level = coalesce.(level_dfs..., content_unique_defined)
last_level_fields = [
    "Id",
    "Zoekdescriptor.Laatst_Bewerkt_Op",
    "Zoekdescriptor.Taal",
    "Zoekdescriptor.Variant",
    "Zoekdescriptor.Titel",
    "Zoekdescriptor.Korte_Omschrijving",
    "Zoekdescriptor.Zoektermen",
    "Regeling.Titel",
    "Regeling.Subtitel",
    "Regeling.Omschrijving",
    "Regeling.Dev.Source_URL",
]
fielddefenition_per_Level[!, last_level_fields] .= max_hierarchy+1

def_dfs = []
for level in 1:max_hierarchy
    df = ifelse.(fielddefenition_per_Level .== level, content, missing)
    df[!, "Zoekdescriptor.Variant"] .= content[:, "Zoekdescriptor.Variant"]
    df = combine(groupby(transform(df, "Zoekdescriptor.Variant" => ByRow(x -> split(x, ".")[1:level]) => :GroupCol), :GroupCol),
        names(content) .=> (x-> length(x) == 1 ? x : [missing]) ∘ unique .=> names(content)
    )
    groupcol = df.GroupCol
    select!(df, Not("GroupCol", "Zoekdescriptor.Variant"))
    push!(def_dfs, df)
    for (row, group) in zip(eachrow(df), groupcol)
        path = mkpath(joinpath(PRA_MODELS_PATH, group...))
        filename = joinpath(path, "defaults.yaml")
        fields = filter(x->!ismissing(x.second), collect(pairs(row)))
        !isempty(fields) && YAML.write_file(filename, OrderedDict(["Compleet" => false, fields...]))
    end
end

filenames = ((x,y)->"$x-$y.yaml").(string.(content.Id), content[:, "Regeling.Dev.Bestandsnaam"])
for (filename, row) in zip(filenames,
        eachrow(ifelse.(fielddefenition_per_Level .== max_hierarchy+1, content, missing)))
    dir = mkpath(joinpath(PRA_MODELS_PATH, split(row["Zoekdescriptor.Variant"], ".")...))
    fields = ["Compleet" => true, filter(x->!ismissing(x.second), collect(pairs(row)))...]
    YAML.write_file(joinpath(dir, filename), OrderedDict(fields))
end

for row in eachrow(levensgebeurtenissen)
    dir = mkpath(joinpath(PRA_MODELS_PATH, split(row["Zoekdescriptor.Variant"], ".")...))
    fields = filter(x->!ismissing(x.second), collect(pairs(row)))
    filename = joinpath(dir, "$(row.Id).yaml")
    YAML.write_file(filename, OrderedDict(["Compleet" => true, fields...]))
end

